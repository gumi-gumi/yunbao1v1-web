/*
 Navicat Premium Data Transfer

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cmf_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `cmf_admin_menu`;
CREATE TABLE `cmf_admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '菜单类型;1:有界面可访问菜单,2:无界面可访问菜单,0:只作为菜单',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态;1:显示,0:不显示',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `app` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '应用名',
  `controller` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '控制器名',
  `action` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '操作名称',
  `param` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '额外参数',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `controller` (`controller`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='后台菜单表';

-- ----------------------------
-- Records of cmf_admin_menu
-- ----------------------------
BEGIN;
INSERT INTO `cmf_admin_menu` VALUES (1, 0, 0, 1, 10001, 'admin', 'Plugin', 'default', '', '插件中心', 'cloud', '插件中心');
INSERT INTO `cmf_admin_menu` VALUES (6, 0, 0, 1, 0, 'admin', 'Setting', 'default', '', '设置', 'cogs', '系统设置入口');
INSERT INTO `cmf_admin_menu` VALUES (15, 6, 1, 0, 10, 'admin', 'Mailer', 'index', '', '邮箱配置', '', '邮箱配置');
INSERT INTO `cmf_admin_menu` VALUES (16, 15, 2, 0, 10000, 'admin', 'Mailer', 'indexPost', '', '邮箱配置提交保存', '', '邮箱配置提交保存');
INSERT INTO `cmf_admin_menu` VALUES (17, 15, 1, 0, 10000, 'admin', 'Mailer', 'template', '', '邮件模板', '', '邮件模板');
INSERT INTO `cmf_admin_menu` VALUES (18, 15, 2, 0, 10000, 'admin', 'Mailer', 'templatePost', '', '邮件模板提交', '', '邮件模板提交');
INSERT INTO `cmf_admin_menu` VALUES (19, 15, 1, 0, 10000, 'admin', 'Mailer', 'test', '', '邮件发送测试', '', '邮件发送测试');
INSERT INTO `cmf_admin_menu` VALUES (20, 6, 1, 0, 10000, 'admin', 'Menu', 'index', '', '后台菜单', '', '后台菜单管理');
INSERT INTO `cmf_admin_menu` VALUES (21, 20, 1, 0, 10000, 'admin', 'Menu', 'lists', '', '所有菜单', '', '后台所有菜单列表');
INSERT INTO `cmf_admin_menu` VALUES (22, 20, 1, 0, 10000, 'admin', 'Menu', 'add', '', '后台菜单添加', '', '后台菜单添加');
INSERT INTO `cmf_admin_menu` VALUES (23, 20, 2, 0, 10000, 'admin', 'Menu', 'addPost', '', '后台菜单添加提交保存', '', '后台菜单添加提交保存');
INSERT INTO `cmf_admin_menu` VALUES (24, 20, 1, 0, 10000, 'admin', 'Menu', 'edit', '', '后台菜单编辑', '', '后台菜单编辑');
INSERT INTO `cmf_admin_menu` VALUES (25, 20, 2, 0, 10000, 'admin', 'Menu', 'editPost', '', '后台菜单编辑提交保存', '', '后台菜单编辑提交保存');
INSERT INTO `cmf_admin_menu` VALUES (26, 20, 2, 0, 10000, 'admin', 'Menu', 'delete', '', '后台菜单删除', '', '后台菜单删除');
INSERT INTO `cmf_admin_menu` VALUES (27, 20, 2, 0, 10000, 'admin', 'Menu', 'listOrder', '', '后台菜单排序', '', '后台菜单排序');
INSERT INTO `cmf_admin_menu` VALUES (28, 20, 1, 0, 10000, 'admin', 'Menu', 'getActions', '', '导入新后台菜单', '', '导入新后台菜单');
INSERT INTO `cmf_admin_menu` VALUES (42, 1, 1, 1, 10000, 'admin', 'Plugin', 'index', '', '插件列表', '', '插件列表');
INSERT INTO `cmf_admin_menu` VALUES (43, 42, 2, 0, 10000, 'admin', 'Plugin', 'toggle', '', '插件启用禁用', '', '插件启用禁用');
INSERT INTO `cmf_admin_menu` VALUES (44, 42, 1, 0, 10000, 'admin', 'Plugin', 'setting', '', '插件设置', '', '插件设置');
INSERT INTO `cmf_admin_menu` VALUES (45, 42, 2, 0, 10000, 'admin', 'Plugin', 'settingPost', '', '插件设置提交', '', '插件设置提交');
INSERT INTO `cmf_admin_menu` VALUES (46, 42, 2, 0, 10000, 'admin', 'Plugin', 'install', '', '插件安装', '', '插件安装');
INSERT INTO `cmf_admin_menu` VALUES (47, 42, 2, 0, 10000, 'admin', 'Plugin', 'update', '', '插件更新', '', '插件更新');
INSERT INTO `cmf_admin_menu` VALUES (48, 42, 2, 0, 10000, 'admin', 'Plugin', 'uninstall', '', '卸载插件', '', '卸载插件');
INSERT INTO `cmf_admin_menu` VALUES (49, 110, 0, 1, 10000, 'admin', 'User', 'default', '', '管理组', '', '管理组');
INSERT INTO `cmf_admin_menu` VALUES (50, 49, 1, 1, 10000, 'admin', 'Rbac', 'index', '', '角色管理', '', '角色管理');
INSERT INTO `cmf_admin_menu` VALUES (51, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleAdd', '', '添加角色', '', '添加角色');
INSERT INTO `cmf_admin_menu` VALUES (52, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleAddPost', '', '添加角色提交', '', '添加角色提交');
INSERT INTO `cmf_admin_menu` VALUES (53, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleEdit', '', '编辑角色', '', '编辑角色');
INSERT INTO `cmf_admin_menu` VALUES (54, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleEditPost', '', '编辑角色提交', '', '编辑角色提交');
INSERT INTO `cmf_admin_menu` VALUES (55, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleDelete', '', '删除角色', '', '删除角色');
INSERT INTO `cmf_admin_menu` VALUES (56, 50, 1, 0, 10000, 'admin', 'Rbac', 'authorize', '', '设置角色权限', '', '设置角色权限');
INSERT INTO `cmf_admin_menu` VALUES (57, 50, 2, 0, 10000, 'admin', 'Rbac', 'authorizePost', '', '角色授权提交', '', '角色授权提交');
INSERT INTO `cmf_admin_menu` VALUES (61, 6, 1, 0, 10000, 'admin', 'Route', 'index', '', 'URL美化', '', 'URL规则管理');
INSERT INTO `cmf_admin_menu` VALUES (62, 61, 1, 0, 10000, 'admin', 'Route', 'add', '', '添加路由规则', '', '添加路由规则');
INSERT INTO `cmf_admin_menu` VALUES (63, 61, 2, 0, 10000, 'admin', 'Route', 'addPost', '', '添加路由规则提交', '', '添加路由规则提交');
INSERT INTO `cmf_admin_menu` VALUES (64, 61, 1, 0, 10000, 'admin', 'Route', 'edit', '', '路由规则编辑', '', '路由规则编辑');
INSERT INTO `cmf_admin_menu` VALUES (65, 61, 2, 0, 10000, 'admin', 'Route', 'editPost', '', '路由规则编辑提交', '', '路由规则编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (66, 61, 2, 0, 10000, 'admin', 'Route', 'delete', '', '路由规则删除', '', '路由规则删除');
INSERT INTO `cmf_admin_menu` VALUES (67, 61, 2, 0, 10000, 'admin', 'Route', 'ban', '', '路由规则禁用', '', '路由规则禁用');
INSERT INTO `cmf_admin_menu` VALUES (68, 61, 2, 0, 10000, 'admin', 'Route', 'open', '', '路由规则启用', '', '路由规则启用');
INSERT INTO `cmf_admin_menu` VALUES (69, 61, 2, 0, 10000, 'admin', 'Route', 'listOrder', '', '路由规则排序', '', '路由规则排序');
INSERT INTO `cmf_admin_menu` VALUES (70, 61, 1, 0, 10000, 'admin', 'Route', 'select', '', '选择URL', '', '选择URL');
INSERT INTO `cmf_admin_menu` VALUES (71, 6, 1, 1, 0, 'admin', 'Setting', 'site', '', '网站信息', '', '网站信息');
INSERT INTO `cmf_admin_menu` VALUES (72, 71, 2, 0, 10000, 'admin', 'Setting', 'sitePost', '', '网站信息设置提交', '', '网站信息设置提交');
INSERT INTO `cmf_admin_menu` VALUES (73, 0, 1, 0, 10000, 'admin', 'Setting', 'password', '', '密码修改', '', '密码修改');
INSERT INTO `cmf_admin_menu` VALUES (74, 73, 2, 0, 10000, 'admin', 'Setting', 'passwordPost', '', '密码修改提交', '', '密码修改提交');
INSERT INTO `cmf_admin_menu` VALUES (75, 6, 1, 1, 10000, 'admin', 'Setting', 'upload', '', '上传设置', '', '上传设置');
INSERT INTO `cmf_admin_menu` VALUES (76, 75, 2, 0, 10000, 'admin', 'Setting', 'uploadPost', '', '上传设置提交', '', '上传设置提交');
INSERT INTO `cmf_admin_menu` VALUES (77, 6, 1, 0, 10000, 'admin', 'Setting', 'clearCache', '', '清除缓存', '', '清除缓存');
INSERT INTO `cmf_admin_menu` VALUES (78, 6, 1, 1, 40, 'admin', 'Slide', 'index', '', '幻灯片管理', '', '幻灯片管理');
INSERT INTO `cmf_admin_menu` VALUES (79, 78, 1, 0, 10000, 'admin', 'Slide', 'add', '', '添加幻灯片', '', '添加幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (80, 78, 2, 0, 10000, 'admin', 'Slide', 'addPost', '', '添加幻灯片提交', '', '添加幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (81, 78, 1, 0, 10000, 'admin', 'Slide', 'edit', '', '编辑幻灯片', '', '编辑幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (82, 78, 2, 0, 10000, 'admin', 'Slide', 'editPost', '', '编辑幻灯片提交', '', '编辑幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (83, 78, 2, 0, 10000, 'admin', 'Slide', 'delete', '', '删除幻灯片', '', '删除幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (84, 78, 1, 0, 10000, 'admin', 'SlideItem', 'index', '', '幻灯片页面列表', '', '幻灯片页面列表');
INSERT INTO `cmf_admin_menu` VALUES (85, 84, 1, 0, 10000, 'admin', 'SlideItem', 'add', '', '幻灯片页面添加', '', '幻灯片页面添加');
INSERT INTO `cmf_admin_menu` VALUES (86, 84, 2, 0, 10000, 'admin', 'SlideItem', 'addPost', '', '幻灯片页面添加提交', '', '幻灯片页面添加提交');
INSERT INTO `cmf_admin_menu` VALUES (87, 84, 1, 0, 10000, 'admin', 'SlideItem', 'edit', '', '幻灯片页面编辑', '', '幻灯片页面编辑');
INSERT INTO `cmf_admin_menu` VALUES (88, 84, 2, 0, 10000, 'admin', 'SlideItem', 'editPost', '', '幻灯片页面编辑提交', '', '幻灯片页面编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (89, 84, 2, 0, 10000, 'admin', 'SlideItem', 'delete', '', '幻灯片页面删除', '', '幻灯片页面删除');
INSERT INTO `cmf_admin_menu` VALUES (90, 84, 2, 0, 10000, 'admin', 'SlideItem', 'ban', '', '幻灯片页面隐藏', '', '幻灯片页面隐藏');
INSERT INTO `cmf_admin_menu` VALUES (91, 84, 2, 0, 10000, 'admin', 'SlideItem', 'cancelBan', '', '幻灯片页面显示', '', '幻灯片页面显示');
INSERT INTO `cmf_admin_menu` VALUES (92, 84, 2, 0, 10000, 'admin', 'SlideItem', 'listOrder', '', '幻灯片页面排序', '', '幻灯片页面排序');
INSERT INTO `cmf_admin_menu` VALUES (93, 6, 1, 0, 10000, 'admin', 'Storage', 'index', '', '文件存储', '', '文件存储');
INSERT INTO `cmf_admin_menu` VALUES (94, 93, 2, 0, 10000, 'admin', 'Storage', 'settingPost', '', '文件存储设置提交', '', '文件存储设置提交');
INSERT INTO `cmf_admin_menu` VALUES (110, 0, 0, 1, 1, 'user', 'AdminIndex', 'default', '', '用户管理', 'group', '用户管理');
INSERT INTO `cmf_admin_menu` VALUES (111, 49, 1, 1, 10000, 'admin', 'User', 'index', '', '管理员', '', '管理员管理');
INSERT INTO `cmf_admin_menu` VALUES (112, 111, 1, 0, 10000, 'admin', 'User', 'add', '', '管理员添加', '', '管理员添加');
INSERT INTO `cmf_admin_menu` VALUES (113, 111, 2, 0, 10000, 'admin', 'User', 'addPost', '', '管理员添加提交', '', '管理员添加提交');
INSERT INTO `cmf_admin_menu` VALUES (114, 111, 1, 0, 10000, 'admin', 'User', 'edit', '', '管理员编辑', '', '管理员编辑');
INSERT INTO `cmf_admin_menu` VALUES (115, 111, 2, 0, 10000, 'admin', 'User', 'editPost', '', '管理员编辑提交', '', '管理员编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (116, 111, 1, 0, 10000, 'admin', 'User', 'userInfo', '', '个人信息', '', '管理员个人信息修改');
INSERT INTO `cmf_admin_menu` VALUES (117, 111, 2, 0, 10000, 'admin', 'User', 'userInfoPost', '', '管理员个人信息修改提交', '', '管理员个人信息修改提交');
INSERT INTO `cmf_admin_menu` VALUES (118, 111, 2, 0, 10000, 'admin', 'User', 'delete', '', '管理员删除', '', '管理员删除');
INSERT INTO `cmf_admin_menu` VALUES (119, 111, 2, 0, 10000, 'admin', 'User', 'ban', '', '停用管理员', '', '停用管理员');
INSERT INTO `cmf_admin_menu` VALUES (120, 111, 2, 0, 10000, 'admin', 'User', 'cancelBan', '', '启用管理员', '', '启用管理员');
INSERT INTO `cmf_admin_menu` VALUES (121, 0, 0, 1, 12, 'portal', 'AdminIndex', 'default', '', '内容管理', 'th', '门户管理');
INSERT INTO `cmf_admin_menu` VALUES (122, 121, 1, 0, 10000, 'portal', 'AdminArticle', 'index', '', '文章管理', '', '文章列表');
INSERT INTO `cmf_admin_menu` VALUES (123, 122, 1, 0, 10000, 'portal', 'AdminArticle', 'add', '', '添加文章', '', '添加文章');
INSERT INTO `cmf_admin_menu` VALUES (124, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'addPost', '', '添加文章提交', '', '添加文章提交');
INSERT INTO `cmf_admin_menu` VALUES (125, 122, 1, 0, 10000, 'portal', 'AdminArticle', 'edit', '', '编辑文章', '', '编辑文章');
INSERT INTO `cmf_admin_menu` VALUES (126, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'editPost', '', '编辑文章提交', '', '编辑文章提交');
INSERT INTO `cmf_admin_menu` VALUES (127, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'delete', '', '文章删除', '', '文章删除');
INSERT INTO `cmf_admin_menu` VALUES (128, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'publish', '', '文章发布', '', '文章发布');
INSERT INTO `cmf_admin_menu` VALUES (129, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'top', '', '文章置顶', '', '文章置顶');
INSERT INTO `cmf_admin_menu` VALUES (130, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'recommend', '', '文章推荐', '', '文章推荐');
INSERT INTO `cmf_admin_menu` VALUES (131, 122, 2, 0, 10000, 'portal', 'AdminArticle', 'listOrder', '', '文章排序', '', '文章排序');
INSERT INTO `cmf_admin_menu` VALUES (132, 121, 1, 0, 10000, 'portal', 'AdminCategory', 'index', '', '分类管理', '', '文章分类列表');
INSERT INTO `cmf_admin_menu` VALUES (133, 132, 1, 0, 10000, 'portal', 'AdminCategory', 'add', '', '添加文章分类', '', '添加文章分类');
INSERT INTO `cmf_admin_menu` VALUES (134, 132, 2, 0, 10000, 'portal', 'AdminCategory', 'addPost', '', '添加文章分类提交', '', '添加文章分类提交');
INSERT INTO `cmf_admin_menu` VALUES (135, 132, 1, 0, 10000, 'portal', 'AdminCategory', 'edit', '', '编辑文章分类', '', '编辑文章分类');
INSERT INTO `cmf_admin_menu` VALUES (136, 132, 2, 0, 10000, 'portal', 'AdminCategory', 'editPost', '', '编辑文章分类提交', '', '编辑文章分类提交');
INSERT INTO `cmf_admin_menu` VALUES (137, 132, 1, 0, 10000, 'portal', 'AdminCategory', 'select', '', '文章分类选择对话框', '', '文章分类选择对话框');
INSERT INTO `cmf_admin_menu` VALUES (138, 132, 2, 0, 10000, 'portal', 'AdminCategory', 'listOrder', '', '文章分类排序', '', '文章分类排序');
INSERT INTO `cmf_admin_menu` VALUES (139, 132, 2, 0, 10000, 'portal', 'AdminCategory', 'delete', '', '删除文章分类', '', '删除文章分类');
INSERT INTO `cmf_admin_menu` VALUES (140, 121, 1, 1, 10000, 'portal', 'AdminPage', 'index', '', '页面管理', '', '页面管理');
INSERT INTO `cmf_admin_menu` VALUES (141, 140, 1, 0, 10000, 'portal', 'AdminPage', 'add', '', '添加页面', '', '添加页面');
INSERT INTO `cmf_admin_menu` VALUES (142, 140, 2, 0, 10000, 'portal', 'AdminPage', 'addPost', '', '添加页面提交', '', '添加页面提交');
INSERT INTO `cmf_admin_menu` VALUES (143, 140, 1, 0, 10000, 'portal', 'AdminPage', 'edit', '', '编辑页面', '', '编辑页面');
INSERT INTO `cmf_admin_menu` VALUES (144, 140, 2, 0, 10000, 'portal', 'AdminPage', 'editPost', '', '编辑页面提交', '', '编辑页面提交');
INSERT INTO `cmf_admin_menu` VALUES (145, 140, 2, 0, 10000, 'portal', 'AdminPage', 'delete', '', '删除页面', '', '删除页面');
INSERT INTO `cmf_admin_menu` VALUES (153, 110, 0, 1, 10000, 'user', 'AdminIndex', 'default1', '', '用户组', '', '用户组');
INSERT INTO `cmf_admin_menu` VALUES (154, 153, 1, 1, 1, 'user', 'AdminIndex', 'index', '', '本站用户', '', '本站用户');
INSERT INTO `cmf_admin_menu` VALUES (155, 154, 2, 0, 10000, 'user', 'AdminIndex', 'ban', '', '本站用户拉黑', '', '本站用户拉黑');
INSERT INTO `cmf_admin_menu` VALUES (156, 154, 2, 0, 10000, 'user', 'AdminIndex', 'cancelBan', '', '本站用户启用', '', '本站用户启用');
INSERT INTO `cmf_admin_menu` VALUES (163, 6, 1, 1, 1, 'Admin', 'Setting', 'configpri', '', '私密设置', '', '');
INSERT INTO `cmf_admin_menu` VALUES (164, 163, 1, 0, 10000, 'Admin', 'Setting', 'configpriPost', '', '提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (165, 0, 0, 1, 8, 'Admin', 'Level', 'default', '', '等级管理', 'level-up', '');
INSERT INTO `cmf_admin_menu` VALUES (166, 165, 1, 1, 10000, 'Admin', 'Level', 'index', '', '经验等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (167, 166, 1, 0, 10000, 'Admin', 'Level', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (168, 166, 1, 0, 10000, 'Admin', 'Level', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (169, 166, 1, 0, 10000, 'Admin', 'Level', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (170, 166, 1, 0, 10000, 'Admin', 'Level', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (171, 166, 1, 0, 10000, 'Admin', 'Level', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (172, 165, 1, 1, 10000, 'Admin', 'Levelanchor', 'index', '', '主播等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (173, 172, 1, 0, 10000, 'Admin', 'Levelanchor', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (174, 172, 1, 0, 10000, 'Admin', 'Levelanchor', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (175, 172, 1, 0, 10000, 'Admin', 'Levelanchor', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (176, 172, 1, 0, 10000, 'Admin', 'Levelanchor', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (177, 172, 1, 0, 10000, 'Admin', 'Levelanchor', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (193, 0, 0, 1, 6, 'Admin', 'Finance', 'default', '', '财务管理', 'jpy', '');
INSERT INTO `cmf_admin_menu` VALUES (194, 193, 1, 1, 10000, 'Admin', 'Chargerule', 'index', '', '充值规则', '', '');
INSERT INTO `cmf_admin_menu` VALUES (195, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (196, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (197, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (198, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (199, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (200, 194, 1, 0, 10000, 'Admin', 'Chargerule', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (201, 193, 1, 1, 10000, 'Admin', 'Charge', 'Index', '', '充值记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (202, 201, 1, 0, 10000, 'Admin', 'Charge', 'setPay', '', '确认支付', '', '');
INSERT INTO `cmf_admin_menu` VALUES (203, 193, 1, 1, 10000, 'Admin', 'Manual', 'index', '', '手动充值', '', '');
INSERT INTO `cmf_admin_menu` VALUES (204, 203, 1, 0, 10000, 'Admin', 'Manual', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (205, 203, 1, 0, 10000, 'Admin', 'Manual', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (206, 193, 1, 1, 10000, 'Admin', 'Cash', 'index', '', '提现记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (207, 206, 1, 0, 10000, 'Admin', 'Cash', 'setCash', '', '审核操作', '', '');
INSERT INTO `cmf_admin_menu` VALUES (208, 193, 1, 1, 10000, 'Admin', 'Coinrecord', 'index', '', '消费记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (209, 153, 1, 1, 2, 'Admin', 'Auth', 'index', '', '实名认证管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (210, 209, 1, 0, 10000, 'Admin', 'Auth', 'setstatus', '', '审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (211, 0, 0, 1, 4, 'Admin', 'Chat', 'default', '', '私聊管理', 'telegram', '');
INSERT INTO `cmf_admin_menu` VALUES (218, 211, 1, 1, 10000, 'Admin', 'feevideo', 'index', '', '视频价格', '', '');
INSERT INTO `cmf_admin_menu` VALUES (219, 218, 1, 0, 10000, 'Admin', 'Feevideo', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (220, 218, 1, 0, 10000, 'Admin', 'Feevideo', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (221, 218, 1, 0, 10000, 'Admin', 'Feevideo', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (222, 218, 1, 0, 10000, 'Admin', 'Feevideo', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (223, 218, 1, 0, 10000, 'Admin', 'Feevideo', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (224, 0, 0, 1, 7, 'Admin', 'Gift', 'default', '', '礼物管理', 'gift', '');
INSERT INTO `cmf_admin_menu` VALUES (225, 224, 1, 1, 10000, 'Admin', 'Gift', 'index', '', '礼物列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (226, 225, 1, 0, 10000, 'Admin', 'Gift', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (227, 225, 1, 0, 10000, 'Admin', 'Gift', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (228, 225, 1, 0, 10000, 'Admin', 'Gift', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (229, 225, 1, 0, 10000, 'Admin', 'Gift', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (230, 225, 1, 0, 10000, 'Admin', 'Gift', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (231, 225, 1, 0, 10000, 'Admin', 'Gift', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (237, 0, 1, 1, 5, 'Admin', 'conversa', 'index', '', '通话记录', 'tty', '');
INSERT INTO `cmf_admin_menu` VALUES (239, 154, 1, 0, 10000, 'user', 'AdminIndex', 'cancelAuth', '', '取消认证', '', '');
INSERT INTO `cmf_admin_menu` VALUES (254, 0, 1, 1, 2, 'Admin', 'Photo', 'default', '', '相册管理', 'photo', '');
INSERT INTO `cmf_admin_menu` VALUES (261, 254, 1, 1, 2, 'Admin', 'Photo', 'index', '', '相册列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (262, 261, 1, 0, 10000, 'Admin', 'Photo', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (263, 261, 1, 0, 10000, 'Admin', 'Photo', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (264, 261, 1, 0, 10000, 'Admin', 'Photo', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (265, 261, 1, 0, 10000, 'Admin', 'Photo', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (266, 261, 1, 0, 10000, 'Admin', 'Photo', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (267, 261, 1, 0, 10000, 'Admin', 'Photo', 'setstatus', '', '审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (320, 0, 1, 1, 5, 'admin', 'Monitor', 'index', '', '通话监控', '', '');
INSERT INTO `cmf_admin_menu` VALUES (324, 6, 1, 1, 10000, 'admin', 'Guide', 'set', '', '引导页', '', '');
INSERT INTO `cmf_admin_menu` VALUES (325, 324, 1, 0, 10000, 'admin', 'Guide', 'setPost', '', '提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (326, 324, 1, 0, 10000, 'admin', 'Guide', 'index', '', '管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (327, 326, 1, 0, 10000, 'admin', 'Guide', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (328, 326, 1, 0, 10000, 'admin', 'Guide', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (329, 326, 1, 0, 10000, 'admin', 'Guide', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (330, 326, 1, 0, 10000, 'admin', 'Guide', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (331, 326, 1, 0, 10000, 'admin', 'Guide', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (332, 326, 1, 0, 10000, 'admin', 'Guide', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (339, 193, 1, 1, 10000, 'Admin', 'voterecord', 'index', '', '云票记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (340, 154, 1, 0, 10000, 'user', 'AdminIndex', 'add', '', '添加主播', '', '');
INSERT INTO `cmf_admin_menu` VALUES (341, 154, 1, 0, 10000, 'user', 'AdminIndex', 'addPost', '', '主播添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (342, 154, 1, 0, 10000, 'user', 'AdminIndex', 'edit', '', '编辑用户', '', '');
INSERT INTO `cmf_admin_menu` VALUES (343, 154, 1, 0, 10000, 'user', 'AdminIndex', 'editPost', '', '编辑保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (344, 154, 1, 0, 10000, 'user', 'AdminIndex', 'del', '', '删除用户', '', '');
INSERT INTO `cmf_admin_menu` VALUES (345, 153, 1, 1, 3, 'Admin', 'Authorauth', 'index', '', '主播认证管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (346, 345, 1, 0, 10000, 'Admin', 'Authorauth', 'setstatus', '', '更改主播认证状态', '', '');
INSERT INTO `cmf_admin_menu` VALUES (347, 345, 1, 0, 10000, 'Admin', 'Authorauth', 'view', '', '查看主播认证视频', '', '');
COMMIT;

-- ----------------------------
-- Table structure for cmf_answer_rate
-- ----------------------------
DROP TABLE IF EXISTS `cmf_answer_rate`;
CREATE TABLE `cmf_answer_rate` (
  `uid` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `answer` bigint(20) unsigned DEFAULT '0' COMMENT '接通数量',
  `total` bigint(20) unsigned DEFAULT '0' COMMENT '总数量',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_answer_rate
-- ----------------------------
BEGIN;
INSERT INTO `cmf_answer_rate` VALUES (2, 2, 3);
INSERT INTO `cmf_answer_rate` VALUES (5, 3, 3);
INSERT INTO `cmf_answer_rate` VALUES (8, 0, 0);
INSERT INTO `cmf_answer_rate` VALUES (14, 0, 0);
INSERT INTO `cmf_answer_rate` VALUES (16, 2, 2);
INSERT INTO `cmf_answer_rate` VALUES (18, 1, 1);
INSERT INTO `cmf_answer_rate` VALUES (19, 1, 1);
INSERT INTO `cmf_answer_rate` VALUES (20, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for cmf_asset
-- ----------------------------
DROP TABLE IF EXISTS `cmf_asset`;
CREATE TABLE `cmf_asset` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `file_size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小,单位B',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:可用,0:不可用',
  `download_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `file_key` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '文件惟一码',
  `filename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `file_path` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '文件路径,相对于upload目录,可以为url',
  `file_md5` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '文件md5值',
  `file_sha1` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `suffix` varchar(10) NOT NULL DEFAULT '' COMMENT '文件后缀名,不包括点',
  `more` text COMMENT '其它详细信息,JSON格式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='资源表';

-- ----------------------------
-- Records of cmf_asset
-- ----------------------------
BEGIN;
INSERT INTO `cmf_asset` VALUES (1, 1, 35298, 1647393098, 1, 0, 'a3bce97d6cf7fc84f9b27b200b0e287342bf25ccfe4ba24843ade2a33c232fe5', '1.jpg', 'admin/20220316/674be105ab6d92e1535b02443b467969.jpg', 'a3bce97d6cf7fc84f9b27b200b0e2873', 'bdbbfa803281bc9c014a3c1c7434b0b6ff5b56ab', 'jpg', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cmf_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_access`;
CREATE TABLE `cmf_auth_access` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL COMMENT '角色',
  `rule_name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `rule_name` (`rule_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限授权表';

-- ----------------------------
-- Records of cmf_auth_access
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_rule`;
CREATE TABLE `cmf_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `app` varchar(40) NOT NULL DEFAULT '' COMMENT '规则所属app',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) NOT NULL DEFAULT '' COMMENT '额外url参数',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述',
  `condition` varchar(200) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `module` (`app`,`status`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限规则表';

-- ----------------------------
-- Records of cmf_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `cmf_auth_rule` VALUES (1, 1, 'admin', 'admin_url', 'admin/Hook/index', '', '钩子管理', '');
INSERT INTO `cmf_auth_rule` VALUES (2, 1, 'admin', 'admin_url', 'admin/Hook/plugins', '', '钩子插件管理', '');
INSERT INTO `cmf_auth_rule` VALUES (3, 1, 'admin', 'admin_url', 'admin/Hook/pluginListOrder', '', '钩子插件排序', '');
INSERT INTO `cmf_auth_rule` VALUES (4, 1, 'admin', 'admin_url', 'admin/Hook/sync', '', '同步钩子', '');
INSERT INTO `cmf_auth_rule` VALUES (5, 1, 'admin', 'admin_url', 'admin/Link/index', '', '友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (6, 1, 'admin', 'admin_url', 'admin/Link/add', '', '添加友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (7, 1, 'admin', 'admin_url', 'admin/Link/addPost', '', '添加友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (8, 1, 'admin', 'admin_url', 'admin/Link/edit', '', '编辑友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (9, 1, 'admin', 'admin_url', 'admin/Link/editPost', '', '编辑友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (10, 1, 'admin', 'admin_url', 'admin/Link/delete', '', '删除友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (11, 1, 'admin', 'admin_url', 'admin/Link/listOrder', '', '友情链接排序', '');
INSERT INTO `cmf_auth_rule` VALUES (12, 1, 'admin', 'admin_url', 'admin/Link/toggle', '', '友情链接显示隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (13, 1, 'admin', 'admin_url', 'admin/Mailer/index', '', '邮箱配置', '');
INSERT INTO `cmf_auth_rule` VALUES (14, 1, 'admin', 'admin_url', 'admin/Mailer/indexPost', '', '邮箱配置提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (15, 1, 'admin', 'admin_url', 'admin/Mailer/template', '', '邮件模板', '');
INSERT INTO `cmf_auth_rule` VALUES (16, 1, 'admin', 'admin_url', 'admin/Mailer/templatePost', '', '邮件模板提交', '');
INSERT INTO `cmf_auth_rule` VALUES (17, 1, 'admin', 'admin_url', 'admin/Mailer/test', '', '邮件发送测试', '');
INSERT INTO `cmf_auth_rule` VALUES (18, 1, 'admin', 'admin_url', 'admin/Menu/index', '', '后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (19, 1, 'admin', 'admin_url', 'admin/Menu/lists', '', '所有菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (20, 1, 'admin', 'admin_url', 'admin/Menu/add', '', '后台菜单添加', '');
INSERT INTO `cmf_auth_rule` VALUES (21, 1, 'admin', 'admin_url', 'admin/Menu/addPost', '', '后台菜单添加提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (22, 1, 'admin', 'admin_url', 'admin/Menu/edit', '', '后台菜单编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (23, 1, 'admin', 'admin_url', 'admin/Menu/editPost', '', '后台菜单编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (24, 1, 'admin', 'admin_url', 'admin/Menu/delete', '', '后台菜单删除', '');
INSERT INTO `cmf_auth_rule` VALUES (25, 1, 'admin', 'admin_url', 'admin/Menu/listOrder', '', '后台菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (26, 1, 'admin', 'admin_url', 'admin/Menu/getActions', '', '导入新后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (27, 1, 'admin', 'admin_url', 'admin/Nav/index', '', '导航管理', '');
INSERT INTO `cmf_auth_rule` VALUES (28, 1, 'admin', 'admin_url', 'admin/Nav/add', '', '添加导航', '');
INSERT INTO `cmf_auth_rule` VALUES (29, 1, 'admin', 'admin_url', 'admin/Nav/addPost', '', '添加导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (30, 1, 'admin', 'admin_url', 'admin/Nav/edit', '', '编辑导航', '');
INSERT INTO `cmf_auth_rule` VALUES (31, 1, 'admin', 'admin_url', 'admin/Nav/editPost', '', '编辑导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (32, 1, 'admin', 'admin_url', 'admin/Nav/delete', '', '删除导航', '');
INSERT INTO `cmf_auth_rule` VALUES (33, 1, 'admin', 'admin_url', 'admin/NavMenu/index', '', '导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (34, 1, 'admin', 'admin_url', 'admin/NavMenu/add', '', '添加导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (35, 1, 'admin', 'admin_url', 'admin/NavMenu/addPost', '', '添加导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (36, 1, 'admin', 'admin_url', 'admin/NavMenu/edit', '', '编辑导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (37, 1, 'admin', 'admin_url', 'admin/NavMenu/editPost', '', '编辑导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (38, 1, 'admin', 'admin_url', 'admin/NavMenu/delete', '', '删除导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (39, 1, 'admin', 'admin_url', 'admin/NavMenu/listOrder', '', '导航菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (40, 1, 'admin', 'admin_url', 'admin/Plugin/default', '', '插件中心', '');
INSERT INTO `cmf_auth_rule` VALUES (41, 1, 'admin', 'admin_url', 'admin/Plugin/index', '', '插件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (42, 1, 'admin', 'admin_url', 'admin/Plugin/toggle', '', '插件启用禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (43, 1, 'admin', 'admin_url', 'admin/Plugin/setting', '', '插件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (44, 1, 'admin', 'admin_url', 'admin/Plugin/settingPost', '', '插件设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (45, 1, 'admin', 'admin_url', 'admin/Plugin/install', '', '插件安装', '');
INSERT INTO `cmf_auth_rule` VALUES (46, 1, 'admin', 'admin_url', 'admin/Plugin/update', '', '插件更新', '');
INSERT INTO `cmf_auth_rule` VALUES (47, 1, 'admin', 'admin_url', 'admin/Plugin/uninstall', '', '卸载插件', '');
INSERT INTO `cmf_auth_rule` VALUES (48, 1, 'admin', 'admin_url', 'admin/Rbac/index', '', '角色管理', '');
INSERT INTO `cmf_auth_rule` VALUES (49, 1, 'admin', 'admin_url', 'admin/Rbac/roleAdd', '', '添加角色', '');
INSERT INTO `cmf_auth_rule` VALUES (50, 1, 'admin', 'admin_url', 'admin/Rbac/roleAddPost', '', '添加角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (51, 1, 'admin', 'admin_url', 'admin/Rbac/roleEdit', '', '编辑角色', '');
INSERT INTO `cmf_auth_rule` VALUES (52, 1, 'admin', 'admin_url', 'admin/Rbac/roleEditPost', '', '编辑角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (53, 1, 'admin', 'admin_url', 'admin/Rbac/roleDelete', '', '删除角色', '');
INSERT INTO `cmf_auth_rule` VALUES (54, 1, 'admin', 'admin_url', 'admin/Rbac/authorize', '', '设置角色权限', '');
INSERT INTO `cmf_auth_rule` VALUES (55, 1, 'admin', 'admin_url', 'admin/Rbac/authorizePost', '', '角色授权提交', '');
INSERT INTO `cmf_auth_rule` VALUES (56, 1, 'admin', 'admin_url', 'admin/RecycleBin/index', '', '回收站', '');
INSERT INTO `cmf_auth_rule` VALUES (57, 1, 'admin', 'admin_url', 'admin/RecycleBin/restore', '', '回收站还原', '');
INSERT INTO `cmf_auth_rule` VALUES (58, 1, 'admin', 'admin_url', 'admin/RecycleBin/delete', '', '回收站彻底删除', '');
INSERT INTO `cmf_auth_rule` VALUES (59, 1, 'admin', 'admin_url', 'admin/Route/index', '', 'URL美化', '');
INSERT INTO `cmf_auth_rule` VALUES (60, 1, 'admin', 'admin_url', 'admin/Route/add', '', '添加路由规则', '');
INSERT INTO `cmf_auth_rule` VALUES (61, 1, 'admin', 'admin_url', 'admin/Route/addPost', '', '添加路由规则提交', '');
INSERT INTO `cmf_auth_rule` VALUES (62, 1, 'admin', 'admin_url', 'admin/Route/edit', '', '路由规则编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (63, 1, 'admin', 'admin_url', 'admin/Route/editPost', '', '路由规则编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (64, 1, 'admin', 'admin_url', 'admin/Route/delete', '', '路由规则删除', '');
INSERT INTO `cmf_auth_rule` VALUES (65, 1, 'admin', 'admin_url', 'admin/Route/ban', '', '路由规则禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (66, 1, 'admin', 'admin_url', 'admin/Route/open', '', '路由规则启用', '');
INSERT INTO `cmf_auth_rule` VALUES (67, 1, 'admin', 'admin_url', 'admin/Route/listOrder', '', '路由规则排序', '');
INSERT INTO `cmf_auth_rule` VALUES (68, 1, 'admin', 'admin_url', 'admin/Route/select', '', '选择URL', '');
INSERT INTO `cmf_auth_rule` VALUES (69, 1, 'admin', 'admin_url', 'admin/Setting/default', '', '设置', '');
INSERT INTO `cmf_auth_rule` VALUES (70, 1, 'admin', 'admin_url', 'admin/Setting/site', '', '网站信息', '');
INSERT INTO `cmf_auth_rule` VALUES (71, 1, 'admin', 'admin_url', 'admin/Setting/sitePost', '', '网站信息设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (72, 1, 'admin', 'admin_url', 'admin/Setting/password', '', '密码修改', '');
INSERT INTO `cmf_auth_rule` VALUES (73, 1, 'admin', 'admin_url', 'admin/Setting/passwordPost', '', '密码修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (74, 1, 'admin', 'admin_url', 'admin/Setting/upload', '', '上传设置', '');
INSERT INTO `cmf_auth_rule` VALUES (75, 1, 'admin', 'admin_url', 'admin/Setting/uploadPost', '', '上传设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (76, 1, 'admin', 'admin_url', 'admin/Setting/clearCache', '', '清除缓存', '');
INSERT INTO `cmf_auth_rule` VALUES (77, 1, 'admin', 'admin_url', 'admin/Slide/index', '', '幻灯片管理', '');
INSERT INTO `cmf_auth_rule` VALUES (78, 1, 'admin', 'admin_url', 'admin/Slide/add', '', '添加幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (79, 1, 'admin', 'admin_url', 'admin/Slide/addPost', '', '添加幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (80, 1, 'admin', 'admin_url', 'admin/Slide/edit', '', '编辑幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (81, 1, 'admin', 'admin_url', 'admin/Slide/editPost', '', '编辑幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (82, 1, 'admin', 'admin_url', 'admin/Slide/delete', '', '删除幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (83, 1, 'admin', 'admin_url', 'admin/SlideItem/index', '', '幻灯片页面列表', '');
INSERT INTO `cmf_auth_rule` VALUES (84, 1, 'admin', 'admin_url', 'admin/SlideItem/add', '', '幻灯片页面添加', '');
INSERT INTO `cmf_auth_rule` VALUES (85, 1, 'admin', 'admin_url', 'admin/SlideItem/addPost', '', '幻灯片页面添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (86, 1, 'admin', 'admin_url', 'admin/SlideItem/edit', '', '幻灯片页面编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (87, 1, 'admin', 'admin_url', 'admin/SlideItem/editPost', '', '幻灯片页面编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (88, 1, 'admin', 'admin_url', 'admin/SlideItem/delete', '', '幻灯片页面删除', '');
INSERT INTO `cmf_auth_rule` VALUES (89, 1, 'admin', 'admin_url', 'admin/SlideItem/ban', '', '幻灯片页面隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (90, 1, 'admin', 'admin_url', 'admin/SlideItem/cancelBan', '', '幻灯片页面显示', '');
INSERT INTO `cmf_auth_rule` VALUES (91, 1, 'admin', 'admin_url', 'admin/SlideItem/listOrder', '', '幻灯片页面排序', '');
INSERT INTO `cmf_auth_rule` VALUES (92, 1, 'admin', 'admin_url', 'admin/Storage/index', '', '文件存储', '');
INSERT INTO `cmf_auth_rule` VALUES (93, 1, 'admin', 'admin_url', 'admin/Storage/settingPost', '', '文件存储设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (94, 1, 'admin', 'admin_url', 'admin/Theme/index', '', '模板管理', '');
INSERT INTO `cmf_auth_rule` VALUES (95, 1, 'admin', 'admin_url', 'admin/Theme/install', '', '安装模板', '');
INSERT INTO `cmf_auth_rule` VALUES (96, 1, 'admin', 'admin_url', 'admin/Theme/uninstall', '', '卸载模板', '');
INSERT INTO `cmf_auth_rule` VALUES (97, 1, 'admin', 'admin_url', 'admin/Theme/installTheme', '', '模板安装', '');
INSERT INTO `cmf_auth_rule` VALUES (98, 1, 'admin', 'admin_url', 'admin/Theme/update', '', '模板更新', '');
INSERT INTO `cmf_auth_rule` VALUES (99, 1, 'admin', 'admin_url', 'admin/Theme/active', '', '启用模板', '');
INSERT INTO `cmf_auth_rule` VALUES (100, 1, 'admin', 'admin_url', 'admin/Theme/files', '', '模板文件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (101, 1, 'admin', 'admin_url', 'admin/Theme/fileSetting', '', '模板文件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (102, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayData', '', '模板文件数组数据列表', '');
INSERT INTO `cmf_auth_rule` VALUES (103, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEdit', '', '模板文件数组数据添加编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (104, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEditPost', '', '模板文件数组数据添加编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (105, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataDelete', '', '模板文件数组数据删除', '');
INSERT INTO `cmf_auth_rule` VALUES (106, 1, 'admin', 'admin_url', 'admin/Theme/settingPost', '', '模板文件编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (107, 1, 'admin', 'admin_url', 'admin/Theme/dataSource', '', '模板文件设置数据源', '');
INSERT INTO `cmf_auth_rule` VALUES (108, 1, 'admin', 'admin_url', 'admin/Theme/design', '', '模板设计', '');
INSERT INTO `cmf_auth_rule` VALUES (109, 1, 'admin', 'admin_url', 'admin/User/default', '', '管理组', '');
INSERT INTO `cmf_auth_rule` VALUES (110, 1, 'admin', 'admin_url', 'admin/User/index', '', '管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (111, 1, 'admin', 'admin_url', 'admin/User/add', '', '管理员添加', '');
INSERT INTO `cmf_auth_rule` VALUES (112, 1, 'admin', 'admin_url', 'admin/User/addPost', '', '管理员添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (113, 1, 'admin', 'admin_url', 'admin/User/edit', '', '管理员编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (114, 1, 'admin', 'admin_url', 'admin/User/editPost', '', '管理员编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (115, 1, 'admin', 'admin_url', 'admin/User/userInfo', '', '个人信息', '');
INSERT INTO `cmf_auth_rule` VALUES (116, 1, 'admin', 'admin_url', 'admin/User/userInfoPost', '', '管理员个人信息修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (117, 1, 'admin', 'admin_url', 'admin/User/delete', '', '管理员删除', '');
INSERT INTO `cmf_auth_rule` VALUES (118, 1, 'admin', 'admin_url', 'admin/User/ban', '', '停用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (119, 1, 'admin', 'admin_url', 'admin/User/cancelBan', '', '启用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (120, 1, 'portal', 'admin_url', 'portal/AdminArticle/index', '', '文章管理', '');
INSERT INTO `cmf_auth_rule` VALUES (121, 1, 'portal', 'admin_url', 'portal/AdminArticle/add', '', '添加文章', '');
INSERT INTO `cmf_auth_rule` VALUES (122, 1, 'portal', 'admin_url', 'portal/AdminArticle/addPost', '', '添加文章提交', '');
INSERT INTO `cmf_auth_rule` VALUES (123, 1, 'portal', 'admin_url', 'portal/AdminArticle/edit', '', '编辑文章', '');
INSERT INTO `cmf_auth_rule` VALUES (124, 1, 'portal', 'admin_url', 'portal/AdminArticle/editPost', '', '编辑文章提交', '');
INSERT INTO `cmf_auth_rule` VALUES (125, 1, 'portal', 'admin_url', 'portal/AdminArticle/delete', '', '文章删除', '');
INSERT INTO `cmf_auth_rule` VALUES (126, 1, 'portal', 'admin_url', 'portal/AdminArticle/publish', '', '文章发布', '');
INSERT INTO `cmf_auth_rule` VALUES (127, 1, 'portal', 'admin_url', 'portal/AdminArticle/top', '', '文章置顶', '');
INSERT INTO `cmf_auth_rule` VALUES (128, 1, 'portal', 'admin_url', 'portal/AdminArticle/recommend', '', '文章推荐', '');
INSERT INTO `cmf_auth_rule` VALUES (129, 1, 'portal', 'admin_url', 'portal/AdminArticle/listOrder', '', '文章排序', '');
INSERT INTO `cmf_auth_rule` VALUES (130, 1, 'portal', 'admin_url', 'portal/AdminCategory/index', '', '分类管理', '');
INSERT INTO `cmf_auth_rule` VALUES (131, 1, 'portal', 'admin_url', 'portal/AdminCategory/add', '', '添加文章分类', '');
INSERT INTO `cmf_auth_rule` VALUES (132, 1, 'portal', 'admin_url', 'portal/AdminCategory/addPost', '', '添加文章分类提交', '');
INSERT INTO `cmf_auth_rule` VALUES (133, 1, 'portal', 'admin_url', 'portal/AdminCategory/edit', '', '编辑文章分类', '');
INSERT INTO `cmf_auth_rule` VALUES (134, 1, 'portal', 'admin_url', 'portal/AdminCategory/editPost', '', '编辑文章分类提交', '');
INSERT INTO `cmf_auth_rule` VALUES (135, 1, 'portal', 'admin_url', 'portal/AdminCategory/select', '', '文章分类选择对话框', '');
INSERT INTO `cmf_auth_rule` VALUES (136, 1, 'portal', 'admin_url', 'portal/AdminCategory/listOrder', '', '文章分类排序', '');
INSERT INTO `cmf_auth_rule` VALUES (137, 1, 'portal', 'admin_url', 'portal/AdminCategory/delete', '', '删除文章分类', '');
INSERT INTO `cmf_auth_rule` VALUES (138, 1, 'portal', 'admin_url', 'portal/AdminIndex/default', '', '内容管理', '');
INSERT INTO `cmf_auth_rule` VALUES (139, 1, 'portal', 'admin_url', 'portal/AdminPage/index', '', '页面管理', '');
INSERT INTO `cmf_auth_rule` VALUES (140, 1, 'portal', 'admin_url', 'portal/AdminPage/add', '', '添加页面', '');
INSERT INTO `cmf_auth_rule` VALUES (141, 1, 'portal', 'admin_url', 'portal/AdminPage/addPost', '', '添加页面提交', '');
INSERT INTO `cmf_auth_rule` VALUES (142, 1, 'portal', 'admin_url', 'portal/AdminPage/edit', '', '编辑页面', '');
INSERT INTO `cmf_auth_rule` VALUES (143, 1, 'portal', 'admin_url', 'portal/AdminPage/editPost', '', '编辑页面提交', '');
INSERT INTO `cmf_auth_rule` VALUES (144, 1, 'portal', 'admin_url', 'portal/AdminPage/delete', '', '删除页面', '');
INSERT INTO `cmf_auth_rule` VALUES (145, 1, 'portal', 'admin_url', 'portal/AdminTag/index', '', '文章标签', '');
INSERT INTO `cmf_auth_rule` VALUES (146, 1, 'portal', 'admin_url', 'portal/AdminTag/add', '', '添加文章标签', '');
INSERT INTO `cmf_auth_rule` VALUES (147, 1, 'portal', 'admin_url', 'portal/AdminTag/addPost', '', '添加文章标签提交', '');
INSERT INTO `cmf_auth_rule` VALUES (148, 1, 'portal', 'admin_url', 'portal/AdminTag/upStatus', '', '更新标签状态', '');
INSERT INTO `cmf_auth_rule` VALUES (149, 1, 'portal', 'admin_url', 'portal/AdminTag/delete', '', '删除文章标签', '');
INSERT INTO `cmf_auth_rule` VALUES (150, 1, 'user', 'admin_url', 'user/AdminAsset/index', '', '资源管理', '');
INSERT INTO `cmf_auth_rule` VALUES (151, 1, 'user', 'admin_url', 'user/AdminAsset/delete', '', '删除文件', '');
INSERT INTO `cmf_auth_rule` VALUES (152, 1, 'user', 'admin_url', 'user/AdminIndex/default', '', '用户管理', '');
INSERT INTO `cmf_auth_rule` VALUES (153, 1, 'user', 'admin_url', 'user/AdminIndex/default1', '', '用户组', '');
INSERT INTO `cmf_auth_rule` VALUES (154, 1, 'user', 'admin_url', 'user/AdminIndex/index', '', '本站用户', '');
INSERT INTO `cmf_auth_rule` VALUES (155, 1, 'user', 'admin_url', 'user/AdminIndex/ban', '', '本站用户拉黑', '');
INSERT INTO `cmf_auth_rule` VALUES (156, 1, 'user', 'admin_url', 'user/AdminIndex/cancelBan', '', '本站用户启用', '');
INSERT INTO `cmf_auth_rule` VALUES (157, 1, 'user', 'admin_url', 'user/AdminOauth/index', '', '第三方用户', '');
INSERT INTO `cmf_auth_rule` VALUES (158, 1, 'user', 'admin_url', 'user/AdminOauth/delete', '', '删除第三方用户绑定', '');
INSERT INTO `cmf_auth_rule` VALUES (159, 1, 'user', 'admin_url', 'user/AdminUserAction/index', '', '用户操作管理', '');
INSERT INTO `cmf_auth_rule` VALUES (160, 1, 'user', 'admin_url', 'user/AdminUserAction/edit', '', '编辑用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (161, 1, 'user', 'admin_url', 'user/AdminUserAction/editPost', '', '编辑用户操作提交', '');
INSERT INTO `cmf_auth_rule` VALUES (162, 1, 'user', 'admin_url', 'user/AdminUserAction/sync', '', '同步用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (163, 1, 'Admin', 'admin_url', 'Admin/Setting/configpri', '', '私密设置', '');
INSERT INTO `cmf_auth_rule` VALUES (164, 1, 'Admin', 'admin_url', 'Admin/Setting/configpriPost', '', '提交', '');
INSERT INTO `cmf_auth_rule` VALUES (165, 1, 'Admin', 'admin_url', 'Admin/Level/default', '', '等级管理', '');
INSERT INTO `cmf_auth_rule` VALUES (166, 1, 'Admin', 'admin_url', 'Admin/Level/index', '', '经验等级', '');
INSERT INTO `cmf_auth_rule` VALUES (167, 1, 'Admin', 'admin_url', 'Admin/Level/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (168, 1, 'Admin', 'admin_url', 'Admin/Level/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (169, 1, 'Admin', 'admin_url', 'Admin/Level/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (170, 1, 'Admin', 'admin_url', 'Admin/Level/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (171, 1, 'Admin', 'admin_url', 'Admin/Level/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (172, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/index', '', '主播等级', '');
INSERT INTO `cmf_auth_rule` VALUES (173, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (174, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (175, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (176, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (177, 1, 'Admin', 'admin_url', 'Admin/Levelanchor/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (178, 1, 'Admin', 'admin_url', 'Admin/Label/default', '', '标签管理', '');
INSERT INTO `cmf_auth_rule` VALUES (179, 1, 'Admin', 'admin_url', 'Admin/Label/index', '', '形象标签', '');
INSERT INTO `cmf_auth_rule` VALUES (180, 1, 'Admin', 'admin_url', 'Admin/Label/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (181, 1, 'Admin', 'admin_url', 'Admin/Label/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (182, 1, 'Admin', 'admin_url', 'Admin/Label/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (183, 1, 'Admin', 'admin_url', 'Admin/Label/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (184, 1, 'Admin', 'admin_url', 'Admin/Label/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (185, 1, 'Admin', 'admin_url', 'Admin/Label/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (186, 1, 'Admin', 'admin_url', 'Admin/Evaluate/index', '', '评价标签', '');
INSERT INTO `cmf_auth_rule` VALUES (187, 1, 'Admin', 'admin_url', 'Admin/Evaluate/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (188, 1, 'Admin', 'admin_url', 'Admin/Evaluate/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (189, 1, 'Admin', 'admin_url', 'Admin/Evaluate/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (190, 1, 'Admin', 'admin_url', 'Admin/Evaluate/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (191, 1, 'Admin', 'admin_url', 'Admin/Evaluate/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (192, 1, 'Admin', 'admin_url', 'Admin/Evaluate/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (193, 1, 'Admin', 'admin_url', 'Admin/Finance/default', '', '财务管理', '');
INSERT INTO `cmf_auth_rule` VALUES (194, 1, 'Admin', 'admin_url', 'Admin/Chargerule/index', '', '充值规则', '');
INSERT INTO `cmf_auth_rule` VALUES (195, 1, 'Admin', 'admin_url', 'Admin/Chargerule/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (196, 1, 'Admin', 'admin_url', 'Admin/Chargerule/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (197, 1, 'Admin', 'admin_url', 'Admin/Chargerule/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (198, 1, 'Admin', 'admin_url', 'Admin/Chargerule/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (199, 1, 'Admin', 'admin_url', 'Admin/Chargerule/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (200, 1, 'Admin', 'admin_url', 'Admin/Chargerule/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (201, 1, 'Admin', 'admin_url', 'Admin/Charge/Index', '', '充值记录', '');
INSERT INTO `cmf_auth_rule` VALUES (202, 1, 'Admin', 'admin_url', 'Admin/Charge/setPay', '', '确认支付', '');
INSERT INTO `cmf_auth_rule` VALUES (203, 1, 'Admin', 'admin_url', 'Admin/Manual/index', '', '手动充值', '');
INSERT INTO `cmf_auth_rule` VALUES (204, 1, 'Admin', 'admin_url', 'Admin/Manual/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (205, 1, 'Admin', 'admin_url', 'Admin/Manual/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (206, 1, 'Admin', 'admin_url', 'Admin/Cash/index', '', '提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (207, 1, 'Admin', 'admin_url', 'Admin/Cash/setCash', '', '审核操作', '');
INSERT INTO `cmf_auth_rule` VALUES (208, 1, 'Admin', 'admin_url', 'Admin/Coinrecord/index', '', '消费记录', '');
INSERT INTO `cmf_auth_rule` VALUES (209, 1, 'Admin', 'admin_url', 'Admin/Auth/index', '', '实名认证管理', '');
INSERT INTO `cmf_auth_rule` VALUES (210, 1, 'Admin', 'admin_url', 'Admin/Auth/setstatus', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (211, 1, 'Admin', 'admin_url', 'Admin/Chat/default', '', '私聊管理', '');
INSERT INTO `cmf_auth_rule` VALUES (212, 1, 'Admin', 'admin_url', 'Admin/feevoice/index', '', '语音价格', '');
INSERT INTO `cmf_auth_rule` VALUES (213, 1, 'Admin', 'admin_url', 'Admin/feevoice/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (214, 1, 'Admin', 'admin_url', 'Admin/feevoice/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (215, 1, 'Admin', 'admin_url', 'Admin/feevoice/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (216, 1, 'Admin', 'admin_url', 'Admin/feevoice/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (217, 1, 'Admin', 'admin_url', 'Admin/feevoice/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (218, 1, 'Admin', 'admin_url', 'Admin/feevideo/index', '', '视频价格', '');
INSERT INTO `cmf_auth_rule` VALUES (219, 1, 'Admin', 'admin_url', 'Admin/Feevideo/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (220, 1, 'Admin', 'admin_url', 'Admin/Feevideo/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (221, 1, 'Admin', 'admin_url', 'Admin/Feevideo/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (222, 1, 'Admin', 'admin_url', 'Admin/Feevideo/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (223, 1, 'Admin', 'admin_url', 'Admin/Feevideo/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (224, 1, 'Admin', 'admin_url', 'Admin/Gift/default', '', '礼物管理', '');
INSERT INTO `cmf_auth_rule` VALUES (225, 1, 'Admin', 'admin_url', 'Admin/Gift/index', '', '礼物列表', '');
INSERT INTO `cmf_auth_rule` VALUES (226, 1, 'Admin', 'admin_url', 'Admin/Gift/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (227, 1, 'Admin', 'admin_url', 'Admin/Gift/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (228, 1, 'Admin', 'admin_url', 'Admin/Gift/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (229, 1, 'Admin', 'admin_url', 'Admin/Gift/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (230, 1, 'Admin', 'admin_url', 'Admin/Gift/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (231, 1, 'Admin', 'admin_url', 'Admin/Gift/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (232, 1, 'Admin', 'admin_url', 'Admin/Sysnotice/default', '', '系统通知', '');
INSERT INTO `cmf_auth_rule` VALUES (233, 1, 'Admin', 'admin_url', 'Admin/Sysnotice/index', '', '通知列表', '');
INSERT INTO `cmf_auth_rule` VALUES (234, 1, 'Admin', 'admin_url', 'Admin/Sysnotice/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (235, 1, 'Admin', 'admin_url', 'Admin/Sysnotice/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (236, 1, 'Admin', 'admin_url', 'Admin/Sysnotice/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (237, 1, 'Admin', 'admin_url', 'Admin/conversa/index', '', '通话记录', '');
INSERT INTO `cmf_auth_rule` VALUES (238, 1, 'user', 'admin_url', 'user/AdminIndex/setrecommend', '', '设置推荐值', '');
INSERT INTO `cmf_auth_rule` VALUES (239, 1, 'user', 'admin_url', 'user/AdminIndex/cancelAuth', '', '取消认证', '');
INSERT INTO `cmf_auth_rule` VALUES (240, 1, 'Admin', 'admin_url', 'Admin/Video/default', '', '视频管理', '');
INSERT INTO `cmf_auth_rule` VALUES (241, 1, 'Admin', 'admin_url', 'Admin/Videofee/index', '', '私密价格', '');
INSERT INTO `cmf_auth_rule` VALUES (242, 1, 'Admin', 'admin_url', 'Admin/Videofee/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (243, 1, 'Admin', 'admin_url', 'Admin/Videofee/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (244, 1, 'Admin', 'admin_url', 'Admin/Videofee/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (245, 1, 'Admin', 'admin_url', 'Admin/Videofee/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (246, 1, 'Admin', 'admin_url', 'Admin/Videofee/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (247, 1, 'Admin', 'admin_url', 'Admin/Video/index', '', '视频列表', '');
INSERT INTO `cmf_auth_rule` VALUES (248, 1, 'Admin', 'admin_url', 'Admin/Video/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (249, 1, 'Admin', 'admin_url', 'Admin/Video/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (250, 1, 'Admin', 'admin_url', 'Admin/Video/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (251, 1, 'Admin', 'admin_url', 'Admin/Video/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (252, 1, 'Admin', 'admin_url', 'Admin/Video/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (253, 1, 'Admin', 'admin_url', 'Admin/Video/setstatus', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (254, 1, 'Admin', 'admin_url', 'Admin/Photo/default', '', '相册管理', '');
INSERT INTO `cmf_auth_rule` VALUES (255, 1, 'Admin', 'admin_url', 'Admin/Photofee/index', '', '私密价格', '');
INSERT INTO `cmf_auth_rule` VALUES (256, 1, 'Admin', 'admin_url', 'Admin/Photofee/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (257, 1, 'Admin', 'admin_url', 'Admin/Photofee/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (258, 1, 'Admin', 'admin_url', 'Admin/Photofee/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (259, 1, 'Admin', 'admin_url', 'Admin/Photofee/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (260, 1, 'Admin', 'admin_url', 'Admin/Photofee/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (261, 1, 'Admin', 'admin_url', 'Admin/Photo/index', '', '相册列表', '');
INSERT INTO `cmf_auth_rule` VALUES (262, 1, 'Admin', 'admin_url', 'Admin/Photo/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (263, 1, 'Admin', 'admin_url', 'Admin/Photo/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (264, 1, 'Admin', 'admin_url', 'Admin/Photo/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (265, 1, 'Admin', 'admin_url', 'Admin/Photo/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (266, 1, 'Admin', 'admin_url', 'Admin/Photo/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (267, 1, 'Admin', 'admin_url', 'Admin/Photo/setstatus', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (268, 1, 'Admin', 'admin_url', 'Admin/Vip/default', '', 'VIP管理', '');
INSERT INTO `cmf_auth_rule` VALUES (269, 1, 'Admin', 'admin_url', 'Admin/Vip/index', '', 'VIP列表', '');
INSERT INTO `cmf_auth_rule` VALUES (270, 1, 'Admin', 'admin_url', 'Admin/Vip/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (271, 1, 'Admin', 'admin_url', 'Admin/Vip/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (272, 1, 'Admin', 'admin_url', 'Admin/Vip/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (273, 1, 'Admin', 'admin_url', 'Admin/Vip/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (274, 1, 'Admin', 'admin_url', 'Admin/Vip/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (275, 1, 'Admin', 'admin_url', 'Admin/Vip/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (276, 1, 'Admin', 'admin_url', 'Admin/Vipuser/index', '', 'VIP用户', '');
INSERT INTO `cmf_auth_rule` VALUES (277, 1, 'Admin', 'admin_url', 'Admin/Vipuser/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (278, 1, 'Admin', 'admin_url', 'Admin/Vipuser/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (279, 1, 'Admin', 'admin_url', 'Admin/Vipuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (280, 1, 'Admin', 'admin_url', 'Admin/Vipuser/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (281, 1, 'Admin', 'admin_url', 'Admin/Vipuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (282, 1, 'Admin', 'admin_url', 'Admin/Viporder/index', '', 'VIP订单', '');
INSERT INTO `cmf_auth_rule` VALUES (283, 1, 'Admin', 'admin_url', 'Admin/Viporder/setpay', '', '确认', '');
INSERT INTO `cmf_auth_rule` VALUES (284, 1, 'Admin', 'admin_url', 'Admin/Agent/default', '', '邀请奖励', '');
INSERT INTO `cmf_auth_rule` VALUES (285, 1, 'Admin', 'admin_url', 'Admin/Agent/index', '', '邀请关系', '');
INSERT INTO `cmf_auth_rule` VALUES (286, 1, 'Admin', 'admin_url', 'Admin/Agent/profit', '', '邀请收益', '');
INSERT INTO `cmf_auth_rule` VALUES (287, 1, 'Admin', 'admin_url', 'Admin/Music/default', '', '音乐管理', '');
INSERT INTO `cmf_auth_rule` VALUES (288, 1, 'Admin', 'admin_url', 'Admin/Musiccat/index', '', '音乐分类', '');
INSERT INTO `cmf_auth_rule` VALUES (289, 1, 'Admin', 'admin_url', 'Admin/Musiccat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (290, 1, 'Admin', 'admin_url', 'Admin/Musiccat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (291, 1, 'Admin', 'admin_url', 'Admin/Musiccat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (292, 1, 'Admin', 'admin_url', 'Admin/Musiccat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (293, 1, 'Admin', 'admin_url', 'Admin/Musiccat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (294, 1, 'Admin', 'admin_url', 'Admin/Musiccat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (295, 1, 'Admin', 'admin_url', 'Admin/Music/index', '', '音乐列表', '');
INSERT INTO `cmf_auth_rule` VALUES (296, 1, 'Admin', 'admin_url', 'Admin/Music/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (297, 1, 'Admin', 'admin_url', 'Admin/Music/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (298, 1, 'Admin', 'admin_url', 'Admin/Music/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (299, 1, 'Admin', 'admin_url', 'Admin/Music/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (300, 1, 'Admin', 'admin_url', 'Admin/Music/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (301, 1, 'Admin', 'admin_url', 'Admin/Videoreport/default', '', '举报管理', '');
INSERT INTO `cmf_auth_rule` VALUES (302, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/index', '', '举报分类', '');
INSERT INTO `cmf_auth_rule` VALUES (303, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (304, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (305, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (306, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (307, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (308, 1, 'Admin', 'admin_url', 'Admin/Videoreportclass/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (309, 1, 'Admin', 'admin_url', 'Admin/Videoreport/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (310, 1, 'Admin', 'admin_url', 'Admin/Videoreport/setStatus', '', '标记处理', '');
INSERT INTO `cmf_auth_rule` VALUES (311, 1, 'Admin', 'admin_url', 'Admin/Dynamic/default', '', '动态管理', '');
INSERT INTO `cmf_auth_rule` VALUES (312, 1, 'Admin', 'admin_url', 'Admin/Dynamic/passindex', '', '审核通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (313, 1, 'Admin', 'admin_url', 'Admin/Dynamic/index', '', '等待审核列表', '');
INSERT INTO `cmf_auth_rule` VALUES (314, 1, 'Admin', 'admin_url', 'Admin/Dynamic/nopassindex', '', '未通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (315, 1, 'Admin', 'admin_url', 'Admin/Dynamic/lowerindex', '', '下架列表', '');
INSERT INTO `cmf_auth_rule` VALUES (316, 1, 'Admin', 'admin_url', 'Admin/Dynamicreport/default', '', '举报管理', '');
INSERT INTO `cmf_auth_rule` VALUES (317, 1, 'Admin', 'admin_url', 'Admin/Dynamicreport/classindex', '', '举报类型', '');
INSERT INTO `cmf_auth_rule` VALUES (318, 1, 'Admin', 'admin_url', 'Admin/Dynamicreport/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (319, 1, 'admin', 'admin_url', 'admin/Monitor/index', '', '通话监控', '');
INSERT INTO `cmf_auth_rule` VALUES (320, 1, 'admin', 'admin_url', 'admin/Report/display', '', '用户举报', '');
INSERT INTO `cmf_auth_rule` VALUES (321, 1, 'admin', 'admin_url', 'admin/Report/classify', '', '举报分类', '');
INSERT INTO `cmf_auth_rule` VALUES (322, 1, 'admin', 'admin_url', 'admin/Report/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (323, 1, 'admin', 'admin_url', 'admin/Guide/set', '', '引导页', '');
INSERT INTO `cmf_auth_rule` VALUES (324, 1, 'admin', 'admin_url', 'admin/Guide/setPost', '', '提交', '');
INSERT INTO `cmf_auth_rule` VALUES (325, 1, 'admin', 'admin_url', 'admin/Guide/index', '', '管理', '');
INSERT INTO `cmf_auth_rule` VALUES (326, 1, 'admin', 'admin_url', 'admin/Guide/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (327, 1, 'admin', 'admin_url', 'admin/Guide/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (328, 1, 'admin', 'admin_url', 'admin/Guide/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (329, 1, 'admin', 'admin_url', 'admin/Guide/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (330, 1, 'admin', 'admin_url', 'admin/Guide/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (331, 1, 'admin', 'admin_url', 'admin/Guide/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (332, 1, 'admin', 'admin_url', 'admin/Recommend/index', '', '推荐设置', '');
INSERT INTO `cmf_auth_rule` VALUES (333, 1, 'admin', 'admin_url', 'admin/Recommend/add', '', '推荐设置添加', '');
INSERT INTO `cmf_auth_rule` VALUES (334, 1, 'admin', 'admin_url', 'admin/Recommend/addPost', '', '推荐设置添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (335, 1, 'admin', 'admin_url', 'admin/Recommend/edit', '', '推荐设置编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (336, 1, 'admin', 'admin_url', 'admin/Recommend/editPost', '', '推荐设置编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (337, 1, 'admin', 'admin_url', 'admin/Recommend/del', '', '推荐删除', '');
INSERT INTO `cmf_auth_rule` VALUES (338, 1, 'Admin', 'admin_url', 'Admin/voterecord/index', '', '云票记录', '');
INSERT INTO `cmf_auth_rule` VALUES (339, 1, 'user', 'admin_url', 'user/AdminIndex/add', '', '添加主播', '');
INSERT INTO `cmf_auth_rule` VALUES (340, 1, 'user', 'admin_url', 'user/AdminIndex/addPost', '', '主播添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (341, 1, 'user', 'admin_url', 'user/AdminIndex/edit', '', '编辑用户', '');
INSERT INTO `cmf_auth_rule` VALUES (342, 1, 'user', 'admin_url', 'user/AdminIndex/editPost', '', '编辑保存', '');
INSERT INTO `cmf_auth_rule` VALUES (343, 1, 'user', 'admin_url', 'user/AdminIndex/del', '', '删除用户', '');
INSERT INTO `cmf_auth_rule` VALUES (344, 1, 'Admin', 'admin_url', 'Admin/Authorauth/index', '', '主播认证管理', '');
INSERT INTO `cmf_auth_rule` VALUES (345, 1, 'Admin', 'admin_url', 'Admin/Authorauth/setstatus', '', '更改主播认证状态', '');
INSERT INTO `cmf_auth_rule` VALUES (346, 1, 'Admin', 'admin_url', 'Admin/Authorauth/view', '', '查看主播认证视频', '');
INSERT INTO `cmf_auth_rule` VALUES (347, 1, 'Admin', 'admin_url', 'Admin/Videocom/index', '', '视频评论列表', '');
INSERT INTO `cmf_auth_rule` VALUES (348, 1, 'Admin', 'admin_url', 'Admin/Videocom/del', '', '删除视频评论', '');
INSERT INTO `cmf_auth_rule` VALUES (349, 1, 'Admin', 'admin_url', 'Admin/Family/default', '', '公会管理', '');
INSERT INTO `cmf_auth_rule` VALUES (350, 1, 'Admin', 'admin_url', 'Admin/Family/index', '', '公会列表', '');
INSERT INTO `cmf_auth_rule` VALUES (351, 1, 'Admin', 'admin_url', 'Admin/familyuser/index', '', '成员管理', '');
INSERT INTO `cmf_auth_rule` VALUES (352, 1, 'Admin', 'admin_url', 'Admin/familyuser/divideapply', '', '分成申请列表', '');
INSERT INTO `cmf_auth_rule` VALUES (353, 1, 'Admin', 'admin_url', 'Admin/Family/edit', '', '编辑公会', '');
INSERT INTO `cmf_auth_rule` VALUES (354, 1, 'Admin', 'admin_url', 'Admin/Family/editPost', '', '公会编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (355, 1, 'Admin', 'admin_url', 'Admin/Family/disable', '', '禁用公会', '');
INSERT INTO `cmf_auth_rule` VALUES (356, 1, 'Admin', 'admin_url', 'Admin/Family/enable', '', '启用公会', '');
INSERT INTO `cmf_auth_rule` VALUES (357, 1, 'Admin', 'admin_url', 'Admin/Family/profit', '', '公会收益', '');
INSERT INTO `cmf_auth_rule` VALUES (358, 1, 'Admin', 'admin_url', 'Admin/Family/cash', '', '提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (359, 1, 'Admin', 'admin_url', 'Admin/Family/del', '', '删除公会', '');
INSERT INTO `cmf_auth_rule` VALUES (360, 1, 'Admin', 'admin_url', 'Admin/familyuser/add', '', '成员添加', '');
INSERT INTO `cmf_auth_rule` VALUES (361, 1, 'Admin', 'admin_url', 'Admin/familyuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (362, 1, 'Admin', 'admin_url', 'Admin/familyuser/edit', '', '成员编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (363, 1, 'Admin', 'admin_url', 'Admin/familyuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (364, 1, 'Admin', 'admin_url', 'Admin/familyuser/del', '', '删除成员', '');
INSERT INTO `cmf_auth_rule` VALUES (365, 1, 'Admin', 'admin_url', 'Admin/familyuser/applyedit', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (366, 1, 'Admin', 'admin_url', 'Admin/familyuser/applyeditPost', '', '审核提交', '');
INSERT INTO `cmf_auth_rule` VALUES (367, 1, 'Admin', 'admin_url', 'Admin/familyuser/delapply', '', '删除申请', '');
COMMIT;

-- ----------------------------
-- Table structure for cmf_author_auth
-- ----------------------------
DROP TABLE IF EXISTS `cmf_author_auth`;
CREATE TABLE `cmf_author_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '封面',
  `backwall` text NOT NULL COMMENT '背景图片',
  `video` varchar(255) NOT NULL DEFAULT '' COMMENT '视频',
  `video_thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '视频封面',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '理由',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0待审核 1通过 2拒绝',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_author_auth
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_backwall
-- ----------------------------
DROP TABLE IF EXISTS `cmf_backwall`;
CREATE TABLE `cmf_backwall` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `href` varchar(255) NOT NULL DEFAULT '' COMMENT '视频链接',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型，-1封面0图片1视频',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_backwall
-- ----------------------------
BEGIN;
INSERT INTO `cmf_backwall` VALUES (1, 5, 'qiniu_android_5_20220315_171112_5923026.png', '', -1);
INSERT INTO `cmf_backwall` VALUES (2, 5, 'qiniu_android_5_20220315_171112_5973323.jpg', '', 0);
INSERT INTO `cmf_backwall` VALUES (3, 2, 'qiniu_android_2_20220317_175127_5002087.png', '', -1);
INSERT INTO `cmf_backwall` VALUES (4, 2, 'qiniu_android_2_20220317_174951_5817473.png', '', 0);
INSERT INTO `cmf_backwall` VALUES (6, 8, 'qiniu_android_8_20220317_174840_7118674.png', '', -1);
INSERT INTO `cmf_backwall` VALUES (7, 2, 'qiniu_android_2_20220317_174942_7941896.jpg', '', 0);
INSERT INTO `cmf_backwall` VALUES (8, 8, 'qiniu_android_8_20220317_174802_8484929.png', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for cmf_cash_account
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_account`;
CREATE TABLE `cmf_cash_account` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型，1表示支付宝，2表示微信，3表示银行卡',
  `account_bank` varchar(255) NOT NULL DEFAULT '' COMMENT '银行名称',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '姓名',
  `account` varchar(255) NOT NULL DEFAULT '' COMMENT '账号',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_cash_account
-- ----------------------------
BEGIN;
INSERT INTO `cmf_cash_account` VALUES (1, 18, 1, '', '张三', '123', 1647998984);
COMMIT;

-- ----------------------------
-- Table structure for cmf_cash_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_record`;
CREATE TABLE `cmf_cash_record` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `money` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '提现金额',
  `votes` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '提现映票数',
  `orderno` varchar(255) NOT NULL DEFAULT '' COMMENT '订单号',
  `trade_no` varchar(255) NOT NULL DEFAULT '' COMMENT '三方订单号',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态，0审核中，1审核通过，2审核拒绝',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '申请时间',
  `uptime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '账号类型',
  `account_bank` varchar(255) NOT NULL DEFAULT '' COMMENT '银行名称',
  `account` varchar(255) NOT NULL DEFAULT '' COMMENT '帐号',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_cash_record
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_charge_admin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_admin`;
CREATE TABLE `cmf_charge_admin` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `touid` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值对象ID',
  `coin` int(20) NOT NULL DEFAULT '0' COMMENT '钻石数',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `admin` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员',
  `ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_charge_admin
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_charge_rules
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_rules`;
CREATE TABLE `cmf_charge_rules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT 'CNY',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '非苹果钻石数',
  `coin_ios` int(11) NOT NULL DEFAULT '0' COMMENT '苹果充值钻石',
  `coin_paypal` int(11) NOT NULL DEFAULT '0' COMMENT 'Paypal支付钻石',
  `product_id` varchar(255) NOT NULL DEFAULT '' COMMENT '苹果项目ID',
  `give` int(11) NOT NULL DEFAULT '0' COMMENT '赠送钻石数',
  `list_order` int(11) NOT NULL DEFAULT '0' COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_charge_rules
-- ----------------------------
BEGIN;
INSERT INTO `cmf_charge_rules` VALUES (1, '100钻石', 0.01, 100, 999999, 999999, 'coin_600', 0, 0, 1484984685);
INSERT INTO `cmf_charge_rules` VALUES (2, '3000钻石', 30.00, 3000, 3000, 3000, 'coin_3000', 1, 1, 1484985389);
INSERT INTO `cmf_charge_rules` VALUES (3, '9800钻石', 98.00, 9800, 9800, 9800, 'coin_9800', 200, 2, 1484985412);
INSERT INTO `cmf_charge_rules` VALUES (4, '38800钻石', 388.00, 38800, 38800, 38800, 'coin_38800', 500, 3, 1484985445);
INSERT INTO `cmf_charge_rules` VALUES (5, '58800钻石', 588.00, 58800, 58800, 58800, 'coin_58800', 1200, 4, 1484985458);
COMMIT;

-- ----------------------------
-- Table structure for cmf_charge_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_user`;
CREATE TABLE `cmf_charge_user` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `touid` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值对象ID',
  `money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `coin` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '钻石数',
  `coin_give` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送钻石数',
  `orderno` varchar(50) NOT NULL DEFAULT '' COMMENT '商家订单号',
  `trade_no` varchar(100) NOT NULL DEFAULT '' COMMENT '三方平台订单号',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态,0未支付，1已支付',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '支付类型，1支付宝，2微信，3苹果，4Paypal',
  `ambient` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付环境',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_charge_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_comment
-- ----------------------------
DROP TABLE IF EXISTS `cmf_comment`;
CREATE TABLE `cmf_comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '被回复的评论id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发表评论的用户id',
  `to_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '被评论的用户id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论内容 id',
  `like_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `dislike_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '不喜欢数',
  `floor` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '楼层数',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:已审核,0:未审核',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '评论类型；1实名评论',
  `table_name` varchar(64) NOT NULL DEFAULT '' COMMENT '评论内容所在表，不带表前缀',
  `full_name` varchar(50) NOT NULL DEFAULT '' COMMENT '评论者昵称',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '评论者邮箱',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '层级关系',
  `url` text COMMENT '原文地址',
  `content` text CHARACTER SET utf8mb4 COMMENT '评论内容',
  `more` text CHARACTER SET utf8mb4 COMMENT '扩展属性',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `table_id_status` (`table_name`,`object_id`,`status`) USING BTREE,
  KEY `object_id` (`object_id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='评论表';

-- ----------------------------
-- Records of cmf_comment
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_conversa_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_conversa_log`;
CREATE TABLE `cmf_conversa_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `liveuid` bigint(20) unsigned NOT NULL COMMENT '主播ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '通话类型，1视频，2语音 ',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `showid` bigint(20) NOT NULL DEFAULT '0' COMMENT '通话标识',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态，0未开始，1进行中，2已结束',
  `starttime` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `total` bigint(20) NOT NULL DEFAULT '0' COMMENT '通话收益',
  `undivide_total` bigint(20) NOT NULL DEFAULT '0' COMMENT '未分成总收益',
  `gift` bigint(255) NOT NULL DEFAULT '0' COMMENT '礼物收益',
  `time` varchar(50) NOT NULL DEFAULT '' COMMENT '时间格式化',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_conversa_log
-- ----------------------------
BEGIN;
INSERT INTO `cmf_conversa_log` VALUES (1, 4, 5, 1, 10, 1647393656, 2, 0, 1647393659, 0, 0, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (2, 4, 5, 1, 10, 1647396790, 2, 0, 1647396794, 0, 0, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (3, 6, 5, 1, 10, 1647412313, 2, 0, 1647412325, 0, 0, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (4, 7, 5, 1, 10, 1647412948, 2, 1647412953, 1647412994, 0, 0, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (5, 7, 5, 1, 10, 1647413007, 2, 1647413011, 1647413024, 0, 0, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (6, 7, 5, 1, 10, 1647413260, 2, 1647413266, 1647413530, 0, 10, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (7, 7, 5, 1, 10, 1647413534, 2, 1647413539, 1647413617, 0, 20, 0, '2022-03-16');
INSERT INTO `cmf_conversa_log` VALUES (8, 12, 2, 1, 81, 1647507367, 2, 1647507371, 1647507390, 0, 81, 0, '2022-03-17');
INSERT INTO `cmf_conversa_log` VALUES (9, 12, 2, 1, 81, 1647507394, 2, 1647507397, 1647507408, 0, 81, 0, '2022-03-17');
INSERT INTO `cmf_conversa_log` VALUES (10, 12, 2, 1, 81, 1647507413, 2, 0, 1647507417, 0, 0, 0, '2022-03-17');
INSERT INTO `cmf_conversa_log` VALUES (11, 13, 5, 1, 10, 1647512525, 2, 0, 1647512530, 0, 0, 0, '2022-03-17');
INSERT INTO `cmf_conversa_log` VALUES (12, 13, 14, 1, 10, 1647513666, 2, 0, 1647513670, 0, 0, 0, '2022-03-17');
INSERT INTO `cmf_conversa_log` VALUES (13, 15, 2, 1, 40, 1647568445, 2, 0, 1647568448, 0, 0, 0, '2022-03-18');
INSERT INTO `cmf_conversa_log` VALUES (14, 10, 16, 1, 10, 1647599268, 2, 1647599273, 1647599283, 0, 10, 0, '2022-03-18');
INSERT INTO `cmf_conversa_log` VALUES (15, 13, 8, 1, 10, 1647600064, 2, 0, 1647600067, 0, 0, 0, '2022-03-18');
INSERT INTO `cmf_conversa_log` VALUES (16, 13, 16, 1, 10, 1647600076, 2, 1647600078, 1647600088, 0, 10, 0, '2022-03-18');
INSERT INTO `cmf_conversa_log` VALUES (17, 18, 2, 1, 40, 1647998424, 2, 0, 1647998426, 0, 0, 0, '2022-03-23');
INSERT INTO `cmf_conversa_log` VALUES (18, 18, 5, 1, 10, 1648018100, 2, 0, 1648018102, 0, 0, 0, '2022-03-23');
INSERT INTO `cmf_conversa_log` VALUES (19, 19, 18, 1, 50, 1648021718, 2, 1648021724, 1648021760, 0, 50, 0, '2022-03-23');
INSERT INTO `cmf_conversa_log` VALUES (20, 18, 19, 1, 10, 1648021783, 2, 1648021788, 1648021979, 0, 40, 0, '2022-03-23');
INSERT INTO `cmf_conversa_log` VALUES (21, 20, 19, 1, 10, 1648100810, 2, 0, 1648100812, 0, 0, 0, '2022-03-24');
INSERT INTO `cmf_conversa_log` VALUES (22, 21, 20, 1, 10, 1648100901, 2, 1648100903, 1648100913, 0, 10, 0, '2022-03-24');
COMMIT;

-- ----------------------------
-- Table structure for cmf_fee_video
-- ----------------------------
DROP TABLE IF EXISTS `cmf_fee_video`;
CREATE TABLE `cmf_fee_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '等级限制',
  `coin` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_fee_video
-- ----------------------------
BEGIN;
INSERT INTO `cmf_fee_video` VALUES (2, 2, 20);
INSERT INTO `cmf_fee_video` VALUES (3, 3, 30);
INSERT INTO `cmf_fee_video` VALUES (4, 4, 40);
INSERT INTO `cmf_fee_video` VALUES (5, 5, 50);
INSERT INTO `cmf_fee_video` VALUES (13, 1, 10);
COMMIT;

-- ----------------------------
-- Table structure for cmf_getcode_limit_ip
-- ----------------------------
DROP TABLE IF EXISTS `cmf_getcode_limit_ip`;
CREATE TABLE `cmf_getcode_limit_ip` (
  `ip` bigint(20) NOT NULL COMMENT 'ip地址',
  `date` varchar(30) NOT NULL DEFAULT '' COMMENT '时间',
  `times` tinyint(4) NOT NULL DEFAULT '0' COMMENT '次数',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_getcode_limit_ip
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_gift
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift`;
CREATE TABLE `cmf_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型,0普通礼物，1豪华礼物',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `needcoin` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '图片',
  `list_order` int(11) NOT NULL DEFAULT '10000' COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `swftype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '动画类型，0gif,1svga',
  `swf` varchar(255) NOT NULL DEFAULT '' COMMENT '动画链接',
  `swftime` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '动画时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_gift
-- ----------------------------
BEGIN;
INSERT INTO `cmf_gift` VALUES (1, 1, '爱神丘比特', 3000, 'gift_1.png', 22, 1459210850, 1, 'gift_gif_1.svga', 6.30);
INSERT INTO `cmf_gift` VALUES (11, 0, '百合', 6, 'gift_11.png', 5, 1525512840, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (14, 0, '蛋糕', 1, 'gift_14.png', 2, 1525512954, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (15, 0, '粉丝牌', 3, 'gift_15.png', 1, 1525513174, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (16, 0, '干杯', 2, 'gift_16.png', 2, 1525513203, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (17, 0, '皇冠', 1000, 'gift_17.png', 25, 1542963178, 0, '', 0.00);
COMMIT;

-- ----------------------------
-- Table structure for cmf_guide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guide`;
CREATE TABLE `cmf_guide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片/视频链接',
  `href` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `list_order` int(11) NOT NULL DEFAULT '10000' COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_guide
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_hook
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook`;
CREATE TABLE `cmf_hook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '钩子类型(1:系统钩子;2:应用钩子;3:模板钩子;4:后台模板钩子)',
  `once` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否只允许一个插件运行(0:多个;1:一个)',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `hook` varchar(50) NOT NULL DEFAULT '' COMMENT '钩子',
  `app` varchar(15) NOT NULL DEFAULT '' COMMENT '应用名(只有应用钩子才用)',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统钩子表';

-- ----------------------------
-- Records of cmf_hook
-- ----------------------------
BEGIN;
INSERT INTO `cmf_hook` VALUES (1, 1, 0, '应用初始化', 'app_init', 'cmf', '应用初始化');
INSERT INTO `cmf_hook` VALUES (2, 1, 0, '应用开始', 'app_begin', 'cmf', '应用开始');
INSERT INTO `cmf_hook` VALUES (3, 1, 0, '模块初始化', 'module_init', 'cmf', '模块初始化');
INSERT INTO `cmf_hook` VALUES (4, 1, 0, '控制器开始', 'action_begin', 'cmf', '控制器开始');
INSERT INTO `cmf_hook` VALUES (5, 1, 0, '视图输出过滤', 'view_filter', 'cmf', '视图输出过滤');
INSERT INTO `cmf_hook` VALUES (6, 1, 0, '应用结束', 'app_end', 'cmf', '应用结束');
INSERT INTO `cmf_hook` VALUES (7, 1, 0, '日志write方法', 'log_write', 'cmf', '日志write方法');
INSERT INTO `cmf_hook` VALUES (8, 1, 0, '输出结束', 'response_end', 'cmf', '输出结束');
INSERT INTO `cmf_hook` VALUES (9, 1, 0, '后台控制器初始化', 'admin_init', 'cmf', '后台控制器初始化');
INSERT INTO `cmf_hook` VALUES (10, 1, 0, '前台控制器初始化', 'home_init', 'cmf', '前台控制器初始化');
INSERT INTO `cmf_hook` VALUES (11, 1, 1, '发送手机验证码', 'send_mobile_verification_code', 'cmf', '发送手机验证码');
INSERT INTO `cmf_hook` VALUES (12, 3, 0, '模板 body标签开始', 'body_start', '', '模板 body标签开始');
INSERT INTO `cmf_hook` VALUES (13, 3, 0, '模板 head标签结束前', 'before_head_end', '', '模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (14, 3, 0, '模板底部开始', 'footer_start', '', '模板底部开始');
INSERT INTO `cmf_hook` VALUES (15, 3, 0, '模板底部开始之前', 'before_footer', '', '模板底部开始之前');
INSERT INTO `cmf_hook` VALUES (16, 3, 0, '模板底部结束之前', 'before_footer_end', '', '模板底部结束之前');
INSERT INTO `cmf_hook` VALUES (17, 3, 0, '模板 body 标签结束之前', 'before_body_end', '', '模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (18, 3, 0, '模板左边栏开始', 'left_sidebar_start', '', '模板左边栏开始');
INSERT INTO `cmf_hook` VALUES (19, 3, 0, '模板左边栏结束之前', 'before_left_sidebar_end', '', '模板左边栏结束之前');
INSERT INTO `cmf_hook` VALUES (20, 3, 0, '模板右边栏开始', 'right_sidebar_start', '', '模板右边栏开始');
INSERT INTO `cmf_hook` VALUES (21, 3, 0, '模板右边栏结束之前', 'before_right_sidebar_end', '', '模板右边栏结束之前');
INSERT INTO `cmf_hook` VALUES (22, 3, 1, '评论区', 'comment', '', '评论区');
INSERT INTO `cmf_hook` VALUES (23, 3, 1, '留言区', 'guestbook', '', '留言区');
INSERT INTO `cmf_hook` VALUES (24, 2, 0, '后台首页仪表盘', 'admin_dashboard', 'admin', '后台首页仪表盘');
INSERT INTO `cmf_hook` VALUES (25, 4, 0, '后台模板 head标签结束前', 'admin_before_head_end', '', '后台模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (26, 4, 0, '后台模板 body 标签结束之前', 'admin_before_body_end', '', '后台模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (27, 2, 0, '后台登录页面', 'admin_login', 'admin', '后台登录页面');
INSERT INTO `cmf_hook` VALUES (28, 1, 1, '前台模板切换', 'switch_theme', 'cmf', '前台模板切换');
INSERT INTO `cmf_hook` VALUES (29, 3, 0, '主要内容之后', 'after_content', '', '主要内容之后');
INSERT INTO `cmf_hook` VALUES (30, 2, 0, '文章显示之前', 'portal_before_assign_article', 'portal', '文章显示之前');
INSERT INTO `cmf_hook` VALUES (31, 2, 0, '后台文章保存之后', 'portal_admin_after_save_article', 'portal', '后台文章保存之后');
INSERT INTO `cmf_hook` VALUES (32, 2, 1, '获取上传界面', 'fetch_upload_view', 'user', '获取上传界面');
INSERT INTO `cmf_hook` VALUES (33, 3, 0, '主要内容之前', 'before_content', 'cmf', '主要内容之前');
INSERT INTO `cmf_hook` VALUES (34, 1, 0, '日志写入完成', 'log_write_done', 'cmf', '日志写入完成');
INSERT INTO `cmf_hook` VALUES (35, 1, 1, '后台模板切换', 'switch_admin_theme', 'cmf', '后台模板切换');
INSERT INTO `cmf_hook` VALUES (36, 1, 1, '验证码图片', 'captcha_image', 'cmf', '验证码图片');
INSERT INTO `cmf_hook` VALUES (37, 2, 1, '后台模板设计界面', 'admin_theme_design_view', 'admin', '后台模板设计界面');
INSERT INTO `cmf_hook` VALUES (38, 2, 1, '后台设置网站信息界面', 'admin_setting_site_view', 'admin', '后台设置网站信息界面');
INSERT INTO `cmf_hook` VALUES (39, 2, 1, '后台清除缓存界面', 'admin_setting_clear_cache_view', 'admin', '后台清除缓存界面');
INSERT INTO `cmf_hook` VALUES (40, 2, 1, '后台导航管理界面', 'admin_nav_index_view', 'admin', '后台导航管理界面');
INSERT INTO `cmf_hook` VALUES (41, 2, 1, '后台友情链接管理界面', 'admin_link_index_view', 'admin', '后台友情链接管理界面');
INSERT INTO `cmf_hook` VALUES (42, 2, 1, '后台幻灯片管理界面', 'admin_slide_index_view', 'admin', '后台幻灯片管理界面');
INSERT INTO `cmf_hook` VALUES (43, 2, 1, '后台管理员列表界面', 'admin_user_index_view', 'admin', '后台管理员列表界面');
INSERT INTO `cmf_hook` VALUES (44, 2, 1, '后台角色管理界面', 'admin_rbac_index_view', 'admin', '后台角色管理界面');
INSERT INTO `cmf_hook` VALUES (45, 2, 1, '门户后台文章管理列表界面', 'portal_admin_article_index_view', 'portal', '门户后台文章管理列表界面');
INSERT INTO `cmf_hook` VALUES (46, 2, 1, '门户后台文章分类管理列表界面', 'portal_admin_category_index_view', 'portal', '门户后台文章分类管理列表界面');
INSERT INTO `cmf_hook` VALUES (47, 2, 1, '门户后台页面管理列表界面', 'portal_admin_page_index_view', 'portal', '门户后台页面管理列表界面');
INSERT INTO `cmf_hook` VALUES (48, 2, 1, '门户后台文章标签管理列表界面', 'portal_admin_tag_index_view', 'portal', '门户后台文章标签管理列表界面');
INSERT INTO `cmf_hook` VALUES (49, 2, 1, '用户管理本站用户列表界面', 'user_admin_index_view', 'user', '用户管理本站用户列表界面');
INSERT INTO `cmf_hook` VALUES (50, 2, 1, '资源管理列表界面', 'user_admin_asset_index_view', 'user', '资源管理列表界面');
INSERT INTO `cmf_hook` VALUES (51, 2, 1, '用户管理第三方用户列表界面', 'user_admin_oauth_index_view', 'user', '用户管理第三方用户列表界面');
INSERT INTO `cmf_hook` VALUES (52, 2, 1, '后台首页界面', 'admin_index_index_view', 'admin', '后台首页界面');
INSERT INTO `cmf_hook` VALUES (53, 2, 1, '后台回收站界面', 'admin_recycle_bin_index_view', 'admin', '后台回收站界面');
INSERT INTO `cmf_hook` VALUES (54, 2, 1, '后台菜单管理界面', 'admin_menu_index_view', 'admin', '后台菜单管理界面');
INSERT INTO `cmf_hook` VALUES (55, 2, 1, '后台自定义登录是否开启钩子', 'admin_custom_login_open', 'admin', '后台自定义登录是否开启钩子');
INSERT INTO `cmf_hook` VALUES (56, 4, 0, '门户后台文章添加编辑界面右侧栏', 'portal_admin_article_edit_view_right_sidebar', 'portal', '门户后台文章添加编辑界面右侧栏');
INSERT INTO `cmf_hook` VALUES (57, 4, 0, '门户后台文章添加编辑界面主要内容', 'portal_admin_article_edit_view_main', 'portal', '门户后台文章添加编辑界面主要内容');
INSERT INTO `cmf_hook` VALUES (58, 2, 1, '门户后台文章添加界面', 'portal_admin_article_add_view', 'portal', '门户后台文章添加界面');
INSERT INTO `cmf_hook` VALUES (59, 2, 1, '门户后台文章编辑界面', 'portal_admin_article_edit_view', 'portal', '门户后台文章编辑界面');
INSERT INTO `cmf_hook` VALUES (60, 2, 1, '门户后台文章分类添加界面', 'portal_admin_category_add_view', 'portal', '门户后台文章分类添加界面');
INSERT INTO `cmf_hook` VALUES (61, 2, 1, '门户后台文章分类编辑界面', 'portal_admin_category_edit_view', 'portal', '门户后台文章分类编辑界面');
INSERT INTO `cmf_hook` VALUES (62, 2, 1, '门户后台页面添加界面', 'portal_admin_page_add_view', 'portal', '门户后台页面添加界面');
INSERT INTO `cmf_hook` VALUES (63, 2, 1, '门户后台页面编辑界面', 'portal_admin_page_edit_view', 'portal', '门户后台页面编辑界面');
INSERT INTO `cmf_hook` VALUES (64, 2, 1, '后台幻灯片页面列表界面', 'admin_slide_item_index_view', 'admin', '后台幻灯片页面列表界面');
INSERT INTO `cmf_hook` VALUES (65, 2, 1, '后台幻灯片页面添加界面', 'admin_slide_item_add_view', 'admin', '后台幻灯片页面添加界面');
INSERT INTO `cmf_hook` VALUES (66, 2, 1, '后台幻灯片页面编辑界面', 'admin_slide_item_edit_view', 'admin', '后台幻灯片页面编辑界面');
INSERT INTO `cmf_hook` VALUES (67, 2, 1, '后台管理员添加界面', 'admin_user_add_view', 'admin', '后台管理员添加界面');
INSERT INTO `cmf_hook` VALUES (68, 2, 1, '后台管理员编辑界面', 'admin_user_edit_view', 'admin', '后台管理员编辑界面');
INSERT INTO `cmf_hook` VALUES (69, 2, 1, '后台角色添加界面', 'admin_rbac_role_add_view', 'admin', '后台角色添加界面');
INSERT INTO `cmf_hook` VALUES (70, 2, 1, '后台角色编辑界面', 'admin_rbac_role_edit_view', 'admin', '后台角色编辑界面');
INSERT INTO `cmf_hook` VALUES (71, 2, 1, '后台角色授权界面', 'admin_rbac_authorize_view', 'admin', '后台角色授权界面');
COMMIT;

-- ----------------------------
-- Table structure for cmf_hook_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook_plugin`;
CREATE TABLE `cmf_hook_plugin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `hook` varchar(50) NOT NULL DEFAULT '' COMMENT '钩子名',
  `plugin` varchar(50) NOT NULL DEFAULT '' COMMENT '插件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统钩子插件表';

-- ----------------------------
-- Records of cmf_hook_plugin
-- ----------------------------
BEGIN;
INSERT INTO `cmf_hook_plugin` VALUES (1, 10000, 0, 'fetch_upload_view', 'Qiniu');
COMMIT;

-- ----------------------------
-- Table structure for cmf_im_limit
-- ----------------------------
DROP TABLE IF EXISTS `cmf_im_limit`;
CREATE TABLE `cmf_im_limit` (
  `uid` bigint(20) NOT NULL COMMENT '用户ID',
  `times` int(11) NOT NULL DEFAULT '0' COMMENT '次数',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_im_limit
-- ----------------------------
BEGIN;
INSERT INTO `cmf_im_limit` VALUES (2, 4, 20220317);
INSERT INTO `cmf_im_limit` VALUES (5, 4, 20220316);
INSERT INTO `cmf_im_limit` VALUES (8, 1, 20220316);
INSERT INTO `cmf_im_limit` VALUES (12, 2, 20220317);
COMMIT;

-- ----------------------------
-- Table structure for cmf_level
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level`;
CREATE TABLE `cmf_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `levelname` varchar(50) NOT NULL DEFAULT '' COMMENT '等级名称',
  `level_up` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '经验上限',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_level
-- ----------------------------
BEGIN;
INSERT INTO `cmf_level` VALUES (1, 1, '一级', 1000, 'level_1.png');
INSERT INTO `cmf_level` VALUES (2, 2, '二级', 3000, 'level_2.png');
INSERT INTO `cmf_level` VALUES (3, 3, '三级', 6000, 'level_3.png');
INSERT INTO `cmf_level` VALUES (4, 4, '四级', 10000, 'level_4.png');
INSERT INTO `cmf_level` VALUES (5, 5, '五级', 15000, 'level_5.png');
COMMIT;

-- ----------------------------
-- Table structure for cmf_level_anchor
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level_anchor`;
CREATE TABLE `cmf_level_anchor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `levelname` varchar(50) NOT NULL DEFAULT '' COMMENT '等级名称',
  `level_up` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '经验上限',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_level_anchor
-- ----------------------------
BEGIN;
INSERT INTO `cmf_level_anchor` VALUES (1, 1, '一级', 1000, 'level_anchor_1.png');
INSERT INTO `cmf_level_anchor` VALUES (2, 2, '二级', 3000, 'level_anchor_2.png');
INSERT INTO `cmf_level_anchor` VALUES (3, 3, '三级', 6000, 'level_anchor_3.png');
INSERT INTO `cmf_level_anchor` VALUES (4, 4, '四级', 10000, 'level_anchor_4.png');
INSERT INTO `cmf_level_anchor` VALUES (5, 5, '五级', 15000, 'level_anchor_5.png');
COMMIT;

-- ----------------------------
-- Table structure for cmf_link
-- ----------------------------
DROP TABLE IF EXISTS `cmf_link`;
CREATE TABLE `cmf_link` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:显示;0:不显示',
  `rating` int(11) NOT NULL DEFAULT '0' COMMENT '友情链接评级',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '友情链接描述',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '友情链接地址',
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接名称',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '友情链接图标',
  `target` varchar(10) NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `rel` varchar(50) NOT NULL DEFAULT '' COMMENT '链接与网站的关系',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='友情链接表';

-- ----------------------------
-- Records of cmf_link
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_option
-- ----------------------------
DROP TABLE IF EXISTS `cmf_option`;
CREATE TABLE `cmf_option` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `autoload` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否自动加载;1:自动加载;0:不自动加载',
  `option_name` varchar(64) NOT NULL DEFAULT '' COMMENT '配置名',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `option_name` (`option_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='全站配置表';

-- ----------------------------
-- Records of cmf_option
-- ----------------------------
BEGIN;
INSERT INTO `cmf_option` VALUES (1, 1, 'site_info', '{\"site_name\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u76f4\\u64ad\\u5f00\\u6e90\\u7248\\u6f14\\u793a\",\"site_url\":\"http:\\/\\/git1v1.yunbaozb.com\",\"name_coin\":\"\\u4e91\\u5e01\",\"name_votes\":\"\\u4e91\\u7968\",\"copyright\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u76f4\\u64ad-\\u5f00\\u6e90\\u7248\\u6f14\\u793a\\uff0c\\u5b98\\u65b9\\u7f51\\u7ad9http:\\/\\/www.yunbaokj.com\",\"site_seo_title\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u5f00\\u6e90\\u7248\\u6f14\\u793a\",\"site_seo_keywords\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u5f00\\u6e90\\u7248\\u6f14\\u793a\",\"site_seo_description\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u5f00\\u6e90\\u7248\\u6f14\\u793a\",\"apk_ver\":\"\",\"apk_url\":\"\",\"apk_des\":\"\\u6709\\u65b0\\u7248\\uff0c\\u8bf7\\u66f4\\u65b0\",\"ipa_ver\":\"\",\"ios_shelves\":\"1\",\"ipa_url\":\"\",\"ipa_des\":\"\\u6709\\u65b0\\u7248\\uff0c\\u8bf7\\u66f4\\u65b0\",\"qr_url\":\"\",\"share_video_title\":\"\\u89c6\\u9891\\u5206\\u4eab\\u6807\\u9898\",\"share_video_des\":\"\\u89c6\\u9891\\u5206\\u4eab\\u8bdd\\u672f\",\"share_agent_title\":\"\\u5168\\u6c11\\u8d5a\\u94b1\\u5206\\u4eab\\u6807\\u9898\",\"share_agent_des\":\"\\u5168\\u6c11\\u8d5a\\u94b1\\u5206\\u4eab\\u8bdd\\u672f\",\"sprout_key\":\"\",\"sprout_key_ios\":\"\",\"skin_whiting\":\"4\",\"skin_smooth\":\"4\",\"skin_tenderness\":\"4\",\"eye_brow\":\"10\",\"big_eye\":\"30\",\"eye_length\":\"10\",\"eye_corner\":\"30\",\"eye_alat\":\"30\",\"face_lift\":\"30\",\"face_shave\":\"30\",\"mouse_lift\":\"30\",\"nose_lift\":\"30\",\"chin_lift\":\"10\",\"forehead_lift\":\"30\",\"lengthen_noseLift\":\"30\",\"login_alert_title\":\"\\u670d\\u52a1\\u534f\\u8bae\\u548c\\u9690\\u79c1\\u653f\\u7b56\",\"login_alert_content\":\"\\u8bf7\\u60a8\\u52a1\\u5fc5\\u4ed4\\u7ec6\\u9605\\u8bfb\\uff0c\\u5145\\u5206\\u7406\\u89e3\\u670d\\u52a1\\u534f\\u8bae\\u548c\\u9690\\u79c1\\u653f\\u7b56\\u5404\\u6761\\u6b3e\\uff0c\\u5305\\u62ec\\u4f46\\u4e0d\\u9650\\u4e8e\\u4e3a\\u4e86\\u5411\\u60a8\\u63d0\\u4f9b\\u5373\\u65f6\\u901a\\u8baf\\uff0c\\u5185\\u5bb9\\u5206\\u4eab\\u7b49\\u670d\\u52a1\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u6536\\u96c6\\u60a8\\u8bbe\\u5907\\u4fe1\\u606f\\u548c\\u4e2a\\u4eba\\u4fe1\\u606f\\uff0c\\u60a8\\u53ef\\u4ee5\\u5728\\u8bbe\\u7f6e\\u4e2d\\u67e5\\u770b\\uff0c\\u7ba1\\u7406\\u60a8\\u7684\\u6388\\u6743\\u3002\\u60a8\\u53ef\\u9605\\u8bfb\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\\u4e86\\u89e3\\u8be6\\u7ec6\\u4fe1\\u606f\\uff0c\\u5982\\u60a8\\u540c\\u610f\\uff0c\\u8bf7\\u70b9\\u51fb\\u540c\\u610f\\u63a5\\u53d7\\u6211\\u4eec\\u7684\\u670d\\u52a1\\u3002\",\"login_clause_title\":\"\\u767b\\u5f55\\u5373\\u4ee3\\u8868\\u540c\\u610f\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_private_title\":\"\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\",\"login_private_url\":\"\\/appapi\\/page\\/detail?id=8\",\"login_service_title\":\"\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_service_url\":\"\\/appapi\\/page\\/detail?id=9\",\"company_name\":\"\\u4e91\\u8c79\\u4e00\\u5bf9\\u4e00\\u76f4\\u64ad\\u5f00\\u6e90\\u7248\\u6f14\\u793a\",\"company_desc\":\"\\u8be5\\u9879\\u76ee\\u7531\\u4e91\\u8c79\\u79d1\\u6280\\u5b98\\u65b9\\u81ea\\u4e3b\\u7814\\u53d1\\uff0c\\u63d0\\u4f9b\\u5f00\\u6e90\\u76f4\\u64ad\\u6e90\\u7801\\u3001\\u642d\\u5efa\\u6587\\u6863\\u3001\\u7591\\u96be\\u8f85\\u52a9\\u8bf4\\u660e\\uff0c\\u4f9b\\u5927\\u5bb6\\u53c2\\u8003\\u3001\\u4ea4\\u6d41\\u3001\\u4f7f\\u7528\\r\\n\\u5f00\\u6e90\\u7248\\u7a0b\\u5e8f\\u4e3b\\u8981\\u56f4\\u7ed5\\u201c\\u4e00\\u5bf9\\u4e00\\u89c6\\u9891\\u804a\\u5929\\u529f\\u80fd\\u201d\\u5c55\\u5f00\\uff0c\\u5982\\u60a8\\u9700\\u8981\\u66f4\\u591a\\u4e13\\u4e1a\\u529f\\u80fd\\u3001\\u4f18\\u8d28\\u670d\\u52a1\\uff0c\\u53ef\\u8054\\u7cfb\\u6211\\u4eec\\u4e86\\u89e3\\u5546\\u7528\\u7248\\u672c\\u3001\\u8fd0\\u8425\\u7248\\u672c\\u7684\\u4e00\\u5bf9\\u4e00\\u7cfb\\u7edf\\u6e90\\u7801\\r\\n\\u8054\\u7cfb\\u6211\\u4eec\\r\\n\\u516c\\u53f8\\u5b98\\u7f51\\uff1ahttp:\\/\\/www.yunbaokj.com\\/\\r\\n\\u7535\\u8bdd\\/\\u5fae\\u4fe1\\uff1a17662585037\\r\\n\\u5ba2\\u670dQQ\\uff1a800144567\",\"copyright_url\":\"http:\\/\\/www.yunbaokj.com\"}');
INSERT INTO `cmf_option` VALUES (2, 1, 'guide', '{\"switch\":\"1\",\"type\":\"0\",\"time\":\"3\"}');
INSERT INTO `cmf_option` VALUES (3, 1, 'cmf_settings', '{\"open_registration\":\"0\",\"banned_usernames\":\"\"}');
INSERT INTO `cmf_option` VALUES (4, 1, 'cdn_settings', '{\"cdn_static_root\":\"\"}');
INSERT INTO `cmf_option` VALUES (5, 1, 'admin_settings', '{\"admin_password\":\"\",\"admin_theme\":\"admin_simpleboot3\",\"admin_style\":\"flatadmin\"}');
INSERT INTO `cmf_option` VALUES (6, 1, 'storage', '{\"storages\":{\"Qiniu\":{\"name\":\"\\u4e03\\u725b\\u4e91\\u5b58\\u50a8\",\"driver\":\"\\\\plugins\\\\qiniu\\\\lib\\\\Qiniu\"}},\"type\":\"Qiniu\"}');
INSERT INTO `cmf_option` VALUES (8, 1, 'configpri', '{\"ihuyi_account\":\"\",\"ihuyi_ps\":\"\",\"sendcode_switch\":\"0\",\"iplimit_switch\":\"0\",\"iplimit_times\":\"20\",\"aliapp_switch\":\"1\",\"aliapp_partner\":\"\",\"aliapp_seller_id\":\"\",\"aliapp_key\":\"\",\"wx_switch\":\"1\",\"wx_appid\":\"\",\"wx_appsecret\":\"\",\"wx_mchid\":\"\",\"wx_key\":\"\",\"login_type\":\"\",\"share_type\":\"\",\"ios_switch\":\"0\",\"ios_sandbox\":\"0\",\"cash_rate\":\"100\",\"cash_min\":\"10\",\"cash_start\":\"1\",\"cash_end\":\"30\",\"cash_max_times\":\"0\",\"cash_tip\":\"\\u6bcf\\u670820-30\\u53f7\\u53ef\\u8fdb\\u884c\\u63d0\\u73b0\\u7533\\u8bf7\\uff0c\\u6536\\u76ca\\u5c06\\u572820-30\\u53f7\\u7edf\\u4e00\\u53d1\\u653e\\u3002\",\"im_sdkappid\":\"\",\"im_accounttype\":\"\",\"im_admin\":\"\",\"im_full_group_id\":\"\",\"tx_appid\":\"\",\"tx_bizid\":\"\",\"tx_push\":\"\",\"tx_pull\":\"\",\"tx_push_key\":\"\",\"tx_api_key\":\"\",\"conversa_time_min\":\"2\",\"ccp_sid\":\"\",\"ccp_token\":\"\",\"ccp_appid\":\"\",\"ccp_tempid\":\"\",\"match_voice\":\"30\",\"match_voice_vip\":\"20\",\"match_video\":\"30\",\"match_video_vip\":\"20\",\"agent_switch\":\"1\",\"agent_must\":\"0\",\"agent_one\":\"10\",\"agent_two\":\"10\",\"agent_three\":\"0\",\"agent_reward\":\"15\",\"agent_daytimes\":\"2\",\"agent_times\":\"6\",\"im_limit\":\"0\",\"im_coin\":\"10\",\"dynamic_switch\":\"0\",\"sprout_key\":\"\",\"im_key\":\"\",\"tx_acc_key\":\"\",\"aly_keydi\":\"\",\"aly_secret\":\"\",\"aly_signName\":\"\",\"aly_templateCode\":\"\",\"reg_reward\":\"999999\",\"sendcode_type\":\"1\",\"aly_signName_inter\":\"\",\"aly_templateCode_inter\":\"\",\"paypal_switch\":\"0\",\"paypal_sandbox\":\"0\",\"sandbox_clientid\":\"\",\"product_clientid\":\"\",\"aws_bucket\":\"\",\"aws_region\":\"\",\"aws_hosturl\":\"\",\"aws_identitypoolid\":\"\",\"cloudtype\":\"1\",\"code_switch\":\"2\",\"tencent_sms_appid\":\"\",\"tencent_sms_appkey\":\"\",\"tencent_sms_signName\":\"\",\"tencent_sms_templateCode\":\"\",\"tencent_sms_hw_signName\":\"\",\"tencent_sms_hw_templateCode\":\"\",\"recommendinfo\":\"\\u8fd9\\u4e9b\\u4eba\\u770b\\u8fc7\\u4f60\\u7684\\u8d44\\u6599\\uff0c\\u90fd\\u89c9\\u5f97\\u4f60\\u5f88\\u6709\\u8da3\\uff01\\u6b63\\u5728\\u7b49\\u4f60\\u6253\\u62db\\u547c\\u54e6~\",\"cash_take\":\"20\",\"aliapp_switch_h5\":\"0\",\"paypal_pattern_braintree\":\"0\",\"braintree_sandbox\":\"\",\"braintree_public_sandbox\":\"\",\"braintree_private_sandbox\":\"\",\"braintree_production\":\"\",\"braintree_public_production\":\"\",\"braintree_private_production\":\"\",\"sensitive_words\":\"\\u6bdb\\u6cfd\\u4e1c,\\u4e60\\u8fd1\\u5e73,\\u80e1\\u9526\\u6d9b,\\u6c5f\\u6cfd\\u6c11,\\u6731\\u9555\\u57fa,\\u4e14,weixin,qq,\\u5fae\\u4fe1,QQ,\\u5728\\u5417\",\"family_switch\":\"1\",\"family_member_divide_switch\":\"1\",\"openinstall_switch\":\"0\",\"openinstall_appkey\":\"\"}');
INSERT INTO `cmf_option` VALUES (9, 1, 'upload_setting', '{\"max_files\":\"20\",\"chunk_size\":\"512\",\"file_types\":{\"image\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"jpg,jpeg,png,gif,bmp4\"},\"video\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"mp4\"},\"audio\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"mp3,wma,wav\"},\"file\":{\"upload_max_filesize\":\"10240\",\"extensions\":\"txt,pdf,doc,docx,xls,xlsx,ppt,pptx,zip,rar,svga,mp4\"}}}');
COMMIT;

-- ----------------------------
-- Table structure for cmf_photo
-- ----------------------------
DROP TABLE IF EXISTS `cmf_photo`;
CREATE TABLE `cmf_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图片',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT '浏览数',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '视频状态 0审核中 1通过 2拒绝,3下架',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '原因',
  `uptime` int(12) NOT NULL DEFAULT '0' COMMENT '审核不通过时间',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '私密价格',
  `isprivate` tinyint(1) NOT NULL DEFAULT '0' COMMENT '私密，0否1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_photo
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_plugin`;
CREATE TABLE `cmf_plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '插件类型;1:网站;8:微信',
  `has_admin` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台管理,0:没有;1:有',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:开启;0:禁用',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '插件安装时间',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '插件标识名,英文字母(惟一)',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `demo_url` varchar(50) NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `hooks` varchar(255) NOT NULL DEFAULT '' COMMENT '实现的钩子;以“,”分隔',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件作者',
  `author_url` varchar(50) NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '插件版本号',
  `description` varchar(255) NOT NULL COMMENT '插件描述',
  `config` text COMMENT '插件配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='插件表';

-- ----------------------------
-- Records of cmf_plugin
-- ----------------------------
BEGIN;
INSERT INTO `cmf_plugin` VALUES (1, 1, 0, 0, 0, 'Qiniu', '七牛云存储', '', '', 'ThinkCMF', '', '1.0.1', 'ThinkCMF七牛专享优惠码:507670e8', '{\"accessKey\":\"1\",\"secretKey\":\"2\",\"protocol\":\"http\",\"domain\":\"3\",\"bucket\":\"4\",\"zone\":\"z0\"}');
COMMIT;

-- ----------------------------
-- Table structure for cmf_portal_category
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category`;
CREATE TABLE `cmf_portal_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '分类父id',
  `post_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '分类文章数',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:发布,0:不发布',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '分类描述',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '分类层级关系路径',
  `seo_title` varchar(100) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `list_tpl` varchar(50) NOT NULL DEFAULT '' COMMENT '分类列表模板',
  `one_tpl` varchar(50) NOT NULL DEFAULT '' COMMENT '分类文章页模板',
  `more` text COMMENT '扩展属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='portal应用 文章分类表';

-- ----------------------------
-- Records of cmf_portal_category
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_portal_category_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category_post`;
CREATE TABLE `cmf_portal_category_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `term_taxonomy_id` (`category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='portal应用 分类文章对应表';

-- ----------------------------
-- Records of cmf_portal_category_post
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_portal_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_post`;
CREATE TABLE `cmf_portal_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `post_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '类型,1:文章;2:页面',
  `post_format` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '内容格式;1:html;2:md',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '发表者用户id',
  `post_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:已发布;0:未发布;',
  `comment_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '评论状态;1:允许;0:不允许',
  `is_top` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否置顶;1:置顶;0:不置顶',
  `recommended` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_hits` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '查看数',
  `post_favorites` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  `post_like` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `comment_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `published_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `post_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'post标题',
  `post_keywords` varchar(150) NOT NULL DEFAULT '' COMMENT 'seo keywords',
  `post_excerpt` varchar(500) NOT NULL DEFAULT '' COMMENT 'post摘要',
  `post_source` varchar(150) NOT NULL DEFAULT '' COMMENT '转载文章的来源',
  `thumbnail` varchar(100) NOT NULL DEFAULT '' COMMENT '缩略图',
  `post_content` longtext COMMENT '文章内容',
  `post_content_filtered` text COMMENT '处理过的文章内容',
  `more` text COMMENT '扩展属性,如缩略图;格式为json',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '页面类型，0单页面，1关于我们',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_status_date` (`post_type`,`post_status`,`create_time`,`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='portal应用 文章表';

-- ----------------------------
-- Records of cmf_portal_post
-- ----------------------------
BEGIN;
INSERT INTO `cmf_portal_post` VALUES (1, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1554183787, 1619745420, 1554183720, 0, '平台协议', '', '', '', '', '&lt;p&gt;平台协议，内容自定义&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0);
INSERT INTO `cmf_portal_post` VALUES (3, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1554859843, 1578560357, 1554859800, 0, '用户充值协议', '', '', '', '', '\n&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: &quot;&gt;用户充值协议&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0);
INSERT INTO `cmf_portal_post` VALUES (4, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1554863351, 1611113324, 1554863340, 0, '关于我们', '', '', '', '', '\n&lt;p&gt;关于我们，内容自定义&lt;/p&gt;\n&lt;p class=&quot;ql-long-5846395&quot; style=&quot;line-height: 1.7;margin-bottom: 0pt;margin-top: 0pt;font-size: 11pt;color: #494949;&quot;&gt;&lt;br&gt;&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0);
INSERT INTO `cmf_portal_post` VALUES (8, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1589785536, 1607393955, 1589785500, 0, '隐私政策', '', '', '', '', '', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0);
INSERT INTO `cmf_portal_post` VALUES (9, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1589785577, 1595497459, 1589785560, 0, '服务协议', '', '', '', '', '&lt;p&gt;服务协议&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0);
COMMIT;

-- ----------------------------
-- Table structure for cmf_portal_tag
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag`;
CREATE TABLE `cmf_portal_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:发布,0:不发布',
  `recommended` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '标签文章数',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='portal应用 文章标签表';

-- ----------------------------
-- Records of cmf_portal_tag
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_portal_tag_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag_post`;
CREATE TABLE `cmf_portal_tag_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '标签 id',
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文章 id',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `post_id` (`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='portal应用 标签文章对应表';

-- ----------------------------
-- Records of cmf_portal_tag_post
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_recycle_bin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_recycle_bin`;
CREATE TABLE `cmf_recycle_bin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT '0' COMMENT '删除内容 id',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  `table_name` varchar(60) DEFAULT '' COMMENT '删除内容所在表名',
  `name` varchar(255) DEFAULT '' COMMENT '删除内容名称',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT=' 回收站';

-- ----------------------------
-- Records of cmf_recycle_bin
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_role
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role`;
CREATE TABLE `cmf_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父角色ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态;0:禁用;1:正常',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `list_order` float NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of cmf_role
-- ----------------------------
BEGIN;
INSERT INTO `cmf_role` VALUES (1, 0, 1, 1329633709, 1329633709, 0, '超级管理员', '拥有网站最高管理员权限！');
COMMIT;

-- ----------------------------
-- Table structure for cmf_role_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role_user`;
CREATE TABLE `cmf_role_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色 id',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色对应表';

-- ----------------------------
-- Records of cmf_role_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_route
-- ----------------------------
DROP TABLE IF EXISTS `cmf_route`;
CREATE TABLE `cmf_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态;1:启用,0:不启用',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'URL规则类型;1:用户自定义;2:别名添加',
  `full_url` varchar(255) NOT NULL DEFAULT '' COMMENT '完整url',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '实际显示的url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='url路由表';

-- ----------------------------
-- Records of cmf_route
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_sendcode
-- ----------------------------
DROP TABLE IF EXISTS `cmf_sendcode`;
CREATE TABLE `cmf_sendcode` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '消息类型，1表示短信验证码，2表示邮箱验证码',
  `account` varchar(255) NOT NULL COMMENT '接收账号',
  `content` text NOT NULL COMMENT '消息内容',
  `addtime` int(11) unsigned NOT NULL COMMENT '提交时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_sendcode
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_slide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide`;
CREATE TABLE `cmf_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:显示,0不显示',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '幻灯片分类',
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '分类备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='幻灯片表';

-- ----------------------------
-- Records of cmf_slide
-- ----------------------------
BEGIN;
INSERT INTO `cmf_slide` VALUES (1, 1, 0, 'APP首页轮播', '不可删除  图片比例：750 X 220');
INSERT INTO `cmf_slide` VALUES (2, 1, 1578561082, '零零', '哈哈哈');
INSERT INTO `cmf_slide` VALUES (3, 1, 1556176607, '1v1付费直播', '');
COMMIT;

-- ----------------------------
-- Table structure for cmf_slide_item
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide_item`;
CREATE TABLE `cmf_slide_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) NOT NULL DEFAULT '0' COMMENT '幻灯片id',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:显示;0:隐藏',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '幻灯片名称',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '幻灯片图片',
  `url` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '幻灯片链接',
  `target` varchar(10) NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '幻灯片描述',
  `content` text CHARACTER SET utf8 COMMENT '幻灯片内容',
  `more` text COMMENT '扩展信息',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `slide_id` (`slide_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='幻灯片子项表';

-- ----------------------------
-- Records of cmf_slide_item
-- ----------------------------
BEGIN;
INSERT INTO `cmf_slide_item` VALUES (1, 1, 1, 10000, '1', 'admin/20220316/674be105ab6d92e1535b02443b467969.jpg', 'http://baidu.com', '', '', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cmf_subscribe
-- ----------------------------
DROP TABLE IF EXISTS `cmf_subscribe`;
CREATE TABLE `cmf_subscribe` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `liveuid` bigint(20) NOT NULL DEFAULT '0' COMMENT '主播ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型，1视频2语音',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '提交时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态，0未赴约1已赴约',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_subscribe
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_theme
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme`;
CREATE TABLE `cmf_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后升级时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '模板状态,1:正在使用;0:未使用',
  `is_compiled` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为已编译模板',
  `theme` varchar(20) NOT NULL DEFAULT '' COMMENT '主题目录名，用于主题的维一标识',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '主题名称',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '主题版本号',
  `demo_url` varchar(50) NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `thumbnail` varchar(100) NOT NULL DEFAULT '' COMMENT '缩略图',
  `author` varchar(20) NOT NULL DEFAULT '' COMMENT '主题作者',
  `author_url` varchar(50) NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `lang` varchar(10) NOT NULL DEFAULT '' COMMENT '支持语言',
  `keywords` varchar(50) NOT NULL DEFAULT '' COMMENT '主题关键字',
  `description` varchar(100) NOT NULL DEFAULT '' COMMENT '主题描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_theme
-- ----------------------------
BEGIN;
INSERT INTO `cmf_theme` VALUES (1, 0, 0, 0, 0, 'simpleboot3', 'simpleboot3', '1.0.2', 'http://demo.thinkcmf.com', '', 'ThinkCMF', 'http://www.thinkcmf.com', 'zh-cn', 'ThinkCMF模板', 'ThinkCMF默认模板');
COMMIT;

-- ----------------------------
-- Table structure for cmf_theme_file
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme_file`;
CREATE TABLE `cmf_theme_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_public` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否公共的模板文件',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `theme` varchar(20) NOT NULL DEFAULT '' COMMENT '模板名称',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '模板文件名',
  `action` varchar(50) NOT NULL DEFAULT '' COMMENT '操作',
  `file` varchar(50) NOT NULL DEFAULT '' COMMENT '模板文件，相对于模板根目录，如Portal/index.html',
  `description` varchar(100) NOT NULL DEFAULT '' COMMENT '模板文件描述',
  `more` text COMMENT '模板更多配置,用户自己后台设置的',
  `config_more` text COMMENT '模板更多配置,来源模板的配置文件',
  `draft_more` text COMMENT '模板更多配置,用户临时保存的配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_theme_file
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user`;
CREATE TABLE `cmf_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '用户类型;1:admin;2:会员',
  `sex` tinyint(2) NOT NULL DEFAULT '0' COMMENT '性别;0:保密,1:男,2:女',
  `birthday` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '生日',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `coin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '金币',
  `consumption` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '消费总额',
  `votes` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '剩余映票',
  `votestotal` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '总映票',
  `balance` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `user_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态;0:禁用,1:正常,2:未验证,3:已注销',
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) NOT NULL DEFAULT '' COMMENT '登录密码;cmf_password加密',
  `user_nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
  `user_url` varchar(100) NOT NULL DEFAULT '' COMMENT '用户个人网址',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  `avatar_thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像缩略图，分享用',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '' COMMENT '激活码',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `more` text COMMENT '扩展属性',
  `login_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '账号类型，0手机号，1QQ，2微信，3新浪，4facebook，5twitter',
  `openid` varchar(255) NOT NULL DEFAULT '' COMMENT '三方openid',
  `source` tinyint(1) NOT NULL DEFAULT '0' COMMENT '设备来源，0web，1android，2ios，3小程序',
  `isauth` tinyint(1) NOT NULL DEFAULT '0' COMMENT '认证状态，0未认证1已认证',
  `goodnums` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '好评数',
  `badnums` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '差评数',
  `level_anchor` int(11) NOT NULL DEFAULT '0' COMMENT '主播等级，排序用',
  `online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '在线，0离线，1勿扰，2在聊，3在线',
  `last_online_time` int(11) NOT NULL DEFAULT '0' COMMENT '离线时间',
  `recommend_val` bigint(20) NOT NULL DEFAULT '0' COMMENT '推荐值',
  `isvoice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '语音开关，0关1开',
  `voice_value` int(11) NOT NULL DEFAULT '0' COMMENT '语音价格',
  `isvideo` tinyint(1) NOT NULL DEFAULT '1' COMMENT '视频开关，0关1开',
  `video_value` int(11) NOT NULL DEFAULT '10' COMMENT '视频价格',
  `isdisturb` tinyint(1) NOT NULL DEFAULT '0' COMMENT '勿扰开关，0关1开',
  `lng` varchar(255) NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(255) NOT NULL DEFAULT '' COMMENT '纬度',
  `country_code` varchar(255) NOT NULL DEFAULT '86' COMMENT '国家/地区',
  `is_firstlogin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否第一次登录 0 否 1是',
  `isauthor_auth` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否主播认证 0 否 1 是',
  `audio` varchar(255) NOT NULL DEFAULT '' COMMENT '语音',
  `audio_length` tinyint(1) NOT NULL DEFAULT '0' COMMENT '语音时长',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '身高',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT '体重',
  `constellation` varchar(50) NOT NULL DEFAULT '' COMMENT '星座',
  `labelid` varchar(255) NOT NULL DEFAULT '' COMMENT '形象标签id',
  `label` varchar(255) NOT NULL DEFAULT '' COMMENT '形象标签',
  `label_c` varchar(255) NOT NULL DEFAULT '' COMMENT '标签颜色',
  `province` varchar(255) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) NOT NULL DEFAULT '' COMMENT '市',
  `district` varchar(255) NOT NULL DEFAULT '' COMMENT '区',
  `intr` varchar(255) NOT NULL DEFAULT '' COMMENT '个人介绍',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_login` (`user_login`) USING BTREE,
  KEY `user_nickname` (`user_nickname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of cmf_user
-- ----------------------------
BEGIN;
INSERT INTO `cmf_user` VALUES (1, 1, 0, 0, 1648100672, 0, 0, 0, 0, 0, 0.00, 1553940312, 1, 'admin', '###d0491aa908307d5fa745b2cd18f56ff9', 'admin', 'admin@163.com', '', '', '', '', '123.135.78.124', '', '', NULL, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '86', 0, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (13, 2, 2, 0, 1647600008, 0, 989, 10, 0, 0, 0.00, 1647512087, 1, '15757575757', '###b5dd4a5a66a88e57929b3483a2ddeaee', '手机用户757', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '39.85.102.58', '', '15757575757', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 20, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (17, 2, 2, 0, 1647997009, 0, 999999, 0, 0, 0, 0.00, 1647997009, 1, '15454545451', '###9c36bf96557361235b125877b733e5f4', '0323测试', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '112.224.130.113', '', '15454545451', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 10, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (19, 2, 1, 0, 1648020636, 0, 999933, 66, 0, 0, 0.00, 1648020636, 1, '15066720071', '###1a3b68533c4838a620e857246710c85b', '男生1501', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '39.85.102.58', '', '15066720071', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 10, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (20, 2, 2, 0, 1648100598, 0, 999999, 0, 0, 0, 0.00, 1648100598, 1, '13411111111', '###8cd1fb2ee384ffbc623fca54b02b339c', '女生1341', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '39.85.102.58', '', '13411111111', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 10, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (21, 2, 1, 0, 1648103605, 0, 999989, 10, 0, 0, 0.00, 1648100881, 1, '13433333333', '###48fa846fba364b25e542d4f459ea2948', '男生1343', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '39.85.102.58', '', '13433333333', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 10, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
INSERT INTO `cmf_user` VALUES (22, 2, 1, 0, 1648102023, 0, 999999, 0, 0, 0, 0.00, 1648102023, 1, '13355558888', '###4b042b3a2ec8ebae62ceabbffe1e33b3', '手机用户888', '', '', '/default.png', '/default_thumb.png', '这家伙很懒，什么都没留下', '39.85.102.58', '', '13355558888', NULL, 0, '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 10, 0, '', '', '86', 1, 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for cmf_user_attention
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_attention`;
CREATE TABLE `cmf_user_attention` (
  `uid` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `touid` bigint(20) unsigned NOT NULL COMMENT '关注人ID',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  KEY `uid_touid_index` (`uid`,`touid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of cmf_user_attention
-- ----------------------------
BEGIN;
INSERT INTO `cmf_user_attention` VALUES (8, 2, 1647508830);
COMMIT;

-- ----------------------------
-- Table structure for cmf_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_auth`;
CREATE TABLE `cmf_user_auth` (
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `cardno` varchar(255) NOT NULL DEFAULT '' COMMENT '身份证号',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '姓名',
  `mobile` varchar(255) NOT NULL DEFAULT '' COMMENT '电话',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '审核说明',
  `addtime` int(12) NOT NULL DEFAULT '0' COMMENT '提交时间',
  `uptime` int(12) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0 审核中  1  通过  2 拒绝',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_user_auth
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_user_coinrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_coinrecord`;
CREATE TABLE `cmf_user_coinrecord` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '收支类型,0支出，1收入',
  `action` tinyint(4) NOT NULL DEFAULT '0' COMMENT '收支行为，1送礼物,2视频通话，3语音通话，4视频付费，5照片付费，6购买VIP，7购买私信，8注册奖励',
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `touid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '对方ID',
  `actionid` int(20) NOT NULL DEFAULT '0' COMMENT '行为对应ID',
  `nums` int(20) NOT NULL DEFAULT '0' COMMENT '数量',
  `totalcoin` int(20) NOT NULL DEFAULT '0' COMMENT '总价',
  `addtime` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `showid` bigint(20) NOT NULL DEFAULT '0' COMMENT '通话标识',
  `isdeduct` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否扣除，0否1是',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `action_uid_addtime` (`action`,`uid`,`addtime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_user_coinrecord
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_user_token
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_token`;
CREATE TABLE `cmf_user_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `expire_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT ' 过期时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `token` varchar(64) NOT NULL DEFAULT '' COMMENT 'token',
  `device_type` varchar(10) NOT NULL DEFAULT '' COMMENT '设备类型;mobile,android,iphone,ipad,web,pc,mac,wxapp',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户客户端登录 token 表';

-- ----------------------------
-- Records of cmf_user_token
-- ----------------------------
BEGIN;
INSERT INTO `cmf_user_token` VALUES (1, 1, 1662435354, 1646883354, '9c6710599077d97accc8f8036cf0c6e7152b7d0688143729c3762ea9af377875', 'web');
INSERT INTO `cmf_user_token` VALUES (2, 2, 1660985929, 1648025929, '4570171065d650315491bdaaa21adda7', '');
INSERT INTO `cmf_user_token` VALUES (3, 3, 1660292638, 1647332638, 'acf262420a80e89848170ecf5882fa04', '');
INSERT INTO `cmf_user_token` VALUES (4, 4, 1660295247, 1647335247, 'abc31b64c16be5455b9c7cceb95d2385', '');
INSERT INTO `cmf_user_token` VALUES (5, 5, 1660437290, 1647477290, '07f3b6dfd6929c70559801856161ff63', '');
INSERT INTO `cmf_user_token` VALUES (6, 6, 1660358213, 1647398213, '4cd5459fbf8d6b9b3c3369a5d54dea1f', '');
INSERT INTO `cmf_user_token` VALUES (7, 7, 1660373057, 1647413057, 'ae0f13cdd5f7a07e6bb99e11a6367bee', '');
INSERT INTO `cmf_user_token` VALUES (8, 8, 1660467113, 1647507113, '304d2a1c57cb961979443b52b14a1277', '');
INSERT INTO `cmf_user_token` VALUES (9, 9, 1660437399, 1647477399, '367e0e2281924d714dfd834ba22b19ac', '');
INSERT INTO `cmf_user_token` VALUES (10, 10, 1660559225, 1647599225, '0ea497896696e22b58005424bfa600c9', '');
INSERT INTO `cmf_user_token` VALUES (11, 11, 1660528372, 1647568372, 'dc35f64ace0c3efce25a69a0fd17701a', '');
INSERT INTO `cmf_user_token` VALUES (12, 12, 1660528357, 1647568357, '47e2790a25b40ad1b0565de473178384', '');
INSERT INTO `cmf_user_token` VALUES (13, 13, 1660560008, 1647600008, 'dd632d9a576c03ad64639cbc32539bd9', '');
INSERT INTO `cmf_user_token` VALUES (14, 14, 1660473204, 1647513204, 'eae20bf28d404e0e62dc413e7007a577', '');
INSERT INTO `cmf_user_token` VALUES (15, 15, 1660528391, 1647568391, '5a9b656e517b4d23f325f1895b8467ad', '');
INSERT INTO `cmf_user_token` VALUES (17, 17, 1660957009, 1647997009, '2779c87c5fc3cad40d7919a507548e2d', '');
INSERT INTO `cmf_user_token` VALUES (18, 18, 1660958248, 1647998248, 'cc26a7c0066d9d88ca66f8854041eb90', '');
INSERT INTO `cmf_user_token` VALUES (19, 19, 1660980636, 1648020636, 'a1b2c27cbebcfe219e6dd6298be2b648', '');
INSERT INTO `cmf_user_token` VALUES (20, 20, 1661060598, 1648100598, '08694d2053b80fa73443f139d35e2765', '');
INSERT INTO `cmf_user_token` VALUES (21, 21, 1661063605, 1648103605, '07f2a81e56f49bdfeedd3202543c708a', '');
INSERT INTO `cmf_user_token` VALUES (22, 22, 1661062023, 1648102023, '6d68abf3c7c060b3fb61020d725f64c9', '');
COMMIT;

-- ----------------------------
-- Table structure for cmf_user_voterecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_voterecord`;
CREATE TABLE `cmf_user_voterecord` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '收支类型,0支出，1收入',
  `action` tinyint(1) NOT NULL DEFAULT '0' COMMENT '收支行为  1注册邀请奖励 2下级充值上级分成奖励 3 收礼物 4 视频通话 5 语音通话 6 视频收费 7照片收费 8 通话不足十秒主播扣除通话收益 9 公会长分成主播通话收益 10 主播通话不足十秒公会长分成退回 11 公会长分成主播礼物收益',
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `fromid` bigint(20) NOT NULL DEFAULT '0' COMMENT '来源用户ID',
  `actionid` bigint(20) NOT NULL DEFAULT '0' COMMENT '行为对应ID',
  `nums` bigint(20) NOT NULL DEFAULT '0' COMMENT '数量',
  `total` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `showid` bigint(20) NOT NULL DEFAULT '0' COMMENT '直播标识',
  `votes` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '收益映票',
  `addtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `action_uid_addtime` (`action`,`uid`,`addtime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cmf_user_voterecord
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cmf_verification_code
-- ----------------------------
DROP TABLE IF EXISTS `cmf_verification_code`;
CREATE TABLE `cmf_verification_code` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天已经发送成功的次数',
  `send_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后发送成功时间',
  `expire_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证码过期时间',
  `code` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '最后发送成功的验证码',
  `account` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '手机号或者邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='手机邮箱数字验证码表';

-- ----------------------------
-- Records of cmf_verification_code
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Function structure for getDistance
-- ----------------------------
DROP FUNCTION IF EXISTS `getDistance`;
delimiter ;;
CREATE FUNCTION `getDistance`(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT)
 RETURNS float
  DETERMINISTIC
BEGIN
    RETURN ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN((lat1 * PI() / 180 - lat2 * PI() / 180) / 2), 2)
           + COS(lat1 * PI() / 180) * COS(lat2 * PI() / 180)
           * POW(SIN(( lon1 * PI() / 180 - lon2 * PI() / 180 ) / 2),2))),2);
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
