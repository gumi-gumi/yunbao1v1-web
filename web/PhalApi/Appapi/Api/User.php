<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 用户信息类
 */

class Api_User extends PhalApi_Api {

    public function getRules() {
        return array(

            'getBaseInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
				'ios_version' => array('name' => 'ios_version', 'type' => 'string', 'default'=>'', 'desc' => 'IOS版本号'),
			),

            'setVideoSwitch' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
				'isvideo' => array('name' => 'isvideo', 'type' => 'string', 'default'=>'0', 'desc' => '开关，0关1开'),
			),
            'setVideoValue' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
				'value' => array('name' => 'value', 'type' => 'string', 'default'=>'0', 'desc' => '价格'),
			),
           
            'upUserInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
				'avatar' => array('name' => 'avatar', 'type' => 'string', 'default'=>'', 'desc' => '头像'),
				'name' => array('name' => 'name', 'type' => 'string', 'default'=>'', 'desc' => '昵称'),
				'audio' => array('name' => 'audio', 'type' => 'string', 'default'=>'', 'desc' => '语音'),
				'audio_length' => array('name' => 'audio_length', 'type' => 'int', 'default'=>'0', 'desc' => '语音时长'),
				'sex' => array('name' => 'sex', 'type' => 'string', 'desc' => '性别,1男2女'),
				'height' => array('name' => 'height', 'type' => 'string', 'desc' => '身高'),
				'weight' => array('name' => 'weight', 'type' => 'string', 'desc' => '体重'),
				'constellation' => array('name' => 'constellation', 'type' => 'string', 'desc' => '星座'),
			),
            'getFollowsList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>1,'desc' => '页数'),
			),
            'getFansList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>1,'desc' => '页数'),
			),
            
            'setAttent' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'desc' => '对方ID'),
			),
            
            'getGiftCab' => array(
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
			),
            
            'getUserHome' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
			),
			
			'getUserList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>1,'desc' => '页数'),
				'type' => array('name' => 'type', 'type' => 'int', 'default'=>1,'desc' => '筛选条件1全部，2未认证，3已认证'),
			),
			'setUserInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'avatar' => array('name' => 'avatar', 'type' => 'string', 'desc' => '头像'),
				'name' => array('name' => 'name', 'type' => 'string', 'desc' => '昵称'),
				'sex' => array('name' => 'sex', 'type' => 'string', 'desc' => '性别'),
			),

			'getUserMaterial'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
        );
    }
	
	/**
	 * 登录时设置头像、昵称及性别
	 * @desc 用于登录时设置头像、昵称及性别
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
	//
	public function setUserInfo() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $avatar=checkNull($this->avatar);
        $name=checkNull($this->name);
		$sex=checkNull($this->sex);
        
		if($name==''){
            $rs['code'] = 1001;
            $rs['msg'] = '请填写昵称';
            return $rs;
        }

        $is_has_sensitive=checkSensitiveWords($name);

        if($is_has_sensitive){
        	$rs['code'] = 1001;
			$rs['msg'] = '昵称输入非法,请重新输入';
			return $rs;
        }
        
        if($sex==''){
            $rs['code'] = 1002;
            $rs['msg'] = '请选择性别';
            return $rs;
        }

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->setUserInfo($uid,$avatar,$name,$sex);
	 
		return $info;
	}
	
	/**
	 * 获取首页用户列表信息
	 * @desc 用于获取首页用户列表信息
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
	//
	public function getUserList() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $p=checkNull($this->p);
		$type=checkNull($this->type);

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->getUserList($uid,$p,$type);

		
		$rs['info']=$info;
	 
		return $rs;
	}
	



	/**
	 * 获取用户信息
	 * @desc 用于获取单个用户基本信息
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return array info[0] 用户信息
	 * @return int info[0].id 用户ID
	 * @return string info[0].level 等级
	 * @return string info[0].follows 关注数
	 * @return string info[0].fans 粉丝数
	 * @return string info[0].isvoice 语音开关，0关1开
	 * @return string info[0].voice_value 语音价格
	 * @return string info[0].isvideo 视频开关，0关1开
	 * @return string info[0].video_value 视频价格
	 * @return string info[0].isdisturb 勿扰开关，0关1开
	 * @return array info[0].voicelist 语音价格列表
     * @return string info[0].voicelist[].coin 价格
     * @return string info[0].voicelist[].canselect 能否选择0否1是
	 * @return array info[0].videolist 视频价格列表
     * @return string info[0].videolist[].coin 价格
     * @return string info[0].videolist[].canselect 能否选择0否1是
     * @return string info[0].isvip 是否VIP，0否1是
     * @return string info[0].family_switch 家族开关
     * @return string info[0].isauth 是否主播认证 0否1是
     * @return string info[0].isuser_auth 是否实名认证 0否1是
	 * @return string msg 提示信息
	 */
	//
	public function getBaseInfo() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        
		$checkToken=checkToken($uid,$token);
		
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}


		$domain = new Domain_User();
		$info = $domain->getBaseInfo($uid);
		$configpub=getConfigPub();
		$configpri=getConfigPri();

		$list=array();

		$isauth=1;

		$info['isauth']=$isauth;
		$info['isuser_auth']=isAuth($uid);

		
        if($isauth==1){
            $list[]=array('id'=>'11','name'=>'背景墙','thumb'=>get_upload_path("/static/app/person/back.png"),'href'=>'' );
            //$list[]=array('id'=>'12','name'=>'我的视频','thumb'=>get_upload_path("/static/app/person/video.png"),'href'=>'' );
            $list[]=array('id'=>'13','name'=>'我的相册','thumb'=>get_upload_path("/static/app/person/photo.png"),'href'=>'' );
            $list[]=array('id'=>'5','name'=>'礼物柜','thumb'=>get_upload_path("/static/app/person/gift.png"),'href'=>'' );
            $list[]=array('id'=>'6','name'=>'视频接听','thumb'=>get_upload_path("/static/app/person/live.png"),'href'=>'' );
        }
        

		$info['list']=$list;
        
        $info['voicelist']=$voicelist;
        /* 视频价格 */
        $videolist=$domain->getVideo();
        foreach($videolist as$k=>$v){
            $v['canselect']='0';
            if($v['level']<=$info['level_anchor']){
                $v['canselect']='1';
            }
            $videolist[$k]=$v;
        }
        $info['videolist']=$videolist;
        
		$rs['info'][0] = $info;

		return $rs;
	}
    
    /**
	 * 视频开关
	 * @desc 用于用户设置视频聊天开关
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
    //
	public function setVideoSwitch() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $isvideo=checkNull($this->isvideo);

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->setVideoSwitch($uid,$isvideo);
	 
		return $info;
	}
    
    /**
	 * 视频价格
	 * @desc 用于用户设置视频聊天价格
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
    //
	public function setVideoValue() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $value=checkNull($this->value);

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->setVideoValue($uid,$value);
	 
		return $info;
	}
    
    /**
	 * 视频收费说明
	 * @desc 用于查看收费说明
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[].thumb 等级图标
	 * @return string info[].coin 价格
	 * @return string msg 提示信息
	 */
	public function getVideoInfo2() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
		$domain = new Domain_User();
		$levellist = getLevelanchorList();
		$list = $domain->getVideo();
        foreach($levellist as $k=>$v ){
            $v['coin']='0';
            foreach($list as $k1=>$v1){
                if($v['level']>=$v1['level']){
                    $v['coin']=$v1['coin'];
                }
            }
            unset($v['id']);
            unset($v['level_up']);
            $levellist[$k]=$v;
        }
        
        $rs['info']=$levellist;
		return $rs;
	}
    

    /**
	 * 编辑用户资料
	 * @desc 用于编辑用户资料
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].avatar 头像
	 * @return string info[0].avatar_thumb 小头像
	 * @return string info[0].user_nickname 昵称
	 * @return string info[0].audio 语音自我介绍
	 * @return int info[0].audio_length 语音时长
	 * @return int info[0].sex 性别
	 * @return int info[0].height 身高
	 * @return int info[0].weight 体重
	 * @return string info[0].constellation 星座
	 * @return string info[0].province 省份
	 * @return string info[0].city 城市
	 * @return string info[0].district 地区
	 * @return string info[0].intr 介绍
	 * @return string info[0].signature 签名
	 * @return string info[0].label_list 标签列表
	 * @return string info[0].name 姓名
	 * @return string msg 提示信息
	 */
	public function upUserInfo() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $avatar=checkNull($this->avatar);
        $name=checkNull($this->name);
        $audio=checkNull($this->audio);
        $audio_length=checkNull($this->audio_length);
        $sex=checkNull($this->sex);
		$height=checkNull($this->height);
		$weight=checkNull($this->weight);
		$constellation=checkNull($this->constellation);
		$checkToken=checkToken($uid,$token);

		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

        $post_data=[];

        if($avatar){

        	$configpri=getConfigPri();
        	$cloudtype=$configpri['cloudtype'];

        	$post_data['avatar']=$avatar;
        	if($cloudtype==1){
        		$post_data['avatar_thumb']=$avatar.'?imageView2/2/w/200/h/200';
        	}else{
        		$post_data['avatar_thumb']=$avatar;
        	}
        	
        }

		if($name){

			if($name=='用户已注销' || $name=='已注销'){
                $rs['code'] = 1003;
                $rs['msg'] = '请填写正确的昵称';
                return $rs;
            }
			
            $count=mb_strlen($name);
            if($count>7){
                $rs['code'] = 1002;
                $rs['msg'] = '昵称最多7个字';
                return $rs;
            }

			$is_has_sensitive=checkSensitiveWords($name);

	        if($is_has_sensitive){
	        	$rs['code'] = 1001;
				$rs['msg'] = '昵称输入非法,请重新输入';
				return $rs;
	        }

	       $post_data['user_nickname']=$name;
		}
        
		if($audio){
			$post_data['audio']=$audio;

			if($audio_length>15){
				$audio_length=15;
			}

			if($audio_length<0){
				$audio_length=0;
			}

			$post_data['audio_length']=$audio_length;
		}

		if($sex){
			if(!in_array($sex, ['1','2'])){
				$rs['code'] = 1002;
				$rs['msg'] = '性别错误';
				return $rs;
			}

			$post_data['sex']=$sex;
		}

		if($height){
			if(!is_numeric($height)){
				$rs['code'] = 1003;
				$rs['msg'] = '身高必须填写数字';
				return $rs;
			}

			if($height>220){
				$rs['code'] = 1004;
				$rs['msg'] = '身高错误';
				return $rs;
			}

			$post_data['height']=$height;
		}

		if($weight){
			if(!is_numeric($weight)){
				$rs['code'] = 1005;
				$rs['msg'] = '体重必须填写数字';
				return $rs;
			}

			if($weight>300){
				$rs['code'] = 1006;
				$rs['msg'] = '体重错误';
				return $rs;
			}

			$post_data['weight']=$weight;
		}

		if($constellation){
			$post_data['constellation']=$constellation;
		}	

        if(empty($post_data)){
        	$rs['code'] = 1014;
            $rs['msg'] = '至少选择一个修改项';
            return $rs;
        }
        
		$domain = new Domain_User();
		$info = $domain->upUserInfo($uid,$post_data);
	 
		return $info;
	}
    
	/**
	 * 关注列表
	 * @desc 用于获取用户的关注列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[].fans 粉丝数
	 * @return string info[].isvip 是否VIP，0否1是
	 * @return string msg 提示信息
	 */
	//
	public function getFollowsList() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->getFollowsList($uid,$p);
	 
		$rs['info']=$info;
		return $rs;
	}
	/**
	 * 粉丝列表
	 * @desc 用于获取用户的关注列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
     * @return string info[].coin 余额
     * @return string info[].isauth 是否认证，0否1是
     * @return string info[].u2t 是否关注对方，0否1是
     * @return string info[].isvip 是否VIP，0否1是
	 * @return string msg 提示信息
	 */
	//
	public function getFansList() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->getFansList($uid,$p);
	 
		$rs['info']=$info;
		return $rs;
	}
    
    /**
	 * 关注/取消关注
	 * @desc 用于关注/取消关注
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].isattent 关注信息，0表示未关注，1表示已关注
	 * @return string msg 提示信息
	 */
    //
	public function setAttent() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $touid=checkNull($this->touid);
        
        if($uid < 1 || $token=='' || $touid < 1){
            $rs['code']=1001;
			$rs['msg']='信息错误';
			return $rs;
        }

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$domain = new Domain_User();
		$info = $domain->setAttent($uid,$touid);
	 
		return $info;
	}
    
    /**
	 * 礼物柜
	 * @desc 用于获取主播的收礼物情况
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].total 总价值
	 * @return string info[0].nums 总数量
	 * @return array info[0].list 礼物列表
	 * @return string info[0].list[].thumb 图片
	 * @return string info[0].list[].name 名称
	 * @return string info[0].list[].total_nums 数量
	 * @return string msg 提示信息
	 */
    //
	public function getGiftCab() {
		$rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
		
        $liveuid=checkNull($this->liveuid);

        if($liveuid<1){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
       
 
       
        
		$domain = new Domain_User();
		$info = $domain->getGiftCab($liveuid);
	 
        $rs['info'][0]=$info;
		return $rs;
	}


    /**
	 * 个人主页
	 * @desc 用于获取主播的个人主页
	 * @return int code 操作码，0表示成功
	 * @return array info 
     * @return string info[0].id 用户ID
     * @return string info[0].level_anchor 主播等级
     * @return string info[0].fans 粉丝数
     * @return string info[0].isvip 是否VIP，0否1是
     * @return string info[0].isvoice 语音开关，0关1开
	 * @return string info[0].voice_value 语音价格
	 * @return string info[0].isvideo 视频开关，0关1开
	 * @return string info[0].video_value 视频价格
	 * @return string info[0].isdisturb 勿扰开关，0关1开
	 * @return string info[0].goodnums 好评数
	 * @return string info[0].badnums 差评数
	 * @return string info[0].online 状态，0离线，1勿扰，2在聊，3在线
	 * @return string info[0].last_online_time 最后登录
     * @return string info[0].thumb 封面
     * @return array  info[0].photos_list 背景图
     * @return string  info[0].photos_list[].type 类型，0图片1视频
     * @return string  info[0].photos_list[].thumb 图片链接
     * @return string  info[0].photos_list[].href 视频
     * @return string info[0].name 姓名
     * @return string info[0].mobile 手机号
     * @return string info[0].sex 性别
     * @return string info[0].height 身高
     * @return string info[0].weight 体重
     * @return array  info[0].label_list 形象标签
     * @return string info[0].label_list[].name 名称
     * @return string info[0].label_list[].colour 颜色
     * @return string info[0].province 省
     * @return string info[0].city 市
     * @return string info[0].district 区
     * @return string info[0].intr 个人介绍
     * @return string info[0].signature 个性签名
     * @return string info[0].audio 语音介绍
     * @return string info[0].answer_rate 接听率
     * @return string info[0].isattent 是否关注，0否1是
     * @return string info[0].gift_total 礼物总价
     * @return array  info[0].gift_list 礼物列表
     * @return string info[0].gift_list[].name 名称
     * @return string info[0].gift_list[].thumb 图标
     * @return array  info[0].evaluate_list 用户印象
     * @return string info[0].evaluate_list[].name 名称
     * @return string info[0].evaluate_list[].colour 颜色
	 * @return string msg 提示信息
	 */
    //
	public function getUserHome() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
        $uid=checkNull($this->uid);
        $liveuid=checkNull($this->liveuid);

        if($liveuid<1){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }

        $domain = new Domain_User();
        $info = $domain->getUserHome($uid,$liveuid);
	 
        $rs['info'][0]=$info;
		return $rs;
	}
	


	/**
     * 获取用户的基本资料
     * @desc 获取用户的基本资料
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].avatar 头像
	 * @return string info[0].avatar_thumb 小头像
	 * @return string info[0].user_nickname 昵称
	 * @return string info[0].audio 语音自我介绍
	 * @return int info[0].audio_length 语音时长
	 * @return int info[0].sex 性别
	 * @return int info[0].height 身高
	 * @return int info[0].weight 体重
	 * @return string info[0].constellation 星座
	 * @return string info[0].province 省份
	 * @return string info[0].city 城市
	 * @return string info[0].district 地区
	 * @return string info[0].intr 介绍
	 * @return string info[0].signature 签名
	 * @return string info[0].label_list 标签列表
	 * @return string info[0].name 姓名
     * @return string msg 提示信息
     */
	//
	public function getUserMaterial(){
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);

        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

		$domain = new Domain_User();
        
        $info = $domain->getUserMaterial($uid);

        $rs['info'][0]=$info;

        return $rs;
	}
	
	
}
