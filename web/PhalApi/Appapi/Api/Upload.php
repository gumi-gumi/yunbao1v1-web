<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/**
 * 上传
 */

class Api_Upload extends PhalApi_Api {

	public function getRules() {
        return array(
        );
	}
	
	/**
     * 七牛Token
     * @desc 用于获取七牛云存储上传使用的Token
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].token 七牛Token
     * @return string msg 提示信息
     */
	public function getQiniuToken(){
	
	   	$rs = array('code' => 0, 'msg' => '', 'info' =>array());
        
        
		$token = DI()->qiniu->getQiniuToken();
		$rs['info'][0]['token']=$token; 
		return $rs; 
		
	}
	
	
	
	/**
     * 获取云存储方式、获取七牛上传验证token字符串、获取腾讯云存储相关配置信息
     * @desc 用于获取云存储方式、获取七牛上传验证token字符串、获取腾讯云存储相关配置信息
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     */

    public function getCosInfo(){

        $rs=array("code"=>0,"msg"=>"","info"=>array());

        //获取七牛信息
        $qiniuToken=$this->getQiniuToken();

        //获取腾讯云存储配置信息
		$configpri=getConfigPri();

        if(!$configpri['cloudtype']){
            $rs['code']=1001;
            $rs['msg']="无指定存储方式";
            return $rs;
        }
		$space_host= DI()->config->get('app.Qiniu.space_host');
		$region= DI()->config->get('app.Qiniu.region');
		
		//七牛云存储区域 华东：z0，华北：z1，华南：z2，北美：na0，东南亚：as0，参考文档：https://developer.qiniu.com/kodo/manual/1671/
		$qiniu_zone=='';
		if($region=='z0'){
			$qiniu_zone='qiniu_hd';
		}else if($region=='z1'){
			$qiniu_zone='qiniu_hb';
		}else if($region=='z2'){
			$qiniu_zone='qiniu_hn';
		}else if($region=='na0'){
			$qiniu_zone='qiniu_bm';
		}else if($region=='as0'){
			$qiniu_zone='qiniu_xjp';
		}
		

		
        $qiniuInfo=array(
            'qiniuToken'=>$qiniuToken['info'][0]['token'],
            'qiniu_domain'=>$space_host,
            'qiniu_zone'=>$qiniu_zone  //华东:qiniu_hd 华北:qiniu_hb  华南:qiniu_hn  北美:qiniu_bm   新加坡:qiniu_xjp 不可随意更改，app已固定好规则
        );

        $awsInfo=array(
            'aws_bucket'=>$configpri['aws_bucket'],
            'aws_region'=>$configpri['aws_region'],
            'aws_identitypoolid'=>$configpri['aws_identitypoolid'],
        );
        
        $rs['info'][0]['qiniuInfo']=$qiniuInfo;
        $rs['info'][0]['awsInfo']=$awsInfo;

        $cloudtype="";
        switch ($configpri['cloudtype']) {
            case '1':
                $cloudtype="qiniu";
                break;

            case '2':
                $cloudtype="aws";
                break;
         } 

        $rs['info'][0]['cloudtype']=$cloudtype;
        
        return $rs;
        
    }
    
    
    
    
}
