<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 首页
 */

class Api_Home extends PhalApi_Api {

	public function getRules() {
        return array(
            'getHot' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'sex' => array('name' => 'sex', 'type' => 'int', 'default'=>'0', 'desc' => '性别，0全部，1男，2女'),
				'type' => array('name' => 'type', 'type' => 'int','default'=>'0', 'desc' => '通话类型，0全部，1视频，2语音'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'0', 'desc' => '页码'),
            ),

            'search' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'key' => array('name' => 'key', 'type' => 'string', 'default'=>'' ,'desc' => '关键词'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),

			 'profitList'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','min'=>1, 'desc' => '用户ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
                'type' => array('name' => 'type', 'type' => 'string', 'default'=>'day' ,'desc' => '参数类型，day表示日榜，week表示周榜，month代表月榜，total代表总榜'),
            ),
			'consumeList'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','min'=>1, 'desc' => '用户ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
                'type' => array('name' => 'type', 'type' => 'string', 'default'=>'day' ,'desc' => '参数类型，day表示日榜，week表示周榜，month代表月榜，total代表总榜'),
            ),
        );
	}
	
    /**
     * 网站信息
     * @desc 用于获取网站基本信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].site_name 网站名称
     * @return string info[0].name_votes 收益币名称
     * @return string info[0].name_coin 消费币名称
     * @return string info[0].apk_ver APK版本号
     * @return string info[0].apk_des APK更新说明
     * @return string info[0].apk_url APK下载链接
     * @return string info[0].ipa_ver IPA版本号
     * @return string info[0].ios_shelves IPA上架版本号
     * @return string info[0].ipa_des IPA更新说明
     * @return string info[0].ipa_url IPA下载链接
     * @return array info[0].login_type 登录方式
     * @return array info[0].share_type 分享方式
     * @return array info[0].levellist 等级列表
     * @return string info[0].levellist[].level 等级
     * @return string info[0].levellist[].thumb 等级图标
     * @return array info[0].levelanchorlist 主播等级列表
     * @return string info[0].levelanchorlist[].level 等级
     * @return string info[0].levelanchorlist[].thumb 等级图标
     * @return string info[0].im_tips 等级图标
     * @return string msg 提示信息
     */
	public function getConfig() {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $info=getConfigPub();
        unset($info['site_url']);
        unset($info['site_seo_title']);
        unset($info['site_seo_keywords']);
        unset($info['site_seo_description']);
        unset($info['copyright']);
        unset($info['qr_url']);
        
        $info_pri=getConfigPri();
        
        $info['login_type']=$info_pri['login_type'];
        $info['share_type']=$info_pri['share_type'];
        
        $level=getLevelList();
        foreach($level as $k=>$v){
            $v['level']=(string)$v['level'];
            unset($v['id']);
            unset($v['level_up']);
            unset($v['levelname']);
            $level[$k]=$v;
        }
        $info['levellist']=$level;
        
        $levelanchor=getLevelanchorList();
        foreach($levelanchor as $k=>$v){
            $v['level']=(string)$v['level'];
            unset($v['id']);
            unset($v['level_up']);
            unset($v['levelname']);
            $levelanchor[$k]=$v;
        }
        $info['levelanchorlist']=$levelanchor;
        
        /* 聊天室群组 */
        $info['full_group_id']='';
        $full_group_id=$info_pri['im_full_group_id'];
        if($full_group_id){
            $api=getTxRestApi();
            
            $ret = $api->group_get_group_info2(array('0'=>$full_group_id),[],[],[]);
           
            $info['full_group_id'] = $ret['GroupInfo'][0]['GroupId'];

            if($ret['GroupInfo'][0]['ErrorCode']){
                $ret = $api->full_group_create($full_group_id);
                 
                if($ret['ActionStatus']!='OK'){
                    $ret = $api->full_group_create($full_group_id);
                    
                }
                $info['full_group_id'] = $ret['GroupId'];
            } 
        }
		
		
		/* 引导页 */
        $domain = new Domain_Guide();
		$guide_info = $domain->getGuide();
        
        $info['guide']=$guide_info;
        
        /* 私信提示 */
        $im_tips='';
        $im_limit=$info_pri['im_limit'];
        if($im_limit>0){
            $im_tips='免费'.$im_limit.'条/天，开通会员无限畅聊';
        }
        $info['im_tips']=$im_tips;
        
        $rs['info'][0] = $info;
		
        return $rs;
	}


    /**
     * 推荐
     * @desc 用于获取首页信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0].slide 轮播
     * @return string info[0].slide[].image 图片
     * @return string info[0].slide[].url 链接
     * @return array info[0].list 列表
     * @return string info[0].list[].online 状态，0离线，1勿扰，2在聊，3在线
     * @return string info[0].list[].isvoice 语音开关，0关，1开
     * @return string info[0].list[].voice_value 语音价格
     * @return string info[0].list[].isvideo 视频开关，0关，1开
     * @return string info[0].list[].video_value 视频价格，0关，1开
     * @return string info[0].list[].thumb 封面
     * @return string msg 提示信息
     */
	public function getHot() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $domain = new Domain_Home();
        
        $slide = $domain->getSlide();
        
        $uid=checkNull($this->uid);
        $sex=checkNull($this->sex);
        $type=checkNull($this->type);
        $p=checkNull($this->p);
		
		//收益（魅力）榜
		$profittop = $domain->getProfittop();
		//消费（土豪）榜
        $consumetop=$domain->getConsumetop();
        
        $list= $domain->getList($uid,$sex,$type,$p);
        
        $info['slide']=$slide;
        $info['list']=$list;
		$info['profittop']=$profittop;
        $info['consumetop']=$consumetop;
        
        
        $rs['info'][0] = $info;
		
        return $rs;
	}






	/**
     * 搜索
     * @desc 用于首页搜索主播
     * @return int code 操作码，0表示成功
     * @return array info 会员列表
     * @return string info[].id 用户ID
     * @return string info[].user_nickname 用户昵称
     * @return string info[].avatar 头像
     * @return string info[].sex 性别
     * @return string info[].signature 签名
     * @return string info[].level 等级
     * @return string info[].fans 粉丝数
     * @return string msg 提示信息
     */
    public function search() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
		$key=checkNull($this->key);
		$p=checkNull($this->p);
		if($key==''){
			$rs['code'] = 1001;
			$rs['msg'] = "请填写关键词";
			return $rs;
		}
		
        $domain = new Domain_Home();
        $info = $domain->search($uid,$key,$p);

        $rs['info'] = $info;

        return $rs;
    }

     /**
     * 收益榜单(魅力榜)
     * @desc 获取收益榜单
     * @return int code 操作码 0表示成功
     * @return string msg 提示信息 
     * @return array info
     * @return string info[0]['user_nickname'] 主播昵称
     * @return string info[0]['avatar_thumb'] 主播头像
     * @return string info[0]['totalcoin'] 主播钻石数
     * @return string info[0]['uid'] 主播id
     * @return string info[0]['levelAnchor'] 主播等级
     * @return string info[0]['isAttention'] 是否关注主播 0 否 1 是
     **/
    
    public function profitList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $p=checkNull($this->p);
        $type=checkNull($this->type);
        $domain=new Domain_Home();
        $res=$domain->profitList($uid,$type,$p);

        $rs['info']=$res;
        return $rs;
    }
	
	/**
     * 消费（土豪）榜单
     * @desc 获取消费榜单
     * @return int code 操作码 0表示成功
     * @return string msg 提示信息 
     * @return array info
     * @return string info[0]['user_nickname'] 用户昵称
     * @return string info[0]['avatar_thumb'] 用户头像
     * @return string info[0]['totalcoin'] 用户钻石数
     * @return string info[0]['uid'] 用户id
     * @return string info[0]['levelAnchor'] 用户等级
     * @return string info[0]['isAttention'] 是否关注用户 0 否 1 是
     **/
    
    public function consumeList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $p=checkNull($this->p);
        $type=checkNull($this->type);
        $domain=new Domain_Home();
        $res=$domain->consumeList($uid,$type,$p);

        $rs['info']=$res;
        return $rs;
    }
    
    
    
}
