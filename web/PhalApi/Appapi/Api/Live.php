<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 私聊
 */

class Api_Live extends PhalApi_Api {

	public function getRules() {
        return array(
            'checkstatus' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'desc' => '对方ID'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
			),
            'checklive' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，1视频，2语音'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
			),
            
            'anchorLaunch' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'desc' => '对方ID'),
				'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，1视频，2语音'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
			),
            
            'toAppointment' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'subscribeid' => array('name' => 'subscribeid', 'type' => 'int', 'desc' => '预约ID'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
			),

            'anchorAnswer' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'desc' => '通话对象ID'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => ''),
			),
            
            'userAnswer' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => ''),
			),
            
            'userHang' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
				'hangtype' => array('name' => 'hangtype', 'type' => 'int', 'desc' => '类型，0等待中挂断1等待结束挂断2通话中挂断'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
			),
            
            'anchorHang' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'desc' => '通话对象ID'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => ''),
			),
            
            'timeCharge' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
			),

			'checkConversa'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'desc' => '通话标识'),
			)
        );
	}
	
	/**
	 * 用户关系
	 * @desc 用于检测两个用户间关系
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return int info[0].status 关系类型，0用户邀主播，1主播邀用户
	 * @return string msg 提示信息
	 */
	public function checkstatus() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $touid=checkNull($this->touid);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $touid<1 || $sign=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'touid'=>$touid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $uid_auth=isAuthorAuth($uid);
        $touid_auth=isAuthorAuth($touid);
        
        
        if($touid_auth==1){
            $status='0';
        }else if($uid_auth==1){
            $status='1';
        }else{
            $rs['code'] = 1002;
			$rs['msg'] = '对方尚未认证成为主播';
			return $rs;
        }
        
        $rs['info'][0]['status']=$status;
        
        return $rs;
	}

	/**
	 * 用户拨打
	 * @desc 用于用户拨打主播
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return int info[0].type 通话类型
	 * @return int info[0].total 价格
	 * @return int info[0].showid 通话标识
	 * @return int info[0].push 推流地址
	 * @return int info[0].pull 播流地址
	 * @return string msg 提示信息
	 */
	//
	public function checklive() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $liveuid=checkNull($this->liveuid);
        $type=checkNull($this->type);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $liveuid<1 || ($type!=1 && $type !=2) ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
		
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'liveuid'=>$liveuid,
            'type'=>$type,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		
		
        
        $domain = new Domain_Live();
		$info = $domain->checklive($uid,$liveuid,$type);
        
        return $info;
	}

	/**
	 * 主播发起
	 * @desc 用于主播发起通话
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return int info[0].type 通话类型
	 * @return int info[0].total 价格
	 * @return int info[0].showid 通话标识
	 * @return int info[0].push 推流地址
	 * @return int info[0].pull 播流地址
	 * @return string msg 提示信息
	 */
	public function anchorLaunch() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $touid=checkNull($this->touid);
        $type=checkNull($this->type);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $touid<1 || ($type!=1 && $type !=2) ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
		
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'touid'=>$touid,
            'type'=>$type,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $uid_auth=isAuthorAuth($uid);
        $touid_auth=isAuthorAuth($touid);
        
        if($uid_auth!=1){
            $rs['code'] = 1002;
			$rs['msg'] = '您尚未认证成为主播';
			return $rs;
        }
        
        if($touid_auth==1){
            $rs['code'] = 1003;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        
        $domain = new Domain_Live();
		$info = $domain->anchorLaunch($uid,$touid,$type);
        
        return $info;
	}

	/**
	 * 主播接听
	 * @desc 用于主播接听用户通话时处理
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
     * @return int info[0].push 推流地址
	 * @return int info[0].pull 播流地址
	 * @return string msg 提示信息
	 */
	//
	public function anchorAnswer() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $touid=checkNull($this->touid);
        $showid=checkNull($this->showid);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $touid<1 || $showid<1 || $sign=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'touid'=>$touid,
            'showid'=>$showid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Live();
		$info = $domain->anchorAnswer($uid,$touid,$showid);
        
        return $info;
	}

	/**
	 * 用户接听
	 * @desc 用于用户接听
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
     * @return int info[0].type 类型，1视频2语音
     * @return int info[0].total 价格
     * @return int info[0].push 推流地址
	 * @return int info[0].pull 播流地址
	 * @return string msg 提示信息
	 */
	public function userAnswer() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $liveuid=checkNull($this->liveuid);
        $showid=checkNull($this->showid);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $liveuid<1 || $showid<1 || $sign=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'liveuid'=>$liveuid,
            'showid'=>$showid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Live();
		$info = $domain->userAnswer($uid,$liveuid,$showid);
        
        return $info;
	}
    
	/**
	 * 用户挂断
	 * @desc 用于用户挂断通话
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return string msg 提示信息
	 */
	public function userHang() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $liveuid=checkNull($this->liveuid);
        $showid=checkNull($this->showid);
        $hangtype=checkNull($this->hangtype);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $liveuid<1 || $showid<1 || $sign=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'liveuid'=>$liveuid,
            'showid'=>$showid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Live();
		$info = $domain->hangUp($uid,$liveuid,$showid,$hangtype,'0');
        
        return $info;
	}

	/**
	 * 主播挂断
	 * @desc 用于主播挂断通话
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return string info[0].length 时长
	 * @return string info[0].gifttotal  礼物收益
	 * @return string info[0].answertotal 通话收益
	 * @return string msg 提示信息
	 */
	//
	public function anchorHang() {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $touid=checkNull($this->touid);
        $showid=checkNull($this->showid);
        $sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $touid<1 || $showid<1 || $sign=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'touid'=>$touid,
            'showid'=>$showid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Live();
		$info = $domain->hangUp($touid,$uid,$showid,0,'1');
        
        return $info;
	}

	/**
	 * 计时扣费
	 * @desc 用于检测用户和主播信息
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return int info[0].level 等级
	 * @return int info[0].coin 余额
	 * @return int info[0].istips 是否提示
	 * @return int info[0].tips 提示内容
	 * @return string msg 提示信息
	 */
	//
	public function timeCharge() {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $liveuid=checkNull($this->liveuid);
        $showid=checkNull($this->showid);
        
        if($uid<1 || $token=='' || $liveuid<1 || $showid<1){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Live();
		$info = $domain->roomCharge($uid,$liveuid,$showid);
        
        return $info;
	}
    	
    /**
	 * 检测通话【用于android】
	 * @desc 检测通话【用于android】
	 * @return int code 操作码，0表示成功， 1表示用户不存在
	 * @return array info 
	 * @return string msg 提示信息
	 */
    public function checkConversa(){
    	$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $showid=checkNull($this->showid);

        if($uid<1 || $token=='' || $showid<1){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

		$domain = new Domain_Live();
		$info = $domain->checkConversa($showid);
        
        $rs['info']=$info;
        return $rs;
    }
    
    
}
