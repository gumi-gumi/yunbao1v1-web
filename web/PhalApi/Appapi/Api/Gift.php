<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 礼物
 */

class Api_Gift extends PhalApi_Api {

	public function getRules() {
        return array(
            'getGiftList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
			),
            
            'sendGift' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'giftid' => array('name' => 'giftid', 'type' => 'int', 'desc' => '礼物ID'),
				'nums' => array('name' => 'nums', 'type' => 'int', 'desc' => '礼物数量'),
				'showid' => array('name' => 'showid', 'type' => 'int', 'default'=>'0','desc' => '通话标识'),
			),
        );
	}
	

    /**
     * 礼物列表
     * @desc 用于获取礼物信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].coin 用户余额
     * @return array info[].list 礼物礼物
     * @return string msg 提示信息
     */
	public function getGiftList() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $userinfo = getUserCoin($uid);
        
        $domain = new Domain_Gift();
        
        $list = $domain->getGiftList();
        
        $rs['info'][0]['coin'] = $userinfo['coin'];
        $rs['info'][0]['list'] = $list;
		
        return $rs;
	}

	/**
	 * 赠送礼物 
	 * @desc 用于赠送礼物
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].level 用户等级
	 * @return string info[0].coin 用户余额
	 * @return string info[0].swftype 动画类型，0gif,1svga
	 * @return string info[0].swf 动画链接
	 * @return string info[0].swftime 动画时长
	 * @return string msg 提示信息
	 */
	public function sendGift() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
		$uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$liveuid=checkNull($this->liveuid);
		$giftid=checkNull($this->giftid);
		$nums=checkNull($this->nums);
		$showid=checkNull($this->showid);
        
        if($uid<1 || $token=='' || $liveuid<1 || $giftid<1 || $nums <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
		
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		


		$domain = new Domain_Gift();
		$result=$domain->sendGift($uid,$liveuid,$giftid,$nums,$showid);

		return $result;

	}

}
