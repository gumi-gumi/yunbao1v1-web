<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 背景墙
 */

class Api_Wall extends PhalApi_Api {

	public function getRules() {
        return array(
            'myWall' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
			),
            
            'setWall' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'action' => array('name' => 'action', 'type' => 'int', 'desc' => '行为,0新加,1替换'),
				'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，0图片1视频'),
				'oldid' => array('name' => 'oldid', 'type' => 'int', 'desc' => '旧背景ID'),
				'newid' => array('name' => 'newid', 'type' => 'int', 'desc' => '新背景ID'),
			),
            
            'setCover' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '封面'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名，uid thumb '),
			),
            
            'delWall' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'wallid' => array('name' => 'wallid', 'type' => 'int', 'desc' => '背景ID'),
			),
            
        );
	}
    
    /**
     * 背景墙
     * @desc 用于获取背景墙信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].id 背景ID
     * @return string info[0].type 类型，0图片1视频
     * @return string info[0].thumb 图片链接
     * @return string info[0].href 视频链接
     * @return string msg 提示信息
     */
	public function myWall() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Wall();
		$list=$domain->myWall($uid);
        
        $rs['info']=$list['photos'];
        
        return $rs;
	}

    /**
     * 设置背景
     * @desc 用于用户取消配置
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function setWall() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$action=checkNull($this->action);
		$type=checkNull($this->type);
		$oldid=checkNull($this->oldid);
		$newid=checkNull($this->newid);
        
        if($uid<1 || $token=='' || $newid < 1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        
        $domain = new Domain_Wall();
		$result=$domain->setWall($uid,$action,$type,$oldid,$newid);
        
        return $result;
	}

    /**
     * 设置封面
     * @desc 用于主播开始匹配
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function setCover() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$thumb=checkNull($this->thumb);
		$sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $thumb == '' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'thumb'=>$thumb
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Wall();
		$result=$domain->setCover($uid,$thumb);
        
        return $result;
	}

    /**
     * 删除背景
     * @desc 用于主播开始匹配
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function delWall() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$wallid=checkNull($this->wallid);
        
        if($uid<1 || $token=='' || $wallid < 1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }

        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $domain = new Domain_Wall();
		$result=$domain->delWall($uid,$wallid);
        
        return $result;
	}
    
}
