<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Home extends PhalApi_Model_NotORM {

	/* 轮播 */   	
    public function getSlide($slide_id) {

		$list=DI()->notorm->slide_item
				->select('image,url')
				->where('slide_id=? and  status =1',$slide_id)
                ->order('list_order asc')
				->fetchAll();
        foreach($list as $k=>$v){
            $v['image']=get_upload_path($v['image']);
            $v['url']=html_entity_decode($v['url']);
            
            $list[$k]=$v;
        }
                
        return $list; 
    }
	
	/* 主播列表 */
    public function getList($where='',$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
        
        $list=DI()->notorm->user
				->select('id,user_nickname,avatar,sex,signature,goodnums,badnums,online,isvoice,voice_value,isvideo,video_value,isdisturb,votestotal')
				->where('user_type=2')
				//->where('user_type=2 and  isauthor_auth =1')
                ->where($where)
                ->order('online desc,id desc,recommend_val desc,level_anchor desc')
                ->limit($start,$pnum)
				->fetchAll();  
		return $list;
    }


	/* 搜索 */
    public function search($uid,$key,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
        
		$where=' user_type="2" and ( id=? or user_nickname like ? ) and id!=?';

		
		$result=DI()->notorm->user
				->select("id,user_nickname,avatar,sex,signature,consumption,goodnums,badnums,votestotal,isauth,isauthor_auth")
				->where($where,$key,'%'.$key.'%',$uid)
				->order("id desc")
				->limit($start,$pnum)
				->fetchAll();

	
		
		return $result;
    }


	//获取用户信息
	public function getHomeuserinfo($uid){
		$userinfo=DI()->notorm->user
				->select('id,user_nickname,avatar,sex,signature,goodnums,badnums,online,isvoice,voice_value,isvideo,video_value,isdisturb,votestotal')
				->where('user_type=2 and  isauthor_auth =1 and id=?',$uid)
				->fetchOne();  
		return $userinfo;
	}
	
	//收益(魅力)榜
	public function profitList($where='',$p){
		 if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		
		$result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin,touid as uid')
            ->where($where)
            ->group('touid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();
		return $result;
		
	}
	//消费(土豪)榜
	public function consumeList($where='',$p){
		 if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		
		$result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin, uid')
            ->where($where)
            ->group('uid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();
		return $result;
		
	}
	
	//收益(魅力)榜：前三名
	public function getProfittop($where=''){
		$result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin,touid as uid')
            ->where($where)
            ->group('touid')
            ->order('totalcoin desc')
            ->limit(0,3)
            ->fetchAll();
		return $result;		
	}
	
	//消费(土豪)榜：前三名
	public function getConsumetop($where=''){
		$result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin, uid')
            ->where($where)
            ->group('uid')
            ->order('totalcoin desc')
            ->limit(0,3)
            ->fetchAll();
		return $result;
		
	}

}
