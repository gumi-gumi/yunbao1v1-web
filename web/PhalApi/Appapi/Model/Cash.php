<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Cash extends PhalApi_Model_NotORM {
	/* 我的收益 */
	public function getProfit($uid){
		$info= DI()->notorm->user
				->select("votes,votestotal")
				->where('id=?',$uid)
				->fetchOne();

		return $info;
	}
    
	/* 本月提现次数  */
	public function getCashNums($uid){
        $nowtime=time();
        //本月第一天
        $month=date('Y-m-d',strtotime(date("Ym",$nowtime).'01'));
        $month_start=strtotime(date("Ym",$nowtime).'01');

        //本月最后一天
        $month_end=strtotime("{$month} +1 month");
            
		$nums=DI()->notorm->cash_record
                ->where('uid=? and addtime > ? and addtime < ?',$uid,$month_start,$month_end)
                ->count();
        return $nums;
	}

	/* 扣除映票  */
	public function upVotes($uid,$votes){
        
		$rs=DI()->notorm->user
                ->where('id = ? and votes>=?', $uid,$votes)
                ->update(array('votes' => new NotORM_Literal("votes - {$votes}")) );     

		return $rs;
	}

	/* 提现  */
	public function setCash($data){
        
		$rs=DI()->notorm->cash_record->insert($data);
		return $rs;
	}
    
    /* 提现账号列表 */
    public function getUserAccountList($uid){
        
        $list=DI()->notorm->cash_account
                ->select("*")
                ->where('uid=?',$uid)
                ->order("addtime desc")
                ->fetchAll();
                
        return $list;
    }

    /* 提现账号详情 */
    public function getAccount($id){
        
        $info=DI()->notorm->cash_account
                    ->select("*")
                    ->where('id=?',$id)
                    ->fetchOne();
                
        return $info;
    }

    /* 设置提账号 */
    public function setUserAccount($data){
        
        $rs=DI()->notorm->cash_account
                ->insert($data);
                
        return $rs;
    }

    /* 删除提账号 */
    public function delUserAccount($data){
        
        $rs=DI()->notorm->cash_account
                ->where($data)
                ->delete();
                
        return $rs;
    }
}
