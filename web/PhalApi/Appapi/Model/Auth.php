<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Auth extends PhalApi_Model_NotORM {
    


    /* 获取实名认证信息 */   	
    public function getUserAuthInfo($uid) {

		$info=DI()->notorm->user_auth
				->select('*')
				->where('uid=?',$uid)
				->fetchOne();
        return $info;
    }

    //获取主播认证信息
    public function getAuthorAuthInfo($uid){
    	$info=DI()->notorm->author_auth
				->select('*')
				->where('uid=?',$uid)
				->fetchOne();
        return $info;
    }

    public function setUserAuth($data){
    	$rs=DI()->notorm->user_auth
				->insert($data);
                
		return $rs;
    }

    /* 更新认证 */
    public function upUserAuth($uid,$data) {
        
        $rs=DI()->notorm->user_auth
				->where('uid=?',$uid)
				->update($data);
                
		return $rs;
    }

    public function setAuthorAuth($data){
    	$rs=DI()->notorm->author_auth
				->insert($data);
                
		return $rs;
    }

    public function upAuthorAuth($uid,$data){
    	$rs=DI()->notorm->author_auth
				->where('uid=?',$uid)
				->update($data);
                
		return $rs;
    }

}
