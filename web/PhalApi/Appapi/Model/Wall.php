<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Wall extends PhalApi_Model_NotORM {
	/* 我的背景墙 */
	public function myWall($uid) {
		
		$info=DI()->notorm->backwall
				->select('*')
				->where('uid=?',$uid)
                ->order('type desc')
				->fetchAll();

		return $info;
	}
    
    /* 封面信息 */
	public function getCover($uid) {
		
		$info=DI()->notorm->backwall
				->select('*')
				->where('uid=? and type=-1',$uid)
				->fetchOne();

		return $info;
	}

    /* 背景信息 */
	public function getWall($id) {
		
		$info=DI()->notorm->backwall
				->select('*')
				->where('id=?',$id)
				->fetchOne();

		return $info;
	}
    
    
    /* 背景墙数量 */
	public function getWallNums($uid,$id=0) {
		
		$nums=DI()->notorm->backwall
				->where('uid=? and type!=-1 and id!=?',$uid,$id)
				->count();

		return (string)$nums;
	}
    
	/* 新加 */
	public function setWall($data) {

		$result= DI()->notorm->backwall->insert($data);

		return $result;
	}
    
    /* 更新 */
    public function upWall($id,$data) {

		$result= DI()->notorm->backwall
                    ->where('id=?',$id)
                    ->update($data);

		return $result;
	}

    /* 删除 */
    public function delWall($uid,$id=0) {

        if($id){
            $result= DI()->notorm->backwall
                    ->where('uid=? and type != -1 and id=?',$uid,$id)
                    ->delete();
        }else{
            $result= DI()->notorm->backwall
                    ->where('uid=?',$uid)
                    ->delete();
        }
		
		return $result;
	}

}
