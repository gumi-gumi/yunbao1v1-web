<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Cash {
    /* 我的收益 */
	public function getProfit($uid) {
			$rs = array();

			$model = new Model_Cash();
			$info = $model->getProfit($uid);
            
            $configpri=getConfigPri();
		
            //提现比例
            $cash_rate=$configpri['cash_rate'];
            $cash_start=$configpri['cash_start'];
            $cash_end=$configpri['cash_end'];
            $cash_max_times=$configpri['cash_max_times'];
			$cash_take=$configpri['cash_take'];
            //剩余票数
            $votes=$info['votes'];
            
            //总可提现数
            // $total=(string)floor($votes/$cash_rate);
			if(!$cash_rate){
				$total='0';
			}else{
				//总可提现数
				$total=(string)(floor($votes/$cash_rate)*(100-$cash_take)/100);
			}
            
            $tips=$configpri['cash_tip'];

            
            $rs=array(
                "votes"=>$votes,
                "votestotal"=>$info['votestotal'],
                "total"=>$total,
                "cash_rate"=>$cash_rate,
				"cash_take"=>$cash_take,
                "tips"=>$tips,
            );

			return $rs;
	}
    /* 提现 */
	public function setCash($data) {
        
			$rs = array('code' => 0, 'msg' => '提现成功', 'info' => array());
            
            $nowtime=time();
        
            $uid=$data['uid'];
            $accountid=$data['accountid'];
            $cashvote=$data['cashvote'];
            
            $configpri=getConfigPri();
            
            $cash_start=$configpri['cash_start'];
            $cash_end=$configpri['cash_end'];
            $cash_max_times=$configpri['cash_max_times'];
            
            $day=(int)date("d",$nowtime);
            
            if($day < $cash_start || $day > $cash_end){
                $rs['code'] = 1005;
                $rs['msg'] = '不在提现期限内，不能提现';
                return $rs;
            }
            
             //提现比例
            $cash_rate=$configpri['cash_rate'];
			
			/*提现抽成比例*/
			$cash_take=$configpri['cash_take'];
            /* 最低额度 */
            $cash_min=$configpri['cash_min'];
            
            //提现钱数
            $money=floor($cashvote/$cash_rate);
            
            if($money < $cash_min){
                $rs['code'] = 1004;
                $rs['msg'] = '提现最低额度为'.$cash_min.'元';
                return $rs;
            }
            
            
            $model = new Model_Cash();
            
            if($cash_max_times){
                $nums=$model->getCashNums($uid);
                if($nums >= $cash_max_times){
                    $rs['code'] = 1006;
                    $rs['msg'] = '每月只可提现'.$cash_max_times.'次,已达上限';
                    return $rs;
                }
            }
   
            /* 钱包信息 */
            $accountinfo=$model->getAccount($accountid);
            if(!$accountinfo){
                $rs['code'] = 1007;
                $rs['msg'] = '提现账号信息不正确';
                return $rs;
            }

            $cashvotes=$money*$cash_rate;
            
            $ifok=$model->upVotes($uid,$cashvotes);
            if(!$ifok){
                $rs['code'] = 1001;
                $rs['msg'] = '余额不足';
                return $rs;
            }
			//平台抽成后最终的钱数
			$money_take=$money*(1-$cash_take*0.01);
			$money=number_format($money_take,2,".","");
        
            $data=array(
                "uid"=>$uid,
                "money"=>$money,
                "votes"=>$cashvotes,
                "orderno"=>$uid.'_'.$nowtime.rand(100,999),
                "status"=>0,
                "addtime"=>$nowtime,
                "uptime"=>$nowtime,
                "type"=>$accountinfo['type'],
                "account_bank"=>$accountinfo['account_bank'],
                "account"=>$accountinfo['account'],
                "name"=>$accountinfo['name'],
            );
        
			
			$rs = $model->setCash($data);

			return $rs;
	}
    /* 账号列表 */
	public function getUserAccountList($uid) {
        $rs = array();
                
        $model = new Model_Cash();
        $rs = $model->getUserAccountList($uid);

        foreach ($rs as $k => $v) {
            $v['account_bank']=htmlSpecialCharsDecode($v['account_bank']);
            $v['name']=htmlSpecialCharsDecode($v['name']);
            $v['account']=htmlSpecialCharsDecode($v['account']);
            $rs[$k]=$v;
        }

        return $rs;
    }	
    /* 设置账号 */
	public function setUserAccount($data) {
        $rs = array();
                
        $model = new Model_Cash();
        $rs = $model->setUserAccount($data);

        return $rs;
    }
    /* 删除账号 */
	public function delUserAccount($data) {
        $rs = array();
                
        $model = new Model_Cash();
        $rs = $model->delUserAccount($data);

        return $rs;
    }	
}
