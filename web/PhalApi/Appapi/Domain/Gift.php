<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Domain_Gift {
    
    /* 礼物列表 */
    public function getGiftList() {

        $key='getGiftList';
        $list=getcaches($key);
        if(!$list){
            $model = new Model_Gift();
            $list = $model->getGiftList();
            if($list){
                setcaches($key,$list);
            }
        }

        return $list;
    }
    
    /* 送礼物 */
    public function sendGift($uid,$liveuid,$giftid,$nums,$showid) {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        if($uid==$liveuid){
            $rs['code'] = 1004;
			$rs['msg'] = '不能给自己送礼物';
			return $rs;
        }

        $model = new Model_Gift();
        $giftinfo = $model->getGiftInfo($giftid);
        if(!$giftinfo){
            $rs['code'] = 1002;
			$rs['msg'] = '礼物信息不存在';
			return $rs;
        }

        
		$total= $giftinfo['needcoin']*$nums;
		 
		$addtime=time();
		$type='0';
		$action='1';
		
		/* 更新用户余额 消费 */
		$ifok =upCoin($uid,$total);
        if(!$ifok){
            $rs['code'] = 1003;
			$rs['msg'] = '余额不足';
			return $rs;
        }

        $overage=0;
		


		/* 更新直播 魅力值 累计魅力值 */
		addVotes($liveuid,$overage,$total);

        //增加映票收入记录
        $data=array(
        	'type'=>1, //收入
        	'action'=>3, //收礼物
        	'uid'=>$liveuid,
        	'fromid'=>$uid,
        	'actionid'=>$giftid,
        	'nums'=>$nums,
        	'total'=>$overage,
        	'showid'=>$showid,
        	'votes'=>$overage,
        	'addtime'=>$addtime
        );

        addVoteRecord($data);

		$insert=array(
            "type"=>$type,
            "action"=>$action,
            "uid"=>$uid,
            "touid"=>$liveuid,
            "actionid"=>$giftid,
            "nums"=>$nums,
            "totalcoin"=>$total,
            "addtime"=>$addtime 
        );
        
        if($showid){
            $model_live = new Model_Live();
            $model_live->upConGift($uid,$liveuid,$showid,$overage);
            $insert['showid']=$showid;
        }
        
        addCoinRecord($insert);
        
		$userinfo =getUserCoin($uid);
			 
		$level=getLevel($userinfo['consumption']);	
        
        $swf=$giftinfo['swf'] ? get_upload_path($giftinfo['swf']):'';
		
		$info=array(
            "level"=>$level,
            "coin"=>$userinfo['coin'],
            "swftype"=>$giftinfo['swftype'],
            "swf"=>$swf,
            "swftime"=>$giftinfo['swftime'],
        );

        /* 送礼物通知*/
        $action='0';
        if($showid){
            $action='1';
        }
        $userinfo=getUserInfo($uid);
        $ext=[
            'method'=>'sendgift',
            'action'=>$action,
            'uid'=>$uid,
            'avatar'=>$userinfo['avatar'],
            'user_nickname'=>$userinfo['user_nickname'],
            'showid'=>$showid,
            'giftid'=>$giftinfo['id'],
            'giftname'=>$giftinfo['name'],
            'gifticon'=>$giftinfo['thumb'],
            'giftcount'=>$nums,
            'type'=>$giftinfo['type'],
            'swftype'=>$giftinfo['swftype'],
            'swf'=>$swf,
            'swftime'=>$giftinfo['swftime'],
        ];
        
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMCustomElem',       //自定义类型
            'MsgContent' => array(
                'Data' => json_encode($ext),
                'Desc' => '',
                //  'Ext' => $ext,
                //  'Sound' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=(string)$uid;
        $receiver=(string)$liveuid;
        $api=getTxRestApi();
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,1);
		
        $rs['info'][0]=$info;
		return $rs; 

    }
}
