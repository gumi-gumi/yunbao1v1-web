<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_User {
	/* 设置头像、昵称、性别 */
    public function setUserInfo($uid,$avatar,$name,$sex) {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());

        $data=[];
        if($avatar){
            $avatar_q=get_upload_path($avatar);
            $configpri=getConfigPri();
            $cloudtype=$configpri['cloudtype'];
            if($cloudtype==1){
                 
                 $avatar=  $avatar_q.'?imageView2/2/w/600/h/600'; //600 X 600
				 $avatar_thumb=  $avatar_q.'?imageView2/2/w/200/h/200'; // 200 X 200
            }else{
                $avatar=$avatar_q;
                $avatar_thumb=$avatar_q;
            }
            
            $data['avatar']=$avatar;
            $data['avatar_thumb']=$avatar_thumb;
            
        }
        $model = new Model_User();
        if($name){
			
			if($name=='用户已注销' || $name=='已注销'){
                $rs['code'] = 1003;
                $rs['msg'] = '请填写正确的昵称';
                return $rs;
            }
			
            $count=mb_strlen($name);
            if($count>7){
                $rs['code'] = 1004;
                $rs['msg'] = '昵称最多7个字';
                return $rs;
            }
			
			
            
            $data['user_nickname']=$name;
        }
		$data['sex']=$sex;
        
        if(!$data){
            $rs['code'] = 1005;
            $rs['msg'] = '信息错误';
            return $rs; 
        }
        
        $result = $model->upUserInfo($uid,$data);
        
        hMSet("userinfo_".$uid,$data);
        
        $userinfo=$model->getBaseInfo($uid);
        
        $info=[
            'avatar'=>get_upload_path($userinfo['avatar']),
            'avatar_thumb'=>get_upload_path($userinfo['avatar_thumb']),
            'user_nickname'=>$userinfo['user_nickname'],
			'sex'=>$userinfo['sex'],
        ];
        $rs['info'][0]=$info;
        return $rs;
    }

    /* 用户基本信息 */
    public function getBaseInfo($uid) {
        
        $model = new Model_User();
        $info = $model->getBaseInfo($uid);
        
        if($info){
            $info['avatar']=get_upload_path($info['avatar']);
            $info['avatar_thumb']=get_upload_path($info['avatar_thumb']);						
            $info['level']=getLevel($info['consumption']);
            unset($info['consumption']);
            $info['level_anchor']=getLevelanchor($info['votestotal']);
            /* $info['level_anchor']=getLevelanchor($info['goodnums'],$info['badnums']); */
            unset($info['goodnums']);
            unset($info['badnums']);
            $info['follows']=getFollows($uid);
            $fans='0';
            if($info['isauthor_auth']){
                $fans=getFans($uid);
            }
            $info['fans']=$fans;

            $info['isvip']="1";

            unset($info['isauth']);
            unset($info['isauthor_auth']);
        }

        

        return $info;
    }

    /* 视频价格 */
    public function getVideo() {
        $rs = array();

        $key='fee_videolist';
        $list=getcaches($key);
        if(!$list){
            $model = new Model_User();
            $list = $model->getVideo();
            if($list){
                setcaches($key,$list);
            }
        }

        return $list;
    }
    
    
    
    /* 视频开关 */
    public function setVideoSwitch($uid,$isvideo) {

        $rs = array('code' => 0, 'msg' => '视频接听已开启', 'info' => array());
        
        $isvideo=$isvideo ? 1 : 0;

        $data=[
            'isvideo'=>$isvideo
        ];
        $model = new Model_User();
        $info = $model->upUserInfo($uid,$data);
        if($isvideo==0){
            $rs['msg']='视频接听已关闭';
        }
        
        return $rs;
    }
    
    /* 视频价格 */
    public function setVideoValue($uid,$value) {

        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $isok=0;
        $list=$this->getVideo();
        foreach($list as $k=>$v){
            if($v['coin']==$value){
                $isok=1;
                break;
            }
        }
        
        if(!$isok){
            $rs['code'] = 1002;
            $rs['msg'] = '信息错误';
            return $rs;
        }
        
        $data=[
            'video_value'=>$value
        ];
        $model = new Model_User();
        $info = $model->upUserInfo($uid,$data);

        return $rs;
    }
    

    
    
    /* 更新用户信息 */
    public function upUserInfo($uid,$post_data) {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());

        $model = new Model_User();


        $result = $model->upUserInfo($uid,$post_data);

        if($result===false){
            $rs['code'] = 1016;
            $rs['msg'] = '资料编辑失败';
            return $rs;
        }
        
        hMSet("userinfo_".$uid,$post_data);
        
        $userinfo=$model->getBaseInfo($uid);
        
        $info=[
            'avatar'=>get_upload_path($userinfo['avatar']),
            'avatar_thumb'=>get_upload_path($userinfo['avatar_thumb']),
            'user_nickname'=>$userinfo['user_nickname'],
            'audio'=>$userinfo['audio'],
            'audio_length'=>$userinfo['audio_length'],
            'audio_format'=>get_upload_path($userinfo['audio']),
            'sex'=>$userinfo['sex'],
            'height'=>$userinfo['height'],
            'weight'=>$userinfo['weight'],
            'constellation'=>$userinfo['constellation'],

        ];


        $rs['info'][0]=$info;
        return $rs;
    }





    /* 关注列表 */
    public function getFollowsList($uid,$p) {
        $rs = array();

        $model = new Model_User();
        
        
        
        $rs = $model->getFollowsList($uid,$p);
        foreach($rs as $k=>$v){
			$userinfo=getUserInfo($v['touid']);
            $userinfo['fans']=getFans($userinfo['id']);
            $userinfo['isvip']="1";
            
            $rs[$k]=$userinfo;
		}


        return $rs;
    }    
    /* 粉丝列表 */
    public function getFansList($uid,$p) {
        $rs = array();

        $model = new Model_User();
                
        $rs = $model->getFansList($uid,$p);
        foreach($rs as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
            $usercoin=getUserCoin($v['uid']);
            $userinfo['isauth']= isAuthorAuth($v['uid']);
            $userinfo['u2t']= isAttention($uid,$v['uid']);
            $userinfo['coin']='0';
            if($usercoin){
                $userinfo['coin']=$usercoin['coin'];
            }
            $userinfo['coin']=number_format($userinfo['coin']);
			$userinfo['fans']=getFans($userinfo['id']);
            
     
            $userinfo['isvip']="1";
			$userinfo['isblack']=isBlack($uid,$v['uid']);
            
            $rs[$k]=$userinfo;
		}

        return $rs;
    }
    /* 关注、取消关注 */
    public function setAttent($uid,$touid) {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
        if($uid == $touid){
			$rs['code']=1001;
			$rs['msg']='不能关注自己';
			return $rs;
		}
        
        $where="id = {$touid}";
        $isexist=checkUser($where);
        
        if(!$isexist){
            $rs['code']=1001;
			$rs['msg']='关注用户不存在';
			return $rs;
        }
        
        $model = new Model_User();
        
        $isAttention=isAttention($uid,$touid);
        
        if($isAttention){
            /* 已关注 */
            $model->delAttent($uid,$touid);
            $rs['info'][0]['isattent']='0';
        }else{
            /* 未关注 */
            $model->setAttent($uid,$touid);
            $rs['info'][0]['isattent']='1';
        }

        return $rs;
	}

    /* 粉丝列表 */
    public function getBlackList($uid,$p) {
        $rs = array();

        $model = new Model_User();
        
        $domain_vip = new Domain_Vip();
        
        $rs = $model->getBlackList($uid,$p);
        foreach($rs as $k=>$v){
			$userinfo=getUserInfo($v['touid']);
            $rs[$k]=$userinfo;
		}

        return $rs;
    }
    
 

    /* 礼物柜 */
    public function getGiftCab($liveuid) {
        $rs = array();

        $model = new Model_User();

        $rs=$model->getGiftCab($liveuid);
        
        if($rs['nums']>0){
            
            $gift_list=[];
            $domain = new Domain_Gift();
            $giftlist = $domain->getGiftList();
            foreach($giftlist as $k=>$v){
                $gift_list[$v['id']]=$v;
            }

            $list=$rs['list'];
            if($list){
                foreach($list as $k=>$v){
                    $v['thumb']=get_upload_path('/logo.png');
                    $v['name']='礼物已删除';
                    $giftinfo=$gift_list[$v['actionid']];
                    if($giftinfo){
                        $v['thumb']=$giftinfo['thumb'];
                        $v['name']=$giftinfo['name'];
                    }
                    $list[$k]=$v;
                }
                $rs['list']=$list;
            }
        }

        return $rs;
	}

    /* 个人主页 */
    public function getUserHome($uid,$liveuid) {
        $info = array();

        $model = new Model_User();

        $userinfo=$model->getUserHome($liveuid);

        if($userinfo){
            $userinfo['avatar']=get_upload_path($userinfo['avatar']);
            $userinfo['avatar_thumb']=get_upload_path($userinfo['avatar_thumb']);
            $userinfo['audio']=get_upload_path($userinfo['audio']); 

            $userinfo['level_anchor']=getLevelanchor($userinfo['votestotal']);
    
            $fans=getFans($liveuid);
            $userinfo['fans']=$fans;
        }
        $answer_rate = $this->getAnswerRate($liveuid);

        unset($authinfo['id']);
        
        $info=$userinfo;

        
        /* 背景墙 封面 */
        $domain_wall = new Domain_Wall();
        $wallinfo=$domain_wall->myWall($liveuid);
        
        $info['thumb']=$wallinfo['thumb'];
        $info['photos_list']=$wallinfo['photos'];
        
        $info['isattent']=isAttention($uid,$liveuid);
        $info['answer_rate']=$answer_rate;
        if($info['online']==3){
            $info['last_online_time']='在线';
        }else if($info['online']==2){
            $info['last_online_time']='在聊';
        }else if($info['online']==1){
            $info['last_online_time']='勿扰';
        }else{
            $offtime=offtime($info['last_online_time']);
            $info['last_online_time']=$offtime;
        }

        $info['video']=get_upload_path($info['video']);
        $info['video_thumb']=get_upload_path($info['video_thumb']);
        
        
        /* 礼物柜 */
        $giftlist=$this->getGiftCab($liveuid);
        $info['gift_total']=$giftlist['nums'];
        $info['gift_list']=array_slice($giftlist['list'],0,5);
        
        $info['evaluate_list']=array_slice($labellist,0,3);

        
        unset($info['uid']);
        unset($info['reason']);
        unset($info['addtime']);
        unset($info['uptime']);
        unset($info['status']);
        unset($info['labelid']);
        unset($info['label']);
        unset($info['label_c']);

        $info['sex']=(string)$info['sex'];
        
        $info['isvip']="1";
        
        $info['isblack']=0;

        return $info;
	}

    /* 接听率 */
    public function getAnswerRate($liveuid) {
        $info = array();

        $rate='0';
        $model = new Model_User();

        $info=$model->getAnswerRate($liveuid);

        if($info){
            if($info['answer'] && $info['total']){
                $rate=floor($info['answer']/$info['total']*100);
                if($rate>100){
                    $rate=100;
                }
            }
            
        }
        return $rate.'%';
	}


	//获取用户列表
	public function getUserList($uid,$p,$type){
		$rs = array();

		$model = new Model_User();
		$rs = $model->getUserList($uid,$p,$type);
		return $rs;
	}

    //
    public function getUserMaterial($uid){

        $model = new Model_User();
        $info = $model->getUserMaterial($uid);

        if($info){
            $info['avatar']=get_upload_path($info['avatar']);
            $info['avatar_thumb']=get_upload_path($info['avatar_thumb']);
            $info['audio']=get_upload_path($info['audio']);

            if($info['height']==0){
                $info['height']='';
            }

            if($info['weight']==0){
                $info['weight']='';
            }

            $labelid_a=[];
            if($info['labelid']){
                $labelid_a=explode(',',$info['labelid']);
                $label_a=explode(',',$info['label']);
                $label_c_a=explode(',',$info['label_c']);
            }
            

            $label=[];

            if(!empty($labelid_a)){

                foreach ($labelid_a as $k => $v) {
                    $label[]=[
                        'id'=>$v,
                        'name'=>$label_a[$k],
                        'colour'=>$label_c_a[$k],
                    ];
                }
            }
            

            $info['label_list']=$label;
            $info['name']=htmlSpecialCharsDecode($info['name']);
            $info['intr']=htmlSpecialCharsDecode($info['intr']);
            $info['signature']=htmlSpecialCharsDecode($info['signature']);

            unset($info['labelid']);
            unset($info['label']);
            unset($info['label_c']);
        }

        return $info;

    }
	
}
