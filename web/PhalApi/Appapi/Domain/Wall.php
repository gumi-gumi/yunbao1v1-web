<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Domain_Wall {

    /* 我的背景 */
	public function myWall($uid) {

        $thumb='';
        $list=[];
        $model = new Model_Wall();
        $list= $model->myWall($uid);
        
        foreach($list as $k=>$v){
            if($v['type']=='-1'){
                $thumb=get_upload_path($v['thumb']);
                unset($list[$k]);
            }else{
                $v['thumb']=get_upload_path($v['thumb']);
                $v['href']=get_upload_path($v['href']);
                $list[$k]=$v;
            }
            
        }
        
        array_values($list);
        
        $rs=[
            'thumb'=>$thumb,
            'photos'=>$list,
        ];
        
		return $rs;
	}

    /* 设置背景 */
	public function setWall($uid,$action,$type,$oldid,$newid) {
		$rs = array('code' => 0, 'msg' => '设置成功', 'info' => array());
        
        /*
        $isauth=isAuth($uid);
        if(!$isauth){
            $rs['code']=1007;
			$rs['msg']='您还未认证或认证还未通过';
			return $rs;
        }
        */

        if(!$oldid){
            $oldid=0;
        }
        
        /* 新文件链接 */
        $thumb='';
        $href='';
        
        $model = new Model_Wall();
        if($action==1){
            /* 替换 */
            if($oldid<1){
                $rs['code'] = 1006;
                $rs['msg'] = '信息错误';
                return $rs;
            }
            $isexist = $model->getWall($oldid);
            if(!$isexist){
                $rs['code'] = 1006;
                $rs['msg'] = '信息错误';
                return $rs;
            }
        }else{
            $oldid=0;
        }
        
        if($type==1){

        }else{
            /* 图片 */
            $nums=$model->getWallNums($uid,$oldid);
            if($nums>5){
                $rs['code'] = 1004;
                $rs['msg'] = '最多设置6张';
                return $rs;
            }
            
            $model_p = new Model_Photo();
            $info=$model_p->getPhoto($newid);
            if(!$info){
                $rs['code'] = 1005;
                $rs['msg'] = '照片不存在或还未通过审核';
                return $rs;
            }
            
            $thumb=$info['thumb'];
            
            if($info['isprivate']==1){
                $data=[
                    'isprivate'=>'0',
                    'coin'=>'0',
                ];
                $model_p->upPhoto($info['uid'],$info['id'],$data);
            }
        }
        
        
        

        $data=[
            'uid'=>$uid,
            'thumb'=>$thumb,
            'href'=>$href,
            'type'=>$type,
        ];
        
        if($action==1){
            /* 替换 */
            $result=$model->upWall($oldid,$data);
        }else{
            $result=$model->setWall($data);
        }
		
		return $rs;
	}

    /* 设置封面 */
	public function setCover($uid,$thumb) {
		$rs = array('code' => 0, 'msg' => '设置成功', 'info' => array());
        
        /*
        $isauth=isAuth($uid);
        if(!$isauth){
            $rs['code']=1007;
			$rs['msg']='您还未认证或认证还未通过';
			return $rs;
        }
        */
        

        if($thumb==''){
            $rs['code'] = 1004;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $data=[
            'uid'=>$uid,
            'thumb'=>$thumb,
            'type'=>-1,
        ];
        
		$model = new Model_Wall();
        
        $isexist = $model->getCover($uid);
        if($isexist){
            $result=$model->upWall($isexist['id'],$data);
        }else{
            $result=$model->setWall($data);
        }

		return $rs;
	}

    /* 删除封面 */
	public function delWall($uid,$wallid) {
		$rs = array('code' => 0, 'msg' => '删除成功', 'info' => array());
        
        if($wallid<1){
            return $rs;
        }
        
		$model = new Model_Wall();
        
        $nums=$model->getWallNums($uid);
        if($nums<2){
            $rs['code'] = 1002;
			$rs['msg'] = '最少要保留有一张';
			return $rs;
        }
        
        $result=$model->delWall($uid,$wallid);


		return $rs;
	}
	
    /* 旧数据更新 */
    public function oldup(){
        
        $list=DI()->notorm->user_auth
                ->select('*')
                ->fetchAll();
        foreach($list as $k=>$v){
            $uid=$v['uid'];
            $thumb=$v['thumb'];
            $photos=explode(',',$v['photos']);
            $data=[
                'uid'=>$uid,
                'thumb'=>$thumb,
                'href'=>'',
                'type'=>-1,
            ];
            DI()->notorm->backwall->insert($data);
            
            foreach($photos as $k1=>$v1){
                if($v1){
                    $data=[
                        'uid'=>$uid,
                        'thumb'=>$v1,
                        'href'=>'',
                        'type'=>0,
                    ];
                    DI()->notorm->backwall->insert($data);                   
                }
                
            }
            
        }
        
    }
}
