<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


namespace app\appapi\controller;
use cmf\controller\HomeBaseController;
use think\Db;

class MonitorController extends HomebaseController {


	public function index() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$p = $this->request->param('p');

		if(!$p || $p<1){
            $p=1;
        }
		$pnum=8;
		$start=($p-1)*$pnum;
		
		$list = Db::name('conversa_log')
            ->where('status=1')
			->order('id desc')
			->limit($start,$pnum)
            ->select();
		
		$list=$list->all();
		foreach($list as $k=>$v){
			$v['userinfo']= getUserInfo($v['uid']);
			$v['liveinfo']= getUserInfo($v['liveuid']);
			$v['pull_user']=PrivateKey_tx('http',$v['uid'].'_'.$v['showid'],0);
			$v['pull_live']=PrivateKey_tx('http',$v['liveuid'].'_'.$v['showid'],0);

			$cha=time()-$v['starttime'];
			$v['length']=getLength($cha);
			$list[$k]=$v;
			
		}

        		
		$count=Db::name('conversa_log')
				->where('status=1')
				->count();
		if(!$count){
			$count=0;
		}	
		$info=array(
			'count'=>$count,
			'list'=>$list,
		);
		
		$rs['info']=$info;

		echo json_encode($rs);
        die;
		
	}
	
}


