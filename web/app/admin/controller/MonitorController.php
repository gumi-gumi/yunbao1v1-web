<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 监控 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class MonitorController extends AdminBaseController
{
    var $type=array("1"=>"视频通话","2"=>"语音通话");
        
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        $map[]=['status','=','1'];
        if($data['type']!=''){
            $map[]=['type','=',$data['type']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['starttime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['starttime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid|liveuid','=',intval($data['uid'])];
        }
        
        $list = Db::name('conversa_log')
            ->where($map)
            ->order("id desc")
            ->paginate(10);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           $v['liveinfo']= getUserInfo($v['liveuid']);
           $v['pull_user']=PrivateKey_tx('http',$v['uid'].'_'.$v['showid'],0);
           $v['pull_live']=PrivateKey_tx('http',$v['liveuid'].'_'.$v['showid'],0);
           
           $cha=time()-$v['starttime'];
           $v['length']=getLength($cha);
           
           return $v; 
        });
        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('type', $this->type);

        return $this->fetch();
    }
    
    function stopRoom(){
        
        $data = $this->request->param();
        
        $id=intval($data['id']);
        
        if($id<1){
            $this->error("信息错误");
        }

        $info=Db::name('conversa_log')
                ->where(" id={$id} ")
                ->find();

        if(!$info){
            $this->error("通话不存在");
        }
        
        if($info['status'] ==0){
            $this->error("通话未接通");
        }
        
        if($info['status'] ==2){
            $this->error("通话已结束");
        }
        
        $nowtime=time();

        $data=[
            'status'=>2,
            'endtime'=>$nowtime,
        ];
        $length=$nowtime - $info['starttime'];

        $sendtype='1'; //主播给用户发
        if($length < 10){
            $votes=$info['total'];
            reduceVotes($info['liveuid'],$votes,$votes);
            
            Db::name('user_coinrecord')
                ->where("action in (2,3) and touid = {$info['liveuid']} and showid={$info['showid']}")
                ->update(['isdeduct'=>1]);

            $data['total']='0';
        }
        
        Db::name('conversa_log')->where("id={$info['id']}")->update($data);
        

        Db::name('user')->where("id={$info['uid']}")->update( ['online'=>3] );
        Db::name('user')->where("id={$info['liveuid']}")->update( ['online'=>3] );

        
        
        
        $content='通话时长 '.getLength($length,2);
        /* IM */
        /* 用户给主播发 */
        $ext=[
            'method'=>'call',
            'action'=>'9',
            'type'=>$info['type'],
            'content'=>$content,
        ];
        
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMCustomElem',       //自定义类型
            'MsgContent' => array(
                'Data' => json_encode($ext),
                'Desc' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=(string)$info['uid'];
        $receiver=(string)$info['liveuid'];
        $api=getTxRestApi();
        $type= $sendtype==0 ? 1 : 2;
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,$type);
        
        /* 主播给用户发 */
        $ext=[
            'method'=>'call',
            'action'=>'8',
            'type'=>$info['type'],
            'content'=>$content,
        ];
        
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMCustomElem',       //自定义类型
            'MsgContent' => array(
                'Data' => json_encode($ext),
                'Desc' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=(string)$info['liveuid'];
        $receiver=(string)$info['uid'];
        $api=getTxRestApi();
        $type= $sendtype==1 ? 1 : 2;
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,$type);
        
        $this->success("关闭成功");
    }

}