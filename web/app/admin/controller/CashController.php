<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 提现记录 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class CashController extends AdminBaseController
{
    var $status=array("0"=>"未处理","1"=>"已完成","2"=>"已拒绝");
    var $type=array(
        '1'=>'支付宝',
        '2'=>'微信',
        '3'=>'银行卡',
    );
        
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        if($data['keyword']!=''){
            $map[]=['orderno|trade_no','like',"%".$data['keyword']."%"];
        }

        
        $list = Db::name('cash_record')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           return $v; 
        });
        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
		
		$cashrecord_total = DB::name("cash_record")->where($map)->sum("money");
        if($status=='')
        {
            $success=$map;
            $success[]=['status','=',1];
			$pending=$map;
            $pending[]=['status','=',0];
            $fail=$map;
            $fail[]=['status','=',2];
            $cashrecord_success = DB::name("cash_record")->where($success)->sum("money");
            $cashrecord_fail = DB::name("cash_record")->where($fail)->sum("money");
			$cashrecord_pending = DB::name("cash_record")->where($pending)->sum("money");
            $cash['success']=$cashrecord_success;
            $cash['fail']=$cashrecord_fail;
			$cash['pending']=$cashrecord_pending;
            $cash['type']=0;
        }
        $cash['total']=$cashrecord_total;
            
    	$this->assign('cash', $cash);
            
        $this->assign('list', $list);
        $this->assign('status', $this->status);
        $this->assign('type', $this->type);

        return $this->fetch();
    }


    public function setCash()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        if(!$id){
            $this->error("数据传入失败！");
        }
        
        $status = $this->request->param('status');
        
        $result=DB::name("cash_record")->where("id={$id}")->find();				
        if($result){
            if($result['status']!=0){
                $this->error("该订单已处理");
            }

            if($status==2){
                DB::name("user")->where("id='{$result['uid']}'")->setInc("votes",$result['votes']);                
            }
            DB::name("cash_record")->where("id='{$result['id']}'")->update(array("status"=>$status,"uptime"=>time()));

            $this->success('操作成功');
         }else{
            $this->error('数据传入失败！');
         }	
             
    }


}