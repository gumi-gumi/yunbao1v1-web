<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 视频价格 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class FeevideoController extends AdminBaseController
{

    public function index()
    {
        
        $list = Db::name('fee_video')
            ->order("coin asc")
            ->paginate(20);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        return $this->fetch();
    }


    public function add()
    {
        $this->assign('type', $this->type);
        return $this->fetch();
    }

    public function addPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();
            
            $coin=$data['coin'];
            $level=$data['level'];
            if($coin == ''){
                $this->error('请填写价格');
            }else{
                $check = Db::name('fee_video')->where("coin='{$coin}'")->find();
                if($check){
                    $this->error('同一价格已存在');
                }
            }

            if($level==''){
                $this->error('请填写等级');
            }
            
            if($level<=0){
                $this->error('请填写正确等级');
            }

            $id = DB::name('fee_video')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            $this->resetcache();
            $this->success("添加成功！");
        }
    }

    public function edit()
    {
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('fee_video')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('type', $this->type);
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();
            
            $id=$data['id'];
            $coin=$data['coin'];
            $level=$data['level'];
            if($coin == ''){
                $this->error('请填写价格');
            }else{
                $check = Db::name('fee_video')->where("coin='{$coin}' and id !='{$id}'")->find();
                if($check){
                    $this->error('同一价格已存在');
                }
            }

            if($level==''){
                $this->error('请填写等级');
            }
            
            if($level<=0){
                $this->error('请填写正确等级');
            }

            $rs = DB::name('fee_video')->update($data);

            if($rs === false){
                $this->error("保存失败！");
            }
            $this->resetcache();
            $this->success("保存成功！");
        }
    }
    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('fee_video')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        $this->resetcache();
        $this->success("删除成功！",url("feevideo/index"));
    }


    protected function resetcache(){

        $key='fee_videolist';

        $level=DB::name('fee_video')
                ->field('coin,level')
                ->order("coin asc")
                ->select();
        if($level){
            setcaches($key,$level);
        }
    }
}