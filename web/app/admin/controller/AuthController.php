<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/* 身份认证 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AuthController extends AdminBaseController
{
    var $status=array("0"=>"审核中","1"=>"已通过",'2'=>'已拒绝');

    public function index(){
        
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        if($data['keyword']!=''){
            $map[]=['name|mobile','like',"%".$data['keyword']."%"];
        }

        
        $list = Db::name('user_auth')
            ->where($map)
            ->order("addtime desc")
            ->paginate(20);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           
            $v['mobile']=m_s($v['mobile']);

           return $v; 
        });

        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('status', $this->status);

        return $this->fetch();
    }


    public function setstatus(){

        $uid = $this->request->param('uid', 0, 'intval');



        if(!$uid){
            $this->error("数据传入失败！");
        }
        $status = $this->request->param('status', 0, 'intval');
        $reason = $this->request->param('reason');
        
        $result=DB::name("user_auth")->where("uid={$uid}")->find();

        if(!$result){
            $this->error("数据传入失败！");
        }


        if($result['status']!=0 && $result['status'] == $status){
            $this->error("操作失败,请刷新重试");
        }
        
        $nowtime=time();
        
        $rs=DB::name("user_auth")
            ->where("uid={$uid}")
            ->update(
                [
                    'status'=>$status,
                    'reason'=>$reason,
                    'uptime'=>$nowtime
                ]
            );

        if(!$rs){
            $this->error("操作失败1");
        }
        
        
        if($status==1){
            // 更新用户信息
            $updata=[
                'isauth'=>'1',
            ];

            $oldinfo=DB::name("user")->field('isauth')->where("id={$uid}")->find();

            if($oldinfo['isauth']==0){

                $fee_video=DB::name("fee_video")->order('coin asc')->value('coin');
                if($fee_video){
                    $updata['isvideo']='1';
                    $updata['video_value']=$fee_video;
                }
                
            }
            
            DB::name("user")->where("id={$uid}")->update($updata);

            
            $this->resetcache($uid);
        
        }

        if($status==2){
            Db::name("user")->where("id={$uid}")
                ->update(
                    [
                        'isauth'=>0,
                        'isauthor_auth'=>0,
                        'isvoice'=>0,
                        'voice_value'=>0,
                        'isvideo'=>0,
                        'video_value'=>0

                    ]
                );
            Db::name("author_auth")->where("uid={$uid}")->update(['status'=>2,'uptime'=>$nowtime,'reason'=>'实名认证失败']);
        }

        /* IM */
        $content='您的实名认证审核未通过，失败原因：'.$reason;
        if($status==1){
            $content='恭喜！您的实名认证审核通过了';
        }
        $this->sendIm($uid,$content);
        
        $this->success("操作成功");        
    }


    
    protected function sendIm($uid,$content){
            
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMTextElem',       //自定义类型
            'MsgContent' => array(
                //'Data' => json_encode($ext),
                'Desc' => '',
                'Text' => $content,
                //  'Ext' => $ext,
                //  'Sound' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=0;
        $receiver=(string)$uid;
        $api=getTxRestApi();
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,1);
    
        return 1;
    }

    protected function resetcache($uid){
        $key='user_auth_'.$uid;
        delcache($key);
        delcache('userinfo_'.$uid);
        $info=DB::name('user_auth')
                ->field('*')
                ->where("uid={$uid}")
                ->find();
        if($info){
            setcaches($key,$info);
        }
    }

    //获取标签列表
    private function getLabelLists(){

        $key='labellist';
        $label_list=getcaches($key);
        if(!$label_list){
            $label_list=DB::name('label')
                ->field('id,name,colour')
                ->order("list_order asc")
                ->select();
            if($label_list){
                setcaches($key,$label_list);
            }
        }

        return $label_list;
    }

}