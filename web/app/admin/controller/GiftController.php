<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 礼物管理 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class GiftController extends AdminBaseController
{
    var $type=[
        '0'=>'普通礼物',
        '1'=>'豪华礼物',
    ];
    
    var $swftype=[
        '0'=>'GIF',
        '1'=>'SVGA',
    ];
    public function index()
    {
        
        $list = Db::name('gift')
            ->order("list_order asc,id desc")
            ->paginate(20);
        $list->each(function($v, $k){
            $v['thumb']=get_upload_path($v['thumb']);
            $v['swf']=get_upload_path($v['swf']);
            return $v;
        });
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('type', $this->type);
        $this->assign('swftype', $this->swftype);

        return $this->fetch();
    }


    public function add()
    {
        $this->assign('type', $this->type);
        $this->assign('swftype', $this->swftype);
        return $this->fetch();
    }

    public function addPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();
            
            $name=$data['name'];
            if($name == ''){
                $this->error('请输入名称');
            }else{
                $check = Db::name('gift')->where("name='{$name}'")->find();
                if($check){
                    $this->error('名称已存在');
                }
            }
            
            
            $needcoin=$data['needcoin'];
            $thumb=$data['thumb'];
            
            if($needcoin==''){
                $this->error('请输入价格');
            }

            if($thumb==''){
                $this->error('请上传图片');
            }
            
            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            $data['addtime']=time();
            unset($data['gif']);
            unset($data['svga']);
            
            $id = DB::name('gift')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            $this->resetcache();
            $this->success("添加成功！");
        }
    }

    public function edit()
    {
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('gift')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('type', $this->type);
        $this->assign('swftype', $this->swftype);
        
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();

            $id=$data['id'];
            $name=$data['name'];
            if($name == ''){
                $this->error('请输入名称');
            }else{
                $check = Db::name('gift')->where("name='{$name}' and id!={$id}")->find();
                if($check){
                    $this->error('名称已存在');
                }
            }
            
            
            $needcoin=$data['needcoin'];
            $thumb=$data['thumb'];
            
            if($needcoin==''){
                $this->error('请输入价格');
            }

            if($thumb==''){
                $this->error('请上传图片');
            }
            
            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            unset($data['gif']);
            unset($data['svga']);
            
            $rs = DB::name('gift')->update($data);

            if($rs === false){
                $this->error("保存失败！");
            }
            $this->resetcache();
            $this->success("保存成功！");
        }
    }


    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('gift')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        $this->resetcache();
        $this->success("删除成功！",url("gift/index"));
    }

    public function listOrder()
    {
        $model = DB::name('gift');
        parent::listOrders($model);
        $this->resetcache();
        $this->success("排序更新成功！");
    }
    
    protected function resetcache(){
        $key='getGiftList';

        $gift=DB::name('gift')
                ->field('id,type,name,needcoin,thumb')
                ->order("list_order asc,id desc")
                ->select();
        foreach($gift as $k=>$v){
            $v['thumb']=get_upload_path($v['thumb']);
            $gift[$k]=$v;
        }
        if($gift){
            setcaches($key,$gift);
        }
    }
}