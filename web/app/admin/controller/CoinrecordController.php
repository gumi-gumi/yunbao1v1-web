<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 消费记录 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class CoinrecordController extends AdminBaseController
{
    var $type=array("0"=>"支出","1"=>"收入");
    var $action=array("1"=>"赠送礼物","2"=>"视频通话","3"=>"语音通话","7"=>"购买私信","8"=>"注册奖励");
        
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        if($data['type']!=''){
            $map[]=['type','=',$data['type']];
        }
        
        if($data['action']!=''){
            $map[]=['action','=',$data['action']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        $this->giftlist=Db::name('gift')->column('name','id');
        
        $list = Db::name('user_coinrecord')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           
           $action=$v['action'];
           switch($action){
                case '1':
                    $name=$this->giftlist[$v['actionid']];
                    if(!$name){
                        $name='礼物已删除';
                    }
                    $v['name']=$name.'('.$v['actionid'].')';
                    break;
                case '2':
                    $v['name']='视频通话';
                    break;
                case '3':
                    $v['name']='语音通话';
                    break;
                case '8':
                    $v['name']='注册奖励';
                    break;
           }
           
           return $v; 
        });
        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('type', $this->type);
        $this->assign('action', $this->action);

        return $this->fetch();
    }

}