<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------

namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

/**
 * Class AdminIndexController
 * @package app\user\controller
 *
 * @adminMenuRoot(
 *     'name'   =>'用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 10,
 *     'icon'   =>'group',
 *     'remark' =>'用户管理'
 * )
 *
 * @adminMenuRoot(
 *     'name'   =>'用户组',
 *     'action' =>'default1',
 *     'parent' =>'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   =>'',
 *     'remark' =>'用户组'
 * )
 */
class AdminIndexController extends AdminBaseController
{

    /**
     * 后台本站用户列表
     * @adminMenu(
     *     'name'   => '本站用户',
     *     'parent' => 'default1',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户',
     *     'param'  => ''
     * )
     */
    public function index(){

        $content = hook_one('user_admin_index_view');

        if (!empty($content)) {
            return $content;
        }
        
        $data = $this->request->param();

        $map=[];
        $map[]=['user_type','=',2];
        $order_str="";
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['create_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['create_time','<=',strtotime($end_time) + 60*60*24];
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['user_login|user_nickname|mobile','like','%'.$keyword.'%'];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $map[]=['id','=',$uid];
        }

        $sex=isset($data['sex']) ? $data['sex']: '';
        if($sex!=''){
            $map[]=['sex','=',$sex];
        }

        //钻石排序
        $coin_status=isset($data['coin_status']) ? $data['coin_status']: '';
        
        if($coin_status){
            $consumption_status='';
            $votes_status='';
            $votestotal_status='';
            $createtime_status='';
        }

        if($coin_status=='1'){
            $order_str="coin asc";
        }else if($coin_status=='-1'){
            $order_str="coin desc";
        }

        //总消费排序
        $consumption_status=isset($data['consumption_status']) ? $data['consumption_status']: '';

        if($consumption_status){
            $coin_status='';
            $votes_status='';
            $votestotal_status='';
            $createtime_status='';
        }
        
        if($consumption_status=='1'){
            $order_str="consumption asc";
        }else if($consumption_status=='-1'){
            $order_str="consumption desc";
        }

        //映票排序
        $votes_status=isset($data['votes_status']) ? $data['votes_status']: '';

        if($votes_status){
            $coin_status='';
            $consumption_status='';
            $votestotal_status='';
            $createtime_status='';
        }

        if($votes_status=='1'){
            $order_str="votes asc";
        }else if($votes_status=='-1'){
            $order_str="votes desc";
        }

        //总映票排序
        
        $votestotal_status=isset($data['votestotal_status']) ? $data['votestotal_status']: '';

        if($votestotal_status){
            $coin_status='';
            $consumption_status='';
            $votes_status='';
            $createtime_status='';
        }

        if($votestotal_status=='1'){
            $order_str="votestotal asc";
        }else if($votestotal_status=='-1'){
            $order_str="votestotal desc";
        }

        //注册时间排序
        $createtime_status=isset($data['createtime_status']) ? $data['createtime_status']: '';

        if($createtime_status){
            $coin_status='';
            $consumption_status='';
            $votes_status='';
            $votestotal_status='';
        }

        if($createtime_status=='1'){
            $order_str="create_time asc";
        }else if($createtime_status=='-1'){
            $order_str="create_time desc";
        }
        
        if($order_str==''){
            $order_str='id DESC';
        }

		
		$nums=Db::name("user")->where($map)->count();

        $list = Db::name('user')
            ->where($map)
            ->order($order_str)
            ->paginate(20);

        /*var_dump(Db::name('user')->getLastsql());
        die;*/

        foreach($list as $k=>$v){
            $v['user_login']=m_s($v['user_login']);
            $v['mobile']=m_s($v['mobile']);
            $v['user_email']=m_s($v['user_email']);


            $list[$k]= $v;
					 

        }
        $list->appends($data);
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
		$this->assign('nums', $nums);
        $this->assign('coin_status', $coin_status);
        $this->assign('consumption_status', $consumption_status);
        $this->assign('votes_status', $votes_status);
        $this->assign('votestotal_status', $votestotal_status);
        $this->assign('createtime_status', $createtime_status);
        // 渲染模板输出
        return $this->fetch();
    }

    /**
     * 本站用户拉黑
     * @adminMenu(
     *     'name'   => '本站用户拉黑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户拉黑',
     *     'param'  => ''
     * )
     */
    public function ban()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 0);
            if ($result) {
                $key2="token_".$id;
                delcache($key2);
                Db::name("user_token")->where(["user_id" => $id])->delete();
                $this->success("会员拉黑成功！");
            } else {
                $this->error('会员拉黑失败,会员不存在,或者是管理员！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 本站用户启用
     * @adminMenu(
     *     'name'   => '本站用户启用',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户启用',
     *     'param'  => ''
     * )
     */
    public function cancelBan()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 1);
            $key2="user_status_".$id;
                delcache($key2);
            $this->success("会员启用成功！");
        } else {
            $this->error('数据传入失败！');
        }
    }
    
    /* 修改推荐值 */
    public function setrecommend()
    {
        $uid = $this->request->param('uid', 0, 'intval');
        if(!$uid){
            $this->error("数据传入失败！");
        }
        $recommend = $this->request->param('recommend', 0, 'intval');


        $nowtime=time();
        $rs=DB::name("user")->where("id={$uid}")->update(['recommend_val'=>$recommend]);
        if($rs===false){
            $this->error("操作失败");
        }
        
        $this->success("操作成功");        
    }

    /* 取消实名认证 */
    public function cancelUserAuth(){

        $uid = $this->request->param('id', 0, 'intval');
        if(!$uid){
            $this->error("数据传入失败！");
        }

        $nowtime=time();
        $rs=DB::name("user")->where("id={$uid}")->update(['isauth'=>0,'isauthor_auth'=>0]);
        if($rs===false){
            $this->error("操作失败");
        }
        
        DB::name("user_auth")->where("uid={$uid}")->update(['status'=>2,'uptime'=>$nowtime,'reason'=>'取消实名认证']);
        $key='user_auth_'.$uid;
        delcache($key);

        Db::name("author_auth")->where("uid={$uid}")->update(['status'=>2,'uptime'=>$nowtime,'reason'=>'取消实名认证']);

        DB::name("backwall")->where("uid={$uid}")->delete();

        $this->sendIm($uid,'你的实名认证已被取消');
        
        $this->success("操作成功");        
    }

    //取消主播认证
    public function cancelAuthorAuth(){
        $uid = $this->request->param('id', 0, 'intval');
        if(!$uid){
            $this->error("数据传入失败！");
        }

        $rs=DB::name("user")->where("id={$uid}")->update(['isauthor_auth'=>0]);
        if($rs===false){
            $this->error("操作失败");
        }

        $nowtime=time();
        DB::name("author_auth")->where("uid={$uid}")->update(['status'=>2,'uptime'=>$nowtime,'reason'=>'取消主播认证']);
        DB::name("backwall")->where("uid={$uid}")->delete();

        $this->sendIm($uid,'你的主播认证已被取消');

        $this->success("操作成功");
    }
    
    public function add(){
        
        $label_list=$this->getLabelLists();
        $this->assign("label_list",$label_list);
        return $this->fetch();
    }

    public function addPost(){

        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
            $user_login=trim($data['user_login']);
            //$user_pass=trim($data['user_pass']);
            $user_nickname=trim($data['user_nickname']);
            $sex=$data['sex'];
            $avatar=$data['avatar'];
            $avatar_thumb=$data['avatar_thumb'];
            $signature=trim($data['signature']);
            $thumb=$data['thumb'];
            $photos=$data['photos'];
            $video_thumb=$data['video_thumb'];
            $video=$data['video'];
            $username=$data['username'];
            $cardno=$data['cardno'];
            $height=trim($data['height']);
            $weight=trim($data['weight']);
            $constellation=$data['constellation'];
            $label=$data['label'];
            $intr=trim($data['intr']);
            $province=$data['province'];
            $city=$data['city'];
            $district=$data['district'];

            if(!$avatar){
                $avatar='/default.png';
            }else{
                $avatar=set_upload_path($avatar);
            }

            if(!$avatar_thumb){
                $avatar_thumb='/default_thumb.png';
            }else{
                $avatar_thumb=set_upload_path($avatar_thumb);
            }

            //判断登录账号
            if($user_login == ''){
                $this->error('请输入手机号');
            }else{
                
                if(!checkMobile($user_login)){
                    $this->error('请输入正确手机号');
                }
                
                $check = Db::name('user')->where("user_login='{$user_login}'")->find();
                if($check){
                    $this->error('该账号已存在');
                }
            }
            
            //判断密码
            $user_pass='live1v1'.time();
            
            $user_pass = cmf_password($user_pass);
            
            //判断昵称
            if($user_nickname == ''){
                $this->error('请输入昵称');
            }else{

                /*$check = Db::name('user')->where("user_nickname='{$user_nickname}'")->find();
                if($check){
                    $this->error('昵称已存在');
                }*/

                if(mb_strlen($user_nickname)>7){
                    $this->error('昵称最多7个字');
                }

                if($user_nickname=='用户已注销' || $user_nickname=='已注销'){
                   
                    $this->error('请填写正确的昵称');
                }

                $is_has_sensitive=checkSensitiveWords($user_nickname);
                if($is_has_sensitive){
                    
                    $this->error("昵称输入非法,请重新输入");
                }

            }

            if(!$video_thumb){
                $this->error("请上传个人介绍视频封面");
            }

            if(!$video){
                $this->error("请上传视频/填写视频地址");
            }

            $uploadSetting = cmf_get_upload_setting();
            $extensions=$uploadSetting['file_types']['video']['extensions'];
            $allow=explode(",",$extensions);

            $video_type=substr(strrchr($video, '.'), 1);

            if(!in_array(strtolower($video_type), $allow)){
                $this->error("视频文件后缀不正确");
            }

            //判断姓名
            if($username==''){
                $this->error("请填写姓名");
            }

            if(mb_strlen($username)>6){
                $this->error("姓名字数6字以内");
            }

            $is_has_sensitive=checkSensitiveWords($username);
            if($is_has_sensitive){
 
                $this->error("姓名输入非法,请重新输入");
            }

            if($cardno==''){
                $this->error("请填写身份证号");
            }

            /*if(!isCreditNo($cardno)){
                $this->error("身份证号不合法");
            }*/
            
            //判断签名
            if($signature==''){
                $this->error("请填写签名");
            }

            if(mb_strlen($signature)>40){
                $this->error("签名字数在40字以内");
            }

            $is_has_sensitive=checkSensitiveWords($signature);
            if($is_has_sensitive){
 
                $this->error("签名输入非法,请重新输入");
            }

            //判断封面
            if($thumb==""){
                $this->error("请上传封面");
            }

            $thumb=set_upload_path($thumb);

            //判断背景墙
            if($photos==""){
                $this->error("请上传背景墙");
            }

            $photos=set_upload_path($photos);

            //判断身高
            if($height==""){
                $this->error("请填写身高");
            }

            if(!is_numeric($height) || $height!=floor($height)){
                $this->error("身高在1-240之间的整数");
            }

            if($height<1 || $height>240){
                $this->error("身高在1-240之间");
            }

            //判断体重
            if($weight==""){
                $this->error("请填写体重");
            }

            if(!is_numeric($weight) || $weight!=floor($weight)){
                $this->error("体重在1-200之间的整数");
            }

            if($weight<1 || $weight>200){
                $this->error("体重在1-200之间");
            }

            //判断标签
            
            if($label=="" || is_null($label)){
                $this->error("请选择形象标签");
            }



            $label_count=count($label);
            if($label_count>3){
                $this->error("形象标签最多选3项");
            }

            $label_ids=implode(',', $label);

            //判断个人介绍
            if($intr==''){
                $this->error("请填写个人介绍");
            }

            if(mb_strlen($intr)>40){
                $this->error("个人介绍在40字以内");
            }

            if($province==""){
                $this->error("请选择省份");
            }

            if($city==""){
                $this->error("请选择市");
            }

            if($district==""){
                $this->error("请选择区");
            }

            $now=time();

            $fee_voice=DB::name("fee_voice")->order('coin asc')->value('coin');
            $fee_video=DB::name("fee_video")->order('coin asc')->value('coin');

            $label_name=[];
            $label_color=[];
            $label_list=$this->getLabelLists();
            
            foreach ($label as $k => $v) {
                foreach ($label_list as $k1 => $v1) {
                    if($v==$v1['id']){
                        $label_name[]=$v1['name'];
                        $label_color[]=$v1['colour'];
                        break;
                    }
                }
            }
            
            $label_names=implode(',', $label_name);
            $label_colors=implode(',', $label_color);

            $arr=array(
                'user_login'=>$user_login,
                'user_pass'=>$user_pass,
                'user_nickname'=>$user_nickname,
                'sex'=>$sex,
                'avatar'=>$avatar,
                'avatar_thumb'=>$avatar_thumb,
                'signature'=>$signature,
                'mobile'=>$user_login,
                'user_status'=>1,
                'user_type'=>2,
                'create_time'=>$now,
                'isauth'=>1,
                'isvoice'=>1,
                'isvideo'=>1,
                'voice_value'=>$fee_voice,
                'video_value'=>$fee_video,
                'isauthor_auth'=>1,
                'height'=>$height,
                'weight'=>$weight,
                'constellation'=>$constellation,
                'labelid'=>$label_ids,
                'label'=>$label_names,
                'label_c'=>$label_colors,
                'province'=>$province,
                'city'=>$city,
                'district'=>$district,
                'intr'=>$intr,
                

            );

            $id = DB::name('user')->insertGetId($arr);
            if(!$id){
                $this->error("添加失败！");
            }

            //用户认证
            $auth_arr=array(
                'uid'=>$id,
                'cardno'=>$cardno,
                'name'=>$username,
                'mobile'=>$user_login,
                'addtime'=>$now,
                'uptime'=>$now,
                'status'=>1

            );

            DB::name("user_auth")->insert($auth_arr);
            $this->resetcache($id);

            //主播认证
            $author_arr=array(
                'uid'=>$id,
                'thumb'=>$thumb,
                'backwall'=>$photos,
                'addtime'=>$now,
                'uptime'=>$now,
                'video_thumb'=>set_upload_path($video_thumb),
                'video'=>set_upload_path($video),
                'status'=>1

            );

            DB::name("author_auth")->insert($author_arr);

            //更新背景墙
            $isexist=DB::name("backwall")->where("uid={$id}")->find();
            if(!$isexist){
                
                $data=[
                    'uid'=>$id,
                    'thumb'=>$thumb,
                    'href'=>'',
                    'type'=>-1,
                ];
                DB::name("backwall")->insert($data);
                   
                $data=[
                    'uid'=>$id,
                    'thumb'=>$photos,
                    'href'=>'',
                    'type'=>0,
                ];
                DB::name("backwall")->insert($data);                   
 
            }

            $this->success("添加成功！");
        }
    }

    public function edit(){

        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('user')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }

        
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost(){

        if ($this->request->isPost()) {
            $data = $this->request->param();

            $id=$data['id'];
            $login_type=$data['login_type'];
            $user_login=trim($data['user_login']);
            $user_nickname=trim($data['user_nickname']);
            $sex=$data['sex'];
            $avatar=$data['avatar'];
            $avatar_thumb=$data['avatar_thumb'];
            $consumption=trim($data['consumption']);

            //判断登录名
            if($user_login == ''){
                $this->error('请输入用户名');
            }else{
                
                if($login_type==0){
                    if(!checkMobile($user_login)){
                        $this->error('请输入正确手机号');
                    }
                }
                
                $check = Db::name('user')->where("user_login='{$user_login}' and id!={$id}")->find();
                if($check){
                    $this->error('该账号已存在');
                }
            }

            
            //判断昵称
            if($user_nickname == ''){
                $this->error('请输入昵称');
            }else{
               /* $check = Db::name('user')->where("user_nickname='{$user_nickname}' and id!={$id}")->find();
                if($check){
                    $this->error('昵称已存在');
                }*/

                if(mb_strlen($user_nickname)>7){
                    $this->error('昵称最多7个字');
                }

                if($user_nickname=='用户已注销' || $user_nickname=='已注销'){
                   
                    $this->error('请填写正确的昵称');
                }

                $is_has_sensitive=checkSensitiveWords($user_nickname);
                if($is_has_sensitive){
                    
                    $this->error('昵称输入非法,请重新输入');
                }
            }

            if(!$sex){
                $this->error("请选择性别");
            }

            if($avatar==''){
                $this->error("请上传头像");
            }

            if($avatar_thumb==''){
                $this->error("请上传头像小图");
            }

            if($consumption=='' || !is_numeric($consumption) || $consumption!=floor($consumption)){
                $this->error("总消费请填写大于等于0的整数");
            }

            $arr=array(
                'id'=>$id,
                'user_login'=>$user_login,
                'user_nickname'=>$user_nickname,
                'sex'=>$sex,
                'avatar'=>$avatar,
                'avatar_thumb'=>$avatar_thumb,
                'consumption'=>$consumption
            );
            
            $rs = DB::name('user')->update($arr);

            if($rs === false){
                $this->error("保存失败！");
            }

            $key='userinfo_'.$id;
            delcache($key);
            $this->success("保存成功！");
        }
    }


    public function del(){

        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('user')->where("id={$id}")->delete();
        
        if(!$rs){
            $this->error("删除失败！");
        }

		//邀请好友关系表
        DB::name('agent')->where("uid={$id} or one={$id}")->delete();
        //邀请码
        DB::name('agent_code')->where("uid={$id}")->delete();
        //邀请限制
        DB::name("agent_limit")->where("uid={$id}")->delete();
        //邀请收益
        DB::name("agent_profit")->where("uid={$id}")->delete();
        //
        DB::name("answer_rate")->where("uid={$id}")->delete();

        
        //动态
        $dynamic_list=DB::name("dynamics")->where("uid={$id}")->column("id");
        DB::name("dynamics")->where("uid={$id}")->delete();

        //动态举报
        DB::name("dynamic_report")->where("uid={$id} or touid={$id}")->delete();
        if(!empty($dynamic_list)){
            DB::name("dynamic_report")->where(['dynamicid'=>$dynamic_list])->delete();
        }

        //动态评论
        DB::name("dynamics_comments")->where("uid={$id} or touid={$id}")->delete();
        if(!empty($dynamic_list)){
            DB::name("dynamics_comments")->where(['dynamicid'=>$dynamic_list])->delete();
        }

        //动态评论点赞
        DB::name("dynamics_comments_like")->where("uid={$id} or touid={$id}")->delete();
        if(!empty($dynamic_list)){
            DB::name("dynamics_comments_like")->where(['dynamicid'=>$dynamic_list])->delete();
        }
        //动态点赞
        DB::name("dynamics_like")->where("uid={$id}")->delete();
        if(!empty($dynamic_list)){
            DB::name("dynamics_like")->where(['dynamicid'=>$dynamic_list])->delete();
        }
        //标签数量
        DB::name("evaluate_count")->where("uid={$id}")->delete();
        //标签记录
        DB::name("evaluate_log")->where("uid={$id}")->delete();
        //im次数限制
        DB::name("im_limit")->where("uid={$id}")->delete();
        //音乐收藏
        DB::name("music_collection")->where("uid={$id}")->delete();

        //相册
        $photo_list=DB::name("photo")->where("uid={$id}")->column("id");
        DB::name("photo")->where("uid={$id}")->delete();

        //相册购买
        DB::name("photo_buy")->where("uid={$id}")->delete();
        if(!empty($photo_list)){
            DB::name("photo_buy")->where(['photoid'=>$photo_list])->delete();
        }
        //管理员用户
        DB::name("role_user")->where("user_id={$id}")->delete();
        //订阅
        DB::name("subscribe")->where("uid={$id} or liveuid={$id}")->delete();
        //系统通知
        DB::name("sys_notice")->where("touid={$id}")->delete();
        //用户关注
        DB::name("user_attention")->where("uid={$id} or touid={$id}")->delete();
        //用户认证
        DB::name("user_auth")->where("uid={$id}")->delete();

        //用户美颜
        DB::name("user_beauty_preinstall")->where("uid={$id}")->delete();
        //用户拉黑
        DB::name("user_black")->where("uid={$id} or touid={$id}")->delete();
        //用户举报
        DB::name("user_report")->where("uid={$id} or touid={$id}")->delete();
        //用户token
        DB::name("user_token")->where("user_id={$id}")->delete();

        //用户视频
        $video_list=DB::name("video")->where("uid={$id}")->column("id");
        DB::name("video")->where("uid={$id}")->delete();
        //视频购买
        DB::name("video_buy")->where("uid={$id}")->delete();
        if(!empty($video_list)){
            DB::name("video_buy")->where(['videoid'=>$video_list])->delete();
        }
        //视频点赞
        DB::name("video_like")->where("uid={$id}")->delete();
        if(!empty($video_list)){
            DB::name("video_like")->where(['videoid'=>$video_list])->delete();
        }
        //视频举报
        DB::name("video_report")->where("uid={$id} or touid={$id}")->delete();
        if(!empty($video_list)){
            DB::name("video_report")->where(['videoid'=>$video_list])->delete();
        }
        //视频观看
        DB::name("video_view")->where("uid={$id}")->delete();
        if(!empty($video_list)){
            DB::name("video_view")->where(['videoid'=>$video_list])->delete();
            Db::name("video_comments")->where(['videoid'=>$video_list])->delete();
            Db::name("video_comments_like")->where(['videoid'=>$video_list])->delete();
        }

        //vip
        DB::name("vip_user")->where("uid={$id}")->delete();

        //主播认证
        Db::name("author_auth")->where(['uid'=>$id])->delete();

        Db::name("video_comments")->where("uid={$id} or touid={$id}")->delete();
        Db::name("video_comments_like")->where("uid={$id} or touid={$id}")->delete();

        Db::name("family_user")->where("uid={$id}")->delete();
        Db::name("family_profit")->where("uid={$id}")->delete();
        Db::name("family_user_divide_apply")->where("uid={$id}")->delete();

        $familyid=Db::name("family")->where("uid={$id}")->value("id");
        if($familyid){
            Db::name("family_profit")->where("familyid={$familyid}")->delete();
            Db::name("family_user")->where("familyid={$familyid}")->delete();
            Db::name("family_user_divide_apply")->where("familyid={$familyid}")->delete();
            
        }
		
        $key='userinfo_'.$id;
        delcache($key);
        delcache('token_'.$id);
        $this->success("删除成功！",url("user/adminIndex/index"));
    }


    //获取标签列表
    private function getLabelLists(){

        $key='labellist';
        $label_list=getcaches($key);
        if(!$label_list){
            $label_list=DB::name('label')
                ->field('id,name,colour')
                ->order("list_order asc")
                ->select();
            if($label_list){
                setcaches($key,$label_list);
            }
        }

        return $label_list;
    }

    protected function resetcache($uid){
        $key='user_auth_'.$uid;
        delcache($key);
        delcache('userinfo_'.$uid);
        $info=DB::name('user_auth')
                ->field('*')
                ->where("uid={$uid}")
                ->find();
        if($info){
            setcaches($key,$info);
        }
    }


    protected function sendIm($uid,$content){
        
            
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMTextElem',       //自定义类型
            'MsgContent' => array(
                //'Data' => json_encode($ext),
                'Desc' => '',
                'Text' => $content,
                //  'Ext' => $ext,
                //  'Sound' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=0;
        $receiver=(string)$uid;
        $api=getTxRestApi();
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,1);
        
        //file_put_contents(CMF_ROOT.'log/auth.txt',date('y-m-d H:i:s').'提交参数信息 msg_content:'.json_encode($msg_content)."\r\n",FILE_APPEND);            
        //file_put_contents(CMF_ROOT.'log/auth.txt',date('y-m-d H:i:s').'提交参数信息 ret:'.json_encode($ret)."\r\n",FILE_APPEND);
        return 1;
    }



}
