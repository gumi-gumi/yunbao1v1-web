<?php /*a:2:{s:80:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/admin/main/index.html";i:1647422687;s:77:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/public/header.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<link rel="stylesheet" type="text/css" href="/static/admin/css/index.css">
<style>
    .home-info li em {
        float: left;
        width: 120px;
        font-style: normal;
        font-weight: bold;
    }

    .home-info ul {
        padding: 0;
        margin: 0;
    }

    .panel {
        margin-bottom: 0;
    }

    .grid-sizer {
        width: 10%;
    }

    .grid-item {
        margin-bottom: 5px;
        padding: 5px;
    }

    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        padding-left: 5px;
        padding-right: 5px;
        float: none;
    }

</style>
<?php 
    \think\facade\Hook::listen('admin_before_head_end',null,null,false);
 ?>
</head>
<body>
<div class="wrap">
    <?php if(empty($has_smtp_setting) || (($has_smtp_setting instanceof \think\Collection || $has_smtp_setting instanceof \think\Paginator ) && $has_smtp_setting->isEmpty())): ?>

    <?php endif; if(!extension_loaded('fileinfo')): ?>
        <div class="grid-item col-md-12">
            <div class="alert alert-danger alert-dismissible fade in" role="alert" style="margin-bottom: 0;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>提示!</strong> php_fileinfo扩展没有开启，无法正常上传文件！
            </div>
        </div>
    <?php endif; ?>

    <div class="home-grid">
        <!-- width of .grid-sizer used for columnWidth -->
        <div class="grid-sizer"></div>
        
            
        <div class="list">
            <div class="list_title list_title5">通话时间统计</div>
            <div class="list_bd">
                <div class="bd_pt">
                    <div class="bd_pt_top">
                        <p class="bd_pt_top_t">语音通话时间（分钟）</p>
                        <p class="bd_pt_top_n"><?php echo $live_voice_t; ?></p>
                    </div>
                    
                    <div class="bd_pt_bot">
                        <p>较于前一日：<span class="<?php echo $live_voice_rate_c; ?>"><?php echo $live_voice_rate; ?></span></p>
                        <p>昨日：<span><?php echo $live_voice_y; ?></span></p>
                        <p>语音通话总时间：<span><?php echo $live_voice; ?></span></p>
                    </div>
                </div>
                <div class="bd_pt">
                    <div class="bd_pt_top">
                        <p class="bd_pt_top_t">视频通话时间（分钟）</p>
                        <p class="bd_pt_top_n"><?php echo $live_video_t; ?></p>
                    </div>
                    
                    <div class="bd_pt_bot">
                        <p>较于前一日：<span class="<?php echo $live_video_rate_c; ?>"><?php echo $live_video_rate; ?></span></p>
                        <p>昨日：<span><?php echo $live_video_y; ?></span></p>
                        <p>视频通话总时间：<span><?php echo $live_video; ?></span></p>
                    </div>
                </div>
                <div class="bd_pt">
                    <div class="bd_pt_top">
                        <p class="bd_pt_top_t">总通话时间（分钟）</p>
                        <p class="bd_pt_top_n"><?php echo $lives_t; ?></p>
                    </div>
                    
                    <div class="bd_pt_bot">
                        <p>较于前一日：<span class="<?php echo $lives_rate_c; ?>"><?php echo $lives_rate; ?></span></p>
                        <p>昨日：<span><?php echo $lives_y; ?></span></p>
                        <p>通话时间总数：<span><?php echo $lives; ?></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/admin.js"></script>
<?php 
    $lang_set=defined('LANG_SET')?LANG_SET:'';
    $thinkcmf_version=cmf_version();
 ?>
<script>


</script>
<?php 
    \think\facade\Hook::listen('admin_before_body_end',null,null,false);
 ?>
</body>
</html>