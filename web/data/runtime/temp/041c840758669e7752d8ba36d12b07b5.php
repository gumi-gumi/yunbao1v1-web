<?php /*a:2:{s:83:"/data/wwwroot/git1v1.yunbaozb.com/themes/admin_simpleboot3/admin/monitor/index.html";i:1646881836;s:77:"/data/wwwroot/git1v1.yunbaozb.com/themes/admin_simpleboot3/public/header.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
    <style>
        .clear{
            clear:both;
        }
        .clear::after{
            clear:both;
            content: " ";
            display: table;
        }
        .list{}
        .list ul{}
        .list ul li{
            list-style:none;
            float:left;
            margin-right:10px;
            width:360px;
        }
        .list ul li .title{
            line-height:30px;
        }
        .list ul li .view{
            float:left;
            width:180px;
            
        }
        .list ul li .view .name{
            line-height:30px;
        }
        .list ul li .view .view_b{
            width:180px;
            height:320px;
        }
    </style>
    <script src="//cdn.jsdelivr.net/npm/xgplayer@1.1.4/browser/index.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/xgplayer-flv.js/browser/index.js" charset="utf-8"></script>
    
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >列表</a></li>
		</ul>
        
        <div class="list">
            <ul>
                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <li>
                    <div class="title">类型：<?php echo $type[$vo['type']]; ?></div>
                    <div class="title">通话时长：<?php echo $vo['length']; ?></div>
                    <div class="title">
                        <a class="btn btn-danger" href="javascript:closeRoom('<?php echo $vo['id']; ?>');">关闭</a>
                    </div>
                    <div class="clear">
                        <div class="view">
                            <div class="name">用户：<?php echo $vo['userinfo']['user_nickname']; ?> (<?php echo $vo['uid']; ?>)</div>
                        </div>
                        <div class="view">
                            <div class="name">主播：<?php echo $vo['liveinfo']['user_nickname']; ?> (<?php echo $vo['liveuid']; ?>)</div>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="view">
                            <div class="view_b" id="mse_<?php echo $vo['uid']; ?>"></div>
                            <script>
                              new FlvJsPlayer({
                                "id": "mse_<?php echo $vo['uid']; ?>",
                                "url": "<?php echo $vo['pull_user']; ?>",
                                "playsinline": true,
                                "whitelist": [""],
                                "autoplay": true,
                                "volume": 0.4,
                                "width": 180,
                                "height": 320
                                
                              });
                            </script>
                        </div>
                        <div class="view">
                            <div class="view_b" id="mse_<?php echo $vo['liveuid']; ?>"></div>
                            <script>
                              new FlvJsPlayer({
                                "id": "mse_<?php echo $vo['liveuid']; ?>",
                                "url": "<?php echo $vo['pull_live']; ?>",
                                "playsinline": true,
                                "whitelist": [""],
                                "autoplay": true,
                                "volume": 0.4,
                                "width": 180,
                                "height": 320
                              });
                            </script>
                        </div>
                    </div>
                    
                </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
        <div class="pagination"><?php echo $page; ?></div>
	</div>
	<script src="/static/js/admin.js"></script>
    <script type="text/javascript">
        function closeRoom(roomId){
            $.ajax({
                async: false,
                url: '<?php echo url('Monitor/stopRoom'); ?>',
                data:{id:roomId},
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if(data.status ==0){
                        alert(data.info);
                    }else{
                        alert("房间已关闭");
                        location.reload();
                    }
                },
                error:function(XMLHttpRequest, textStatus, errorThrown){
                    alert('关闭失败，请重试');
                }
            });
        }
    </script>
</body>
</html>