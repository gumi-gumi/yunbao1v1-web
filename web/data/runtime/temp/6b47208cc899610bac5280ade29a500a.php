<?php /*a:4:{s:74:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/appapi/record/expend.html";i:1646881836;s:65:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/appapi/head.html";i:1646881836;s:67:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/appapi/footer.html";i:1646881836;s:68:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/appapi/scripts.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <title>支出</title>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="<?php echo (isset($site_info['site_seo_keywords']) && ($site_info['site_seo_keywords'] !== '')?$site_info['site_seo_keywords']:''); ?>"/>
    <meta name="description" content="<?php echo (isset($site_info['site_seo_description']) && ($site_info['site_seo_description'] !== '')?$site_info['site_seo_description']:''); ?>">
    <meta content="telephone=no" name="format-detection" />
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

    <!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
    <link rel="icon" href="/favicon.ico" >
    <link rel="shortcut icon" href="/favicon.ico">
    <link href='/static/appapi/css/common.css?t=1555903036' rel="stylesheet" type="text/css" >

	
    <link href='/static/appapi/css/record.css?t=3' type="text/css" rel="stylesheet">
</head>
<body>
     
     <div class="recordlist">
        <ul>
            <li class="title">
                <span class="span1">支出类型</span>
                <span class="span2">收礼用户</span>
                <span class="span3"><?php echo $site_info['name_coin']; ?></span>
                <span class="span4">时间</span>
            </li>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <li>
                <span class="span1"><?php echo $v['actionname']; ?></span>
                <span class="span2"><?php echo $v['user_nickname']; ?></span>
                <span class="span3"><?php echo $v['totalcoin']; ?></span>
                <span class="span4"><?php echo $v['addtime']; ?></span>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
            <div class="empty">
            </div>
            <?php endif; ?>
        </ul>
     </div>
     <script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>

     <script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>



      <script>
        var uid='<?php echo $uid; ?>';
        var token='<?php echo $token; ?>';
     	$(function(){
            function getlistmore(){
                $.ajax({
                    url:'/appapi/record/expendmore',
                    data:{'page':page,'uid':uid,'token':token},
                    type:'post',
                    dataType:'json',
                    success:function(data){
                        if(data.nums>0){
                                var nums=data.nums;
                                var list=data.list.data;
                                var html='';
                                for(var i=0;i<nums;i++){
                                    var v=list[i];
                                    html+='<li>\
                                            <span class="span1">'+v['actionname']+'</span>\
                                            <span class="span2">'+v['user_nickname']+'</span>\
                                            <span class="span3">'+v['totalcoin']+'</span>\
                                            <span class="span4">'+v['addtime']+'</span>\
                                        </li>';
                                }
                            
                            $(".recordlist ul").append(html);
                        }
                        
                        if(data.isscroll==1){
                            page++;
                            isscroll=true;
                        }
                    }
                })
            }
            

            var page=2; 
            var isscroll=true; 

            var scroll_obj=$(window);
            scroll_obj.scroll(function(){  
                    var srollPos = scroll_obj.scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)  		
                    var totalheight = parseFloat(scroll_obj.height()) + parseFloat(srollPos);  
                    if(($(document).height()-50) <= totalheight  && isscroll) {  
                            isscroll=false;
                            getlistmore()
                    }  
            });  


        })
     </script>
</body>
</html>
